<?php
# composer autoload
include __DIR__ . '/../vendor/autoload.php';

use supervillainhq\spectre\db\SqlQuery;

error_reporting(E_ALL);

// disable DOMPDF's internal autoloader if you are using Composer
define('DOMPDF_ENABLE_AUTOLOAD', false);

// include DOMPDF's default configuration
require_once __DIR__ . '/../vendor/dompdf/dompdf/dompdf_config.inc.php';

try {

    /**
     * Read the configuration
     */
    $config = include __DIR__ . "/../config/config.php";

    /**
     * Read auto-loader
     */
    include __DIR__ . "/../config/loader.php";

    /**
     * Read services
     */
    include __DIR__ . "/../config/services.php";

    /**
     * Handle the request
     */
    $application = new \Phalcon\Mvc\Application($di);

	$application->registerModules(
		[
			'frontend' => [
				'className' => 'testapp\ShopFront',
				'path'      => '../app/testapp/ShopFront.php',
			],
			'api'  => [
				'className' => 'testapp\api\ShopAPI',
				'path'      => '../app/testapp/api/ShopAPI.php',
			],
			'admin'  => [
				'className' => 'testapp\admin\ShopAdmin',
				'path'      => '../app/testapp/admin/TobissenAdmin.php',
			]
		]
	);
    // static reference to the current dbadapter
    SqlQuery::$dbAdapter = $application->db;
    // set up datetimeutil
    \supervillainhq\core\date\Date::config((array) $config->localization);

    echo $application->handle()->getContent();

} catch (\Exception $e) {
    echo $e->getMessage();
}
