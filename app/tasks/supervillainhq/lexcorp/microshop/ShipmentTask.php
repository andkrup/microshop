<?php

namespace supervillainhq\lexcorp\microshop{
	use Phalcon\Cli\Task;

	/**
	 * Created or manage model classes
	 * User: ak
	 * Date: 22/03/16
	 * Time: 10:53
	 */
	class ShipmentTask extends Task{
		public function mainAction(){
			echo "\nactions:\n";
			echo "    create\n";
			echo "\nparameters:\n";
			echo "    model\n";
		}

		public function addAction(array $parameters = null){
			echo "\nadd shipment\n";
		}
		public function statusAction(array $parameters = null){
			echo "\nlist shipments\n";
		}
	}
}
