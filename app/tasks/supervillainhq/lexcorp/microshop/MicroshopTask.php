<?php

namespace supervillainhq\lexcorp\microshop{
	use Phalcon\Cli\Task;
	use Phalcon\DI;
	use supervillainhq\thugs\cli\CliOutput;
	use supervillainhq\thugs\oddjob\Oddjob;

	/**
	 * Created or manage model classes
	 * User: ak
	 * Date: 22/03/16
	 * Time: 10:53
	 */
	class MicroshopTask extends Task{
		public function mainAction(){
			$output = new CliOutput();
			$output->line("Microshop main task");
		}

		public function installAction(array $parameters = null){
			$output = new CliOutput();
			$output->line("Add all required core features");
			$output->line("Add all required aliases to the classmap");

			// can't autoload the task directly cuz it's namespace is a dev-dependency and is not included by webservers/fpm-processes running with no-dev
			$classmaptask = Oddjob::instance()->getTaskInstance('ClassMap');

			$classmaptask->addClassAlias('address', 'supervillainhq\spectre\contacts\db\AddressMapper', 'mappers');
			$classmaptask->addClassAlias('address', 'supervillainhq\spectre\contacts\db\AddressWriter', 'writers');
		}

		public function uninstallAction(array $parameters = null){
			$output = new CliOutput();
			$output->line("Remove all required core features");
		}

		public function updateAction(array $parameters = null){
			$output = new CliOutput();
			$output->line("Update configurations and smooth rough patches");
		}
	}
}
