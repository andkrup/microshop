<?php

namespace supervillainhq\lexcorp\microshop{
	use Phalcon\Cli\Task;

	/**
	 * Created or manage model classes
	 * User: ak
	 * Date: 22/03/16
	 * Time: 10:53
	 */
	class BrowsingTask extends Task{
		public function mainAction(){
			echo "\nactions:\n";
			echo "    create\n";
			echo "\nparameters:\n";
			echo "    model\n";
		}

		public function listTagsAction(array $parameters = null){
			echo "\nlist tags\n";
		}
		public function addTagAction(array $parameters = null){
			echo "\nadd tag\n";
		}
		public function removeTagAction(array $parameters = null){
			echo "\nremove tag\n";
		}

		public function listCategoriesAction(array $parameters = null){
			echo "\nadd categories\n";
		}
		public function addCategoryAction(array $parameters = null){
			echo "\nadd category\n";
		}
		public function removeCategoryAction(array $parameters = null){
			echo "\nremove categories\n";
		}

		public function promotionAction(array $parameters = null){
			echo "\nhandle promotion workflows \n";
		}

		public function shipmentAction(array $parameters = null){
			echo "\nhandle shipment workflows \n";
		}

		public function cartAction(array $parameters = null){
			echo "\nhandle shipment workflows \n";
		}

		public function browsingAction(array $parameters = null){
			echo "\nhandle browsing workflows \n";
		}
	}
}
