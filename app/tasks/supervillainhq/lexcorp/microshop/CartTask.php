<?php

namespace supervillainhq\lexcorp\microshop{
	use Phalcon\Cli\Task;

	/**
	 * Created or manage model classes
	 * User: ak
	 * Date: 22/03/16
	 * Time: 10:53
	 */
	class CartTask extends Task{
		public function mainAction(){
			echo "\nactions:\n";
			echo "    create\n";
			echo "\nparameters:\n";
			echo "    model\n";
		}

		public function addAction(array $parameters = null){
			echo "\nhandle gateway workflows \n";
		}
		public function removeAction(array $parameters = null){
			echo "\nhandle gateway workflows \n";
		}

		public function catalogueAction(array $parameters = null){
			echo "\nhandle catalogue workflows \n";
		}

		public function productAction(array $parameters = null){
			echo "\nhandle product workflows \n";
		}

		public function promotionAction(array $parameters = null){
			echo "\nhandle promotion workflows \n";
		}

		public function shipmentAction(array $parameters = null){
			echo "\nhandle shipment workflows \n";
		}

		public function cartAction(array $parameters = null){
			echo "\nhandle shipment workflows \n";
		}

		public function browsingAction(array $parameters = null){
			echo "\nhandle browsing workflows \n";
		}
	}
}
