<?php
namespace controllers{
	use supervillainhq\spectre\cms\CmsController;

	class EpayController extends CmsController{

		public function indexAction(){
			echo "";
			exit;
		}

		/**
		 * Requested by epay upon accept, under the hood (server to server)
		 */
		public function callbackAction(){
			if(!$this->shop->gateway()->validateRequest($this->request)){
				echo 'invalid hash';
			}
			else{
				$invoiceId = $this->request->get('orderid');
				$transactionId = $this->request->get('txnid');
				$this->shop->authorizeTransaction($invoiceId, $transactionId);
				echo "";
			}
// 			echo 'callback captured';
			exit;
		}

		/**
		 * Redirected to here from epays pages in the users browser (similar to the callbackAction() but in the open)
		 */
		public function acceptAction(){
			if(!$this->shop->gateway()->validateRequest($this->request)){
				echo 'invalid hash';
			}
			else{
				$invoiceId = $this->request->get('orderid');
				$transactionId = $this->request->get('txnid');
				$this->shop->authorizeTransaction($invoiceId, $transactionId);
				$this->response->redirect("shop/exit");
				return;
			}
			exit;
		}

		public function cancelAction(){
			$invoiceId = $this->request->get('orderid');
			$error = intval($this->request->get('error'));
			$this->shop->cancelTransaction($invoiceId, $error);

			$this->response->redirect("shop/cancelled");
			return;
		}
	}
}

