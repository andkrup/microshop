<?php
namespace controllers\admin{
	use supervillainhq\spectre\cms\CmsController;

	class IndexController extends CmsController{

		function initialize(){
			if(!$this->auth->isAdmin()){
				$this->response->redirect('/login');
				return;
			}
		}

		function indexAction(){
			$shop = $this->shop;
		}

		function woblerAction(){
			$this->requireHeadScripts(['jquery']);
		}

	}
}