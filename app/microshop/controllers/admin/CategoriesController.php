<?php
namespace controllers\admin{
	use supervillainhq\lexcorp\microshop\db\ProductMapper;
	use supervillainhq\spectre\cms\CmsController;

	class CategoriesController extends CmsController{
		function indexAction(){
			;
		}

		function editAction($id){
			$mapper = $this->di->getObjectmapper('category', ['id' => $id]);
			$category = $mapper->get();
			$this->view->setVar('category', $category);
		}

		function updateAction($id){
			$mapper = new ProductMapper(['id' => $id]);
			$product = $mapper->get();
			var_dump($product);exit;
			$this->requireHeadScripts(['jquery']);
		}

		function removeAction($id){
			$mapper = $this->di->getObjectmapper('category', ['id' => $id]);
			$category = $mapper->get();
			var_dump($category);exit;
			if($category->hasChildren()){
				throw new \Exception('category has children');
			}
		}

	}
}