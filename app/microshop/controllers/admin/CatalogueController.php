<?php
namespace controllers\admin{
	use supervillainhq\spectre\cms\CmsController;

	class CatalogueController extends CmsController{

		function initialize(){
			parent::initialize();
			$inventoryEnabled = $this->shop->inventoryEnabled();
			$navigation = $this->shop->catalog()->navigationType();
			$this->view->setVar('inventoryEnabled', $inventoryEnabled);
			$this->view->setVar('navigation', $navigation);
		}


		function indexAction(){
		}

		function productsAction(){
			$products = $this->di->getObjectmapper('product')->all();

			$this->view->setVar('products', $products);
		}

		function tagsAction(){
			$tags = $this->di->getObjectmapper('tag')->all();
			$this->view->setVar('tags', $tags);
		}
		function categoriesAction(){
			$this->requireHeadScripts(['jquery-treetable']);
			$this->requireStylesheets(['jquery-treetable-theme']);
			$categories = $this->di->getObjectmapper('category')->hierachical(true);
			$this->view->setVar('categories', $categories);
		}

		function optionsAction(){
		}

		function inventoryAction(){
			$inventory = $this->shop->inventory();
			$this->view->setVar('inventory', $inventory);
		}
	}
}