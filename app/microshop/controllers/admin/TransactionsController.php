<?php
namespace controllers\admin{
	use supervillainhq\lexcorp\microshop\payment\Transaction;
	use supervillainhq\spectre\cms\CmsController;

	class TransactionsController extends CmsController{


		function indexAction(){
			$mapper = $this->getDI()->getObjectmapper('transaction');
			$transactions = $mapper->all();
			$this->view->setVar('transactions', $transactions);
		}

		function viewAction($id){
			$mapper = $this->getDI()->getObjectmapper('transaction', ['id' => $id]);
			$transaction = $mapper->get();
			$this->view->setVar('transaction', $transaction);
		}

		function captureAction($id){
			$mapper = $this->getDI()->getObjectmapper('transaction', ['id' => $id]);
			$transaction = $mapper->get();

			Transaction::captureTransaction($transaction);
			$this->view->setVar('transaction', $transaction);
		}
	}
}