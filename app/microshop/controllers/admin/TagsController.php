<?php
namespace controllers\admin{
	use supervillainhq\spectre\cms\CmsController;

	class TagsController extends CmsController{

		function indexAction(){
		}

		function createAction(){
		}
		function storeAction(){
			$token = $this->security->checkToken();

			if($token && $this->request->isPost()){
				$tagInfo = (object) $this->request->getPost('tag');
				$writer = $this->di->getObjectwriter('tag', ['name' => $tagInfo->name]);
				$id = $writer->create();
				$this->response->redirect("/admin/tags/edit/{$id}");
				return;
			}
			$this->response->redirect("/admin/tags/create");
		}

		function editAction($id){
			$mapper = $this->di->getObjectmapper('tag', ['id' => $id]);
			$tag = $mapper->get();
			$this->view->setVar('tag', $tag);

			$mapper = $this->di->getObjectmapper('product', $tag);
			$products = $mapper->tagged(); // TODO: How do I know that there is a tagged() method?
			$this->view->setVar('products', $products);
		}

		function updateAction($id){
			$token = $this->security->checkToken();

			if($token && $this->request->isPost()){
				$tagInfo = (object) $this->request->getPost('tag');
				$writer = $this->di->getObjectwriter('tag', ['id' => $tagInfo->id, 'name' => $tagInfo->name]);
				$writer->update();
			}
			$this->response->redirect("/admin/tags/edit/{$id}");
			return;
		}

		function removeAction($id){
			$mapper = $this->di->getObjectwriter('tag', ['id' => $id]);
			$mapper->delete();
		}

	}
}