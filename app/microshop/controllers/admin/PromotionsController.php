<?php
namespace controllers\admin{
	use supervillainhq\lexcorp\microshop\Promotion;
	use supervillainhq\spectre\cms\CmsController;
	use supervillainhq\spectre\cms\Stylesheet;

	class PromotionsController extends CmsController{

		function createAction(){
			$this->requireHeadScripts(['jquery-datetimepicker']);
			$this->site->addStylesheet('jquery-ui', new Stylesheet('/css/jquery-ui.min.css'));
			$this->site->addStylesheet('lightslider', new Stylesheet('/css/jquery-ui-timepicker-addon.css'));
			$promotion = new Promotion();
			$availableProducts = $this->di->getObjectmapper('product')->all();
			$this->view->pick("promotions/edit");
			$this->view->setVar('promotion', $promotion);
			$this->view->setVar('availableProducts', $availableProducts);
		}

		function storeAction(){
			var_dump('create new promotion');exit;
		}

		function updateAction(){}
		function updateProductsAction(){
			var_dump('update products list');exit;
		}
	}
}