<?php
namespace controllers\admin{
	use supervillainhq\spectre\cms\CmsController;
	use supervillainhq\spectre\cms\assets\Images;

	class FilesController extends CmsController{
		private $menu;

		function initialize(){
			parent::initialize();
			$this->menu = [
					(object) ['url' => '/admin/files/upload', 'name' => 'Upload Image'],
			];
		}

		function indexAction(){
			$this->view->setVar('menu', $this->menu);

			$images = Images::imageAssets();
			$this->view->setVar('images', $images);
		}

		function uploadAction(){
			$token = $this->security->checkToken();

			if($token && $this->request->isPost()){
				$files = $this->request->getUploadedFiles();
				$assets = $this->uploader->handleUploads($files);
				var_dump($assets);
				var_dump($this->uploader->errors());
				exit;
			}
			$this->view->setVar('menu', $this->menu);
		}
	}
}