<?php
namespace controllers\admin{
	use supervillainhq\spectre\cms\CmsController;

	class InvoicesController extends CmsController{


		function indexAction(){
			$mapper = $this->getDI()->getObjectmapper('invoice');
			$invoices = $mapper->all();
			$this->view->setVar('invoices', $invoices);
		}

		function viewAction($id){
			$mapper = $this->getDI()->getObjectmapper('invoice', ['id' => $id]);
			$invoice = $mapper->get();
// 			var_dump($invoice);exit;
			$this->view->setVar('invoice', $invoice);
		}
	}
}