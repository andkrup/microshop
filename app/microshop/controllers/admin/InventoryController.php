<?php
namespace controllers\admin{
	use supervillainhq\spectre\cms\CmsController;

	class InventoryController extends CmsController{
		private $menu;

		function initialize(){
			parent::initialize();
			$this->menu = [
					(object) ['url' => '/admin/inventory', 'name' => 'Inventories'],
					(object) ['url' => '/admin/inventory/shipment_details', 'name' => 'Shipments'],
					(object) ['url' => '/admin/inventory/add_shipment', 'name' => 'Add Shipment'],
			];
		}

		function indexAction(){
			$this->view->setVar('menu', $this->menu);
		}

		function shipment_detailsAction(){
			$this->view->setVar('menu', $this->menu);

			$storages = $this->shop->inventory()->storages();
			$storedShipments = [];
			foreach ($storages as $storage){
				$shipments = $storage->shipments();
				$storedShipments[$storage->name()] = $shipments;
			}
			$this->view->setVar('storedShipments', $storedShipments);
		}

		/**
		 * Display form for creating new shipments
		 */
		function add_shipmentAction(){
			$this->view->setVar('menu', $this->menu);
		}
		/**
		 * Handle new shipment form submissions
		 */
		function create_shipmentAction(){
			if($this->request->isPost()){
				$shipment = $this->request->get('shipment');
				$this->shop->executeCommand('inventory/createshipment', $shipment);

				$this->response->redirect('admin/inventory/shipment_details');
			}
		}
	}
}