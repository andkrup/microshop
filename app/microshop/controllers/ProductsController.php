<?php
namespace controllers{
	use supervillainhq\spectre\cms\CmsController;

	class ProductsController extends CmsController{
		function initialize(){
			parent::initialize();
			$this->gettext->init();
		}

		function indexAction(){}

		function tagsAction($tags){
			$products;
			$catalog = $this->shop->catalog();
			if(strpos($tags, ',')){
				$tags = explode(',', $tags);
			}
			else{
				$tags = [$tags];
			}
			$tags = $catalog->getAnyTag($tags);
			if(count($tags) > 0){
				$partials = $tags;
				$catalogueItems = $catalog->navigator()->findCatalogueItems($tags);
// 			var_dump($tags);exit;
			}
			else{
				throw new \Exception('unknown tag');
			}
			$this->requireHeadScripts(['jquery']);

			$tokenKey = $this->security->getTokenKey();
			$token = $this->security->getToken();
			$session = $this->shop->getCheckoutSession();

			$this->view->setVar("tokenKey", $tokenKey);
			$this->view->setVar("token", $token);
			$this->view->setVar("session", $session);
			$this->view->setVar("partials", $partials);
			$this->view->setVar("catalogueItems", $catalogueItems);
		}

		function categoriesAction($category){
			$catalog = $this->shop->catalog();
			if(in_array($category, $catalog->categories())){

				var_dump($catalog);exit;
			}
			$this->requireHeadScripts(['jquery']);

			$tokenKey = $this->security->getTokenKey();
			$token = $this->security->getToken();
			$session = $this->shop->getCheckoutSession();

			$this->view->setVar("tokenKey", $tokenKey);
			$this->view->setVar("token", $token);
			$this->view->setVar("session", $session);
		}

	}
}