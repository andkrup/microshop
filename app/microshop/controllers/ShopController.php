<?php
namespace microshop\controllers{
	use supervillainhq\spectre\cms\CmsController;
	use Phalcon\Mvc\View;
	use supervillainhq\lexcorp\microshop\http\formvalidation\ValidationException;
	use supervillainhq\lexcorp\microshop\payment\epay\ErrorCode;
	use Phalcon\Validation\Message\Group;

	class ShopController extends CmsController{
		function initialize(){
			parent::initialize();
			$this->gettext->init();
		}

		function indexAction(){
			$this->requireHeadScripts(['jquery']);
		}

		function addItemAction(){
			$token = $this->security->checkToken();

			if($token && $this->request->isPost()){
				$productInfo = (object) $this->request->getPost('product');
				$product = $this->di->getObjectmapper('product', ['id' => $productInfo->id])->get();
				$this->cart->add($product, intval($productInfo->amount));
			}
			$this->response->redirect("products/tags/wobler");
		}

		function removeItemAction(){}

		function cartAction(){
			$this->requireHeadScripts(['jquery']);
			// TODO: let the cart-status dictate what view to display - redirect as we're doing in ordersAction()
		}

		function checkoutAction(){
			$this->requireHeadScripts(['jquery']);
			// TODO: let the cart-status dictate what view to display - redirect as we're doing in ordersAction()
		}

		function cancelledAction(){
			$session = $this->shop->getCheckoutSession();
			$errorCode = $session->errorCode();
			var_dump(ErrorCode::translate($errorCode));
			exit;
		}

		function exitAction(){
			$session = $this->shop->getCheckoutSession();
			$transaction = $session->transaction();
			$invoice = $this->di->getObjectmapper('invoice', ['transaction_id' => $transaction->id()])->find();
			$downloadUrl = $this->shop->getInvoiceURI($invoice);
			$this->shop->mailInvoiceToUser($invoice, $downloadUrl);
			var_dump($downloadUrl);
			exit;

			$invoiceId =
			$invoice->id('terms', $invoiceId);
			$this->session->set('invoice', $invoice);

			$session->destroy();
			$this->cart->destroy();
		}

		function pdfAction($hash){
			$invoice = $this->di->getObjectmapper('invoice', ['hash' => trim($hash)])->find();
			$filename = "{$hash}.pdf";
			$view = clone $this->view;
			$view->start();
			$view->disableLevel(View::LEVEL_MAIN_LAYOUT);
			$view->setVar("invoice", $invoice);
			$view->render('shop', 'pdf');
			$view->finish();
			$content = $view->getContent();

			$this->pdf->load_html($content);
			$this->pdf->render();
			$this->pdf->stream($filename);
			exit; // stop before the framework adds more bytes to the pdf stream
		}

		function orderAction(){
			// handle the check-out flow from start to finish
			if($this->request->isPost()){
				$checkoutStep = $this->request->get('next_checkout_step'); // TODO: $checkoutStep is only for testing. Use cart-status to handle redirection
				switch ($checkoutStep){
					case 'input-addresses':
						// request contact information and address user-input
						$this->shop->getCheckoutSession(); // creates a new session if none exists
						$this->response->redirect("shop/addresses");
						break;
					case 'select-payment':
						// update contact information, invoice and delivery addresses and request payment options user-input
						// fetch or create a shopper entity for this session
						$session = $this->shop->getCheckoutSession();
						try{
							$session->updateContactInformation($this->request);
							$session->updateInvoiceAddress($this->request);
							$session->updateDeliveryAddress($this->request);
						}
						catch(ValidationException $validationException){
							$this->handleValidationErrors($validationException->messageGroup(), 'shop/addresses');
							return;
						}
						$this->response->redirect("shop/delivery");
						break;
					case 'select-delivery':
						// set chosen delivery options
						$session = $this->shop->getCheckoutSession();
						$session->updateDeliveryService($this->request);
						$this->response->redirect("shop/payment");
						break;
					case 'place-order':
						// finalize cart-contents, handle payment options and initiate payment
						$session = $this->shop->getCheckoutSession();
						$session->updateLegal($this->request);
						$amount = $session->cart()->total() + $session->deliveryService()->price();
						// create an object for handling future state-changes to the transaction
						$transaction = $this->shop->initTransaction($amount);
						// save session and stop further meddling with it
						$session->finalize($transaction);
						// redirect user to payment window and wait for closure on payment
						$this->response->redirect($this->shop->getPaymentWindowURL($transaction), true);
						break;
					case 'payment-success':
						// payment was successful
						$this->response->redirect("shop/invoice");
						break;
					case 'payment-failed':
						// payment was not successful, alert user
						$this->response->redirect("shop/payment?r=fail");
						break;
				}
			}
		}
		private function handleValidationErrors(Group $messages, $redirectUri){
			$text = '';
			foreach ($messages as $message){
				$text .= "{$message}";
			}
			$this->flash->error($text);
			$this->response->redirect($redirectUri);
		}

		function addressesAction(){
			// TODO: let the cart-status dictate what view to display - redirect as we're doing in ordersAction()
			$this->requireHeadScripts(['jquery']);
			// fetch or create a shopper entity for this session
			$entity = $this->shop->currentVisitor();
			$this->view->setVar("shopper", $entity);
		}

		function deliveryAction(){
			// TODO: let the cart-status dictate what view to display - redirect as we're doing in ordersAction()
			$this->requireHeadScripts(['jquery']);
			// fetch or create a shopper entity for this session
			$entity = $this->shop->currentVisitor();
			$this->view->setVar("shopper", $entity);
			$services = $this->shop->deliveryServices();
			$this->view->setVar("services", $services);
		}

		function paymentAction(){
			// TODO: let the cart-status dictate what view to display - redirect as we're doing in ordersAction()
			$this->requireHeadScripts(['jquery']);
			// fetch or create a shopper entity for this session
			$entity = $this->shop->currentVisitor();
			$this->view->setVar("shopper", $entity);
		}

		function invoiceAction(){
			$this->requireHeadScripts(['jquery']);
			// TODO: let the cart-status dictate what view to display - redirect as we're doing in ordersAction()
		}
	}
}