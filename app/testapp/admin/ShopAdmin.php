<?php
namespace testapp\admin{
	use Phalcon\Mvc\ModuleDefinitionInterface;
	use Phalcon\Loader;
	use Phalcon\Mvc\View;
	use Phalcon\Mvc\Dispatcher;
	use Phalcon\DiInterface;

	class TobissenAdmin implements ModuleDefinitionInterface{
		public function registerAutoloaders(DiInterface $dependencyInjector = null){
			$loader = new Loader();
			$loader->registerNamespaces([
					'admin\controllers' => '../app/admin/controllers/',
			]);
			$loader->register();
		}
		public function registerServices(DiInterface $dependencyInjector){
			//Registering a dispatcher
			$dependencyInjector->set('dispatcher', function() {
				$dispatcher = new Dispatcher();
				$dispatcher->setDefaultNamespace('admin\controllers');
				return $dispatcher;
			});

			//Registering the view component
			$dependencyInjector->set('view', function() {
				$view = new View();
				$view->setViewsDir('../app/admin/views/');
				return $view;
			});
		}
	}
}