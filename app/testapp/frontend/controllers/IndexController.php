<?php

namespace frontend\controllers {
	use supervillainhq\spectre\cms\controllers\CmsController;

	/**
	 * Created by ak.
	 */
	class IndexController extends CmsController {
		function indexAction(){
			$this->requireHeadScripts(['jquery']);
		}
	}
}
