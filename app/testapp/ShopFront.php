<?php
namespace testapp{
	use Phalcon\Mvc\ModuleDefinitionInterface;
	use Phalcon\Loader;
	use Phalcon\Mvc\View;
	use Phalcon\Mvc\Dispatcher;
	use Phalcon\DiInterface;

	class ShopFront implements ModuleDefinitionInterface{
		private $session;

		function __construct(){
		}

		public function registerAutoloaders(DiInterface $dependencyInjector=null){
			$loader = new Loader();
			$loader->registerNamespaces([
					'frontend\controllers' => '../app/testapp/frontend/controllers/',
			]);
			$loader->register();
		}
		public function registerServices(DiInterface $dependencyInjector){
			//Registering a dispatcher
			$dependencyInjector->set('dispatcher', function() {
				$dispatcher = new Dispatcher();
				$dispatcher->setDefaultNamespace('frontend\controllers');
				return $dispatcher;
			});

			//Registering the view component
			$dependencyInjector->set('view', function() {
				$view = new View();
				$view->setViewsDir('../app/testapp/frontend/views/');
				return $view;
			});
		}
	}
}