<?php
namespace testapp\api{
	use Phalcon\Mvc\ModuleDefinitionInterface;
	use Phalcon\Loader;
	use Phalcon\Mvc\View;
	use Phalcon\Mvc\Dispatcher;

	class ShopAPI implements ModuleDefinitionInterface{
		private $session;

		function __construct(){
		}

		public function registerAutoloaders(\Phalcon\DiInterface $dependencyInjector=null){
			$loader = new Loader();
			$loader->registerNamespaces([
					'controllers\api' => '../app/controllers/api/',
			]);
			$loader->register();
		}
		public function registerServices(\Phalcon\DiInterface $dependencyInjector){
			//Registering a dispatcher
			$dependencyInjector->set('dispatcher', function() {
				$dispatcher = new Dispatcher();
				$dispatcher->setDefaultNamespace("controllers\api");
				return $dispatcher;
			});

			//Registering the view component
			$dependencyInjector->set('view', function() {
				$view = new View();
				$view->setViewsDir('../app/views/');
				return $view;
			});
		}
	}
}