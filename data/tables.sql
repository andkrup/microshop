# ************************************************************
# Sequel Pro SQL dump
# Version 4500
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.28-0ubuntu0.15.04.1)
# Database: Tobissen
# Generation Time: 2016-02-07 23:01:21 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table cms_Addresses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_Addresses`;

CREATE TABLE `cms_Addresses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) DEFAULT NULL,
  `attention` varchar(255) DEFAULT NULL,
  `address1` varchar(255) NOT NULL DEFAULT '',
  `address2` varchar(255) DEFAULT NULL,
  `postalcode` varchar(16) NOT NULL DEFAULT '',
  `city` varchar(128) DEFAULT NULL,
  `county` varchar(128) DEFAULT NULL,
  `region` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cms_AssetLocations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_AssetLocations`;

CREATE TABLE `cms_AssetLocations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) unsigned NOT NULL,
  `location` varchar(255) NOT NULL DEFAULT '',
  `type` enum('normal','retina','responsive') NOT NULL DEFAULT 'normal',
  PRIMARY KEY (`id`),
  UNIQUE KEY `image_id` (`asset_id`,`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cms_Assets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_Assets`;

CREATE TABLE `cms_Assets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mimetype_id` int(11) unsigned NOT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `src` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `mimetype_fk` (`mimetype_id`),
  CONSTRAINT `mimetype_fk` FOREIGN KEY (`mimetype_id`) REFERENCES `cms_Mimetypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cms_Comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_Comments`;

CREATE TABLE `cms_Comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `parent_id` int(11) unsigned NOT NULL,
  `text` text NOT NULL,
  `created` datetime NOT NULL,
  `created_from` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_fk` (`user_id`),
  KEY `parent_fk` (`parent_id`),
  CONSTRAINT `cms_Comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `cms_Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cms_Comments_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `cms_Comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cms_Editors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_Editors`;

CREATE TABLE `cms_Editors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `bootstrap` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cms_Editors` WRITE;
/*!40000 ALTER TABLE `cms_Editors` DISABLE KEYS */;

INSERT INTO `cms_Editors` (`id`, `name`, `bootstrap`)
VALUES
	(1,'default image editor','');

/*!40000 ALTER TABLE `cms_Editors` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cms_EditorTypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_EditorTypes`;

CREATE TABLE `cms_EditorTypes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  `editor_id` int(11) unsigned DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `editor_fk` (`editor_id`),
  CONSTRAINT `cms_EditorTypes_ibfk_1` FOREIGN KEY (`editor_id`) REFERENCES `cms_Editors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cms_EditorTypes` WRITE;
/*!40000 ALTER TABLE `cms_EditorTypes` DISABLE KEYS */;

INSERT INTO `cms_EditorTypes` (`id`, `name`, `editor_id`, `description`)
VALUES
	(1,'image',1,'An image');

/*!40000 ALTER TABLE `cms_EditorTypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cms_MenuItems
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_MenuItems`;

CREATE TABLE `cms_MenuItems` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) unsigned NOT NULL,
  `page_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cms_Menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_Menus`;

CREATE TABLE `cms_Menus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cms_Mimetypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_Mimetypes`;

CREATE TABLE `cms_Mimetypes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `editortype_id` int(11) unsigned NOT NULL,
  `extension` varchar(32) NOT NULL DEFAULT '',
  `mimetype` varchar(128) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `editortype_fk` (`editortype_id`),
  CONSTRAINT `editortype_fk` FOREIGN KEY (`editortype_id`) REFERENCES `cms_EditorTypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cms_Mimetypes` WRITE;
/*!40000 ALTER TABLE `cms_Mimetypes` DISABLE KEYS */;

INSERT INTO `cms_Mimetypes` (`id`, `editortype_id`, `extension`, `mimetype`)
VALUES
	(1,1,'jpg','image/jpeg'),
	(2,1,'jpeg','image/jpeg'),
	(3,1,'gif','image/gif'),
	(4,1,'png','image/png');

/*!40000 ALTER TABLE `cms_Mimetypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cms_Pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_Pages`;

CREATE TABLE `cms_Pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  `title` varchar(128) DEFAULT '',
  `slug` varchar(255) NOT NULL DEFAULT '',
  `template_id` int(11) unsigned DEFAULT NULL,
  `section_manager` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cms_Pages` WRITE;
/*!40000 ALTER TABLE `cms_Pages` DISABLE KEYS */;

INSERT INTO `cms_Pages` (`id`, `name`, `title`, `slug`, `template_id`, `section_manager`)
VALUES
	(1,'Do it yourself','Do It Yourself','do-it-yourself',1,NULL);

/*!40000 ALTER TABLE `cms_Pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cms_PageSections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_PageSections`;

CREATE TABLE `cms_PageSections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) unsigned NOT NULL,
  `section_id` int(11) unsigned NOT NULL,
  `display_order` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cms_Privileges
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_Privileges`;

CREATE TABLE `cms_Privileges` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cms_Privileges` WRITE;
/*!40000 ALTER TABLE `cms_Privileges` DISABLE KEYS */;

INSERT INTO `cms_Privileges` (`id`, `name`, `description`)
VALUES
	(1,'administrator','Site administrator'),
	(2,'commentator','User is allowed to add comments'),
	(3,'contributor','User is allowed to add posts');

/*!40000 ALTER TABLE `cms_Privileges` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cms_RequestAssets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_RequestAssets`;

CREATE TABLE `cms_RequestAssets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `route` varchar(128) NOT NULL DEFAULT '/',
  `asset_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `asset_id` (`asset_id`),
  KEY `route` (`route`),
  CONSTRAINT `asset_id` FOREIGN KEY (`asset_id`) REFERENCES `cms_Assets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cms_RotatingPageSections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_RotatingPageSections`;

CREATE TABLE `cms_RotatingPageSections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `recurring` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cms_Sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_Sections`;

CREATE TABLE `cms_Sections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cms_Settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_Settings`;

CREATE TABLE `cms_Settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(64) NOT NULL DEFAULT '',
  `default_values` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cms_Settings` WRITE;
/*!40000 ALTER TABLE `cms_Settings` DISABLE KEYS */;

INSERT INTO `cms_Settings` (`id`, `title`, `default_values`)
VALUES
	(1,'timezone','Europe/Copenhagen'),
	(2,'language','en_US.utf8');

/*!40000 ALTER TABLE `cms_Settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cms_Templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_Templates`;

CREATE TABLE `cms_Templates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  `filepath` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cms_Templates` WRITE;
/*!40000 ALTER TABLE `cms_Templates` DISABLE KEYS */;





# Dump of table cms_UserEmails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_UserEmails`;

CREATE TABLE `cms_UserEmails` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `email` varchar(128) NOT NULL,
  `primary` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`email`),
  CONSTRAINT `cms_UserEmails_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `cms_Users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cms_UserEmails` WRITE;
/*!40000 ALTER TABLE `cms_UserEmails` DISABLE KEYS */;

INSERT INTO `cms_UserEmails` (`id`, `user_id`, `email`, `primary`)
VALUES
	(2,1,'andkrup@gmail.com',1),
	(3,2,'ak@fitnessworld.dk',1);

/*!40000 ALTER TABLE `cms_UserEmails` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cms_UserPrivileges
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_UserPrivileges`;

CREATE TABLE `cms_UserPrivileges` (
  `user_id` int(11) unsigned NOT NULL,
  `privilege_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`privilege_id`),
  KEY `privilege_fk` (`privilege_id`),
  KEY `user_id` (`user_id`,`privilege_id`),
  CONSTRAINT `privilege_fk` FOREIGN KEY (`privilege_id`) REFERENCES `cms_Privileges` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_fk` FOREIGN KEY (`user_id`) REFERENCES `cms_Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cms_UserPrivileges` WRITE;
/*!40000 ALTER TABLE `cms_UserPrivileges` DISABLE KEYS */;

INSERT INTO `cms_UserPrivileges` (`user_id`, `privilege_id`)
VALUES
	(1,1),
	(2,1);

/*!40000 ALTER TABLE `cms_UserPrivileges` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cms_Users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_Users`;

CREATE TABLE `cms_Users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL DEFAULT '',
  `passhash` varchar(128) NOT NULL DEFAULT '',
  `activated` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cms_Users` WRITE;
/*!40000 ALTER TABLE `cms_Users` DISABLE KEYS */;

INSERT INTO `cms_Users` (`id`, `username`, `passhash`, `activated`)
VALUES
	(1,'andkrup','$2a$10$IajriXZYfiMYHRZA0nfIQewB2vkgnvYNdK7rUn9uaEcHi5ys0LSMq',1),
	(2,'nimda','$2a$10$IajriXZYfiMYHRZA0nfIQewB2vkgnvYNdK7rUn9uaEcHi5ys0LSMq',1);

/*!40000 ALTER TABLE `cms_Users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cms_UserSettings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_UserSettings`;

CREATE TABLE `cms_UserSettings` (
  `user_id` int(11) unsigned NOT NULL,
  `setting_id` int(11) unsigned NOT NULL,
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`setting_id`),
  KEY `user_id` (`user_id`,`setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Colors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Colors`;

CREATE TABLE `Colors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Colors` WRITE;
/*!40000 ALTER TABLE `Colors` DISABLE KEYS */;

INSERT INTO `Colors` (`id`, `name`, `description`)
VALUES
	(1,'green','Grøn ryg, hvid mave'),
	(2,'black','Sort ryg, hvid mave'),
	(3,'orange','Orange ryg, hvid mave');

/*!40000 ALTER TABLE `Colors` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Country
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Country`;

CREATE TABLE `Country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `Country` WRITE;
/*!40000 ALTER TABLE `Country` DISABLE KEYS */;

INSERT INTO `Country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`)
VALUES
	(1,'AF','AFGHANISTAN','Afghanistan','AFG',4,93),
	(2,'AL','ALBANIA','Albania','ALB',8,355),
	(3,'DZ','ALGERIA','Algeria','DZA',12,213),
	(4,'AS','AMERICAN SAMOA','American Samoa','ASM',16,1684),
	(5,'AD','ANDORRA','Andorra','AND',20,376),
	(6,'AO','ANGOLA','Angola','AGO',24,244),
	(7,'AI','ANGUILLA','Anguilla','AIA',660,1264),
	(8,'AQ','ANTARCTICA','Antarctica',NULL,NULL,0),
	(9,'AG','ANTIGUA AND BARBUDA','Antigua and Barbuda','ATG',28,1268),
	(10,'AR','ARGENTINA','Argentina','ARG',32,54),
	(11,'AM','ARMENIA','Armenia','ARM',51,374),
	(12,'AW','ARUBA','Aruba','ABW',533,297),
	(13,'AU','AUSTRALIA','Australia','AUS',36,61),
	(14,'AT','AUSTRIA','Austria','AUT',40,43),
	(15,'AZ','AZERBAIJAN','Azerbaijan','AZE',31,994),
	(16,'BS','BAHAMAS','Bahamas','BHS',44,1242),
	(17,'BH','BAHRAIN','Bahrain','BHR',48,973),
	(18,'BD','BANGLADESH','Bangladesh','BGD',50,880),
	(19,'BB','BARBADOS','Barbados','BRB',52,1246),
	(20,'BY','BELARUS','Belarus','BLR',112,375),
	(21,'BE','BELGIUM','Belgium','BEL',56,32),
	(22,'BZ','BELIZE','Belize','BLZ',84,501),
	(23,'BJ','BENIN','Benin','BEN',204,229),
	(24,'BM','BERMUDA','Bermuda','BMU',60,1441),
	(25,'BT','BHUTAN','Bhutan','BTN',64,975),
	(26,'BO','BOLIVIA','Bolivia','BOL',68,591),
	(27,'BA','BOSNIA AND HERZEGOVINA','Bosnia and Herzegovina','BIH',70,387),
	(28,'BW','BOTSWANA','Botswana','BWA',72,267),
	(29,'BV','BOUVET ISLAND','Bouvet Island',NULL,NULL,0),
	(30,'BR','BRAZIL','Brazil','BRA',76,55),
	(31,'IO','BRITISH INDIAN OCEAN TERRITORY','British Indian Ocean Territory',NULL,NULL,246),
	(32,'BN','BRUNEI DARUSSALAM','Brunei Darussalam','BRN',96,673),
	(33,'BG','BULGARIA','Bulgaria','BGR',100,359),
	(34,'BF','BURKINA FASO','Burkina Faso','BFA',854,226),
	(35,'BI','BURUNDI','Burundi','BDI',108,257),
	(36,'KH','CAMBODIA','Cambodia','KHM',116,855),
	(37,'CM','CAMEROON','Cameroon','CMR',120,237),
	(38,'CA','CANADA','Canada','CAN',124,1),
	(39,'CV','CAPE VERDE','Cape Verde','CPV',132,238),
	(40,'KY','CAYMAN ISLANDS','Cayman Islands','CYM',136,1345),
	(41,'CF','CENTRAL AFRICAN REPUBLIC','Central African Republic','CAF',140,236),
	(42,'TD','CHAD','Chad','TCD',148,235),
	(43,'CL','CHILE','Chile','CHL',152,56),
	(44,'CN','CHINA','China','CHN',156,86),
	(45,'CX','CHRISTMAS ISLAND','Christmas Island',NULL,NULL,61),
	(46,'CC','COCOS (KEELING) ISLANDS','Cocos (Keeling) Islands',NULL,NULL,672),
	(47,'CO','COLOMBIA','Colombia','COL',170,57),
	(48,'KM','COMOROS','Comoros','COM',174,269),
	(49,'CG','CONGO','Congo','COG',178,242),
	(50,'CD','CONGO, THE DEMOCRATIC REPUBLIC OF THE','Congo, the Democratic Republic of the','COD',180,242),
	(51,'CK','COOK ISLANDS','Cook Islands','COK',184,682),
	(52,'CR','COSTA RICA','Costa Rica','CRI',188,506),
	(53,'CI','COTE D\'IVOIRE','Cote D\'Ivoire','CIV',384,225),
	(54,'HR','CROATIA','Croatia','HRV',191,385),
	(55,'CU','CUBA','Cuba','CUB',192,53),
	(56,'CY','CYPRUS','Cyprus','CYP',196,357),
	(57,'CZ','CZECH REPUBLIC','Czech Republic','CZE',203,420),
	(58,'DK','DENMARK','Denmark','DNK',208,45),
	(59,'DJ','DJIBOUTI','Djibouti','DJI',262,253),
	(60,'DM','DOMINICA','Dominica','DMA',212,1767),
	(61,'DO','DOMINICAN REPUBLIC','Dominican Republic','DOM',214,1809),
	(62,'EC','ECUADOR','Ecuador','ECU',218,593),
	(63,'EG','EGYPT','Egypt','EGY',818,20),
	(64,'SV','EL SALVADOR','El Salvador','SLV',222,503),
	(65,'GQ','EQUATORIAL GUINEA','Equatorial Guinea','GNQ',226,240),
	(66,'ER','ERITREA','Eritrea','ERI',232,291),
	(67,'EE','ESTONIA','Estonia','EST',233,372),
	(68,'ET','ETHIOPIA','Ethiopia','ETH',231,251),
	(69,'FK','FALKLAND ISLANDS (MALVINAS)','Falkland Islands (Malvinas)','FLK',238,500),
	(70,'FO','FAROE ISLANDS','Faroe Islands','FRO',234,298),
	(71,'FJ','FIJI','Fiji','FJI',242,679),
	(72,'FI','FINLAND','Finland','FIN',246,358),
	(73,'FR','FRANCE','France','FRA',250,33),
	(74,'GF','FRENCH GUIANA','French Guiana','GUF',254,594),
	(75,'PF','FRENCH POLYNESIA','French Polynesia','PYF',258,689),
	(76,'TF','FRENCH SOUTHERN TERRITORIES','French Southern Territories',NULL,NULL,0),
	(77,'GA','GABON','Gabon','GAB',266,241),
	(78,'GM','GAMBIA','Gambia','GMB',270,220),
	(79,'GE','GEORGIA','Georgia','GEO',268,995),
	(80,'DE','GERMANY','Germany','DEU',276,49),
	(81,'GH','GHANA','Ghana','GHA',288,233),
	(82,'GI','GIBRALTAR','Gibraltar','GIB',292,350),
	(83,'GR','GREECE','Greece','GRC',300,30),
	(84,'GL','GREENLAND','Greenland','GRL',304,299),
	(85,'GD','GRENADA','Grenada','GRD',308,1473),
	(86,'GP','GUADELOUPE','Guadeloupe','GLP',312,590),
	(87,'GU','GUAM','Guam','GUM',316,1671),
	(88,'GT','GUATEMALA','Guatemala','GTM',320,502),
	(89,'GN','GUINEA','Guinea','GIN',324,224),
	(90,'GW','GUINEA-BISSAU','Guinea-Bissau','GNB',624,245),
	(91,'GY','GUYANA','Guyana','GUY',328,592),
	(92,'HT','HAITI','Haiti','HTI',332,509),
	(93,'HM','HEARD ISLAND AND MCDONALD ISLANDS','Heard Island and Mcdonald Islands',NULL,NULL,0),
	(94,'VA','HOLY SEE (VATICAN CITY STATE)','Holy See (Vatican City State)','VAT',336,39),
	(95,'HN','HONDURAS','Honduras','HND',340,504),
	(96,'HK','HONG KONG','Hong Kong','HKG',344,852),
	(97,'HU','HUNGARY','Hungary','HUN',348,36),
	(98,'IS','ICELAND','Iceland','ISL',352,354),
	(99,'IN','INDIA','India','IND',356,91),
	(100,'ID','INDONESIA','Indonesia','IDN',360,62),
	(101,'IR','IRAN, ISLAMIC REPUBLIC OF','Iran, Islamic Republic of','IRN',364,98),
	(102,'IQ','IRAQ','Iraq','IRQ',368,964),
	(103,'IE','IRELAND','Ireland','IRL',372,353),
	(104,'IL','ISRAEL','Israel','ISR',376,972),
	(105,'IT','ITALY','Italy','ITA',380,39),
	(106,'JM','JAMAICA','Jamaica','JAM',388,1876),
	(107,'JP','JAPAN','Japan','JPN',392,81),
	(108,'JO','JORDAN','Jordan','JOR',400,962),
	(109,'KZ','KAZAKHSTAN','Kazakhstan','KAZ',398,7),
	(110,'KE','KENYA','Kenya','KEN',404,254),
	(111,'KI','KIRIBATI','Kiribati','KIR',296,686),
	(112,'KP','KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF','Korea, Democratic People\'s Republic of','PRK',408,850),
	(113,'KR','KOREA, REPUBLIC OF','Korea, Republic of','KOR',410,82),
	(114,'KW','KUWAIT','Kuwait','KWT',414,965),
	(115,'KG','KYRGYZSTAN','Kyrgyzstan','KGZ',417,996),
	(116,'LA','LAO PEOPLE\'S DEMOCRATIC REPUBLIC','Lao People\'s Democratic Republic','LAO',418,856),
	(117,'LV','LATVIA','Latvia','LVA',428,371),
	(118,'LB','LEBANON','Lebanon','LBN',422,961),
	(119,'LS','LESOTHO','Lesotho','LSO',426,266),
	(120,'LR','LIBERIA','Liberia','LBR',430,231),
	(121,'LY','LIBYAN ARAB JAMAHIRIYA','Libyan Arab Jamahiriya','LBY',434,218),
	(122,'LI','LIECHTENSTEIN','Liechtenstein','LIE',438,423),
	(123,'LT','LITHUANIA','Lithuania','LTU',440,370),
	(124,'LU','LUXEMBOURG','Luxembourg','LUX',442,352),
	(125,'MO','MACAO','Macao','MAC',446,853),
	(126,'MK','MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','Macedonia, the Former Yugoslav Republic of','MKD',807,389),
	(127,'MG','MADAGASCAR','Madagascar','MDG',450,261),
	(128,'MW','MALAWI','Malawi','MWI',454,265),
	(129,'MY','MALAYSIA','Malaysia','MYS',458,60),
	(130,'MV','MALDIVES','Maldives','MDV',462,960),
	(131,'ML','MALI','Mali','MLI',466,223),
	(132,'MT','MALTA','Malta','MLT',470,356),
	(133,'MH','MARSHALL ISLANDS','Marshall Islands','MHL',584,692),
	(134,'MQ','MARTINIQUE','Martinique','MTQ',474,596),
	(135,'MR','MAURITANIA','Mauritania','MRT',478,222),
	(136,'MU','MAURITIUS','Mauritius','MUS',480,230),
	(137,'YT','MAYOTTE','Mayotte',NULL,NULL,269),
	(138,'MX','MEXICO','Mexico','MEX',484,52),
	(139,'FM','MICRONESIA, FEDERATED STATES OF','Micronesia, Federated States of','FSM',583,691),
	(140,'MD','MOLDOVA, REPUBLIC OF','Moldova, Republic of','MDA',498,373),
	(141,'MC','MONACO','Monaco','MCO',492,377),
	(142,'MN','MONGOLIA','Mongolia','MNG',496,976),
	(143,'MS','MONTSERRAT','Montserrat','MSR',500,1664),
	(144,'MA','MOROCCO','Morocco','MAR',504,212),
	(145,'MZ','MOZAMBIQUE','Mozambique','MOZ',508,258),
	(146,'MM','MYANMAR','Myanmar','MMR',104,95),
	(147,'NA','NAMIBIA','Namibia','NAM',516,264),
	(148,'NR','NAURU','Nauru','NRU',520,674),
	(149,'NP','NEPAL','Nepal','NPL',524,977),
	(150,'NL','NETHERLANDS','Netherlands','NLD',528,31),
	(151,'AN','NETHERLANDS ANTILLES','Netherlands Antilles','ANT',530,599),
	(152,'NC','NEW CALEDONIA','New Caledonia','NCL',540,687),
	(153,'NZ','NEW ZEALAND','New Zealand','NZL',554,64),
	(154,'NI','NICARAGUA','Nicaragua','NIC',558,505),
	(155,'NE','NIGER','Niger','NER',562,227),
	(156,'NG','NIGERIA','Nigeria','NGA',566,234),
	(157,'NU','NIUE','Niue','NIU',570,683),
	(158,'NF','NORFOLK ISLAND','Norfolk Island','NFK',574,672),
	(159,'MP','NORTHERN MARIANA ISLANDS','Northern Mariana Islands','MNP',580,1670),
	(160,'NO','NORWAY','Norway','NOR',578,47),
	(161,'OM','OMAN','Oman','OMN',512,968),
	(162,'PK','PAKISTAN','Pakistan','PAK',586,92),
	(163,'PW','PALAU','Palau','PLW',585,680),
	(164,'PS','PALESTINIAN TERRITORY, OCCUPIED','Palestinian Territory, Occupied',NULL,NULL,970),
	(165,'PA','PANAMA','Panama','PAN',591,507),
	(166,'PG','PAPUA NEW GUINEA','Papua New Guinea','PNG',598,675),
	(167,'PY','PARAGUAY','Paraguay','PRY',600,595),
	(168,'PE','PERU','Peru','PER',604,51),
	(169,'PH','PHILIPPINES','Philippines','PHL',608,63),
	(170,'PN','PITCAIRN','Pitcairn','PCN',612,0),
	(171,'PL','POLAND','Poland','POL',616,48),
	(172,'PT','PORTUGAL','Portugal','PRT',620,351),
	(173,'PR','PUERTO RICO','Puerto Rico','PRI',630,1787),
	(174,'QA','QATAR','Qatar','QAT',634,974),
	(175,'RE','REUNION','Reunion','REU',638,262),
	(176,'RO','ROMANIA','Romania','ROM',642,40),
	(177,'RU','RUSSIAN FEDERATION','Russian Federation','RUS',643,70),
	(178,'RW','RWANDA','Rwanda','RWA',646,250),
	(179,'SH','SAINT HELENA','Saint Helena','SHN',654,290),
	(180,'KN','SAINT KITTS AND NEVIS','Saint Kitts and Nevis','KNA',659,1869),
	(181,'LC','SAINT LUCIA','Saint Lucia','LCA',662,1758),
	(182,'PM','SAINT PIERRE AND MIQUELON','Saint Pierre and Miquelon','SPM',666,508),
	(183,'VC','SAINT VINCENT AND THE GRENADINES','Saint Vincent and the Grenadines','VCT',670,1784),
	(184,'WS','SAMOA','Samoa','WSM',882,684),
	(185,'SM','SAN MARINO','San Marino','SMR',674,378),
	(186,'ST','SAO TOME AND PRINCIPE','Sao Tome and Principe','STP',678,239),
	(187,'SA','SAUDI ARABIA','Saudi Arabia','SAU',682,966),
	(188,'SN','SENEGAL','Senegal','SEN',686,221),
	(189,'CS','SERBIA AND MONTENEGRO','Serbia and Montenegro',NULL,NULL,381),
	(190,'SC','SEYCHELLES','Seychelles','SYC',690,248),
	(191,'SL','SIERRA LEONE','Sierra Leone','SLE',694,232),
	(192,'SG','SINGAPORE','Singapore','SGP',702,65),
	(193,'SK','SLOVAKIA','Slovakia','SVK',703,421),
	(194,'SI','SLOVENIA','Slovenia','SVN',705,386),
	(195,'SB','SOLOMON ISLANDS','Solomon Islands','SLB',90,677),
	(196,'SO','SOMALIA','Somalia','SOM',706,252),
	(197,'ZA','SOUTH AFRICA','South Africa','ZAF',710,27),
	(198,'GS','SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','South Georgia and the South Sandwich Islands',NULL,NULL,0),
	(199,'ES','SPAIN','Spain','ESP',724,34),
	(200,'LK','SRI LANKA','Sri Lanka','LKA',144,94),
	(201,'SD','SUDAN','Sudan','SDN',736,249),
	(202,'SR','SURINAME','Suriname','SUR',740,597),
	(203,'SJ','SVALBARD AND JAN MAYEN','Svalbard and Jan Mayen','SJM',744,47),
	(204,'SZ','SWAZILAND','Swaziland','SWZ',748,268),
	(205,'SE','SWEDEN','Sweden','SWE',752,46),
	(206,'CH','SWITZERLAND','Switzerland','CHE',756,41),
	(207,'SY','SYRIAN ARAB REPUBLIC','Syrian Arab Republic','SYR',760,963),
	(208,'TW','TAIWAN, PROVINCE OF CHINA','Taiwan, Province of China','TWN',158,886),
	(209,'TJ','TAJIKISTAN','Tajikistan','TJK',762,992),
	(210,'TZ','TANZANIA, UNITED REPUBLIC OF','Tanzania, United Republic of','TZA',834,255),
	(211,'TH','THAILAND','Thailand','THA',764,66),
	(212,'TL','TIMOR-LESTE','Timor-Leste',NULL,NULL,670),
	(213,'TG','TOGO','Togo','TGO',768,228),
	(214,'TK','TOKELAU','Tokelau','TKL',772,690),
	(215,'TO','TONGA','Tonga','TON',776,676),
	(216,'TT','TRINIDAD AND TOBAGO','Trinidad and Tobago','TTO',780,1868),
	(217,'TN','TUNISIA','Tunisia','TUN',788,216),
	(218,'TR','TURKEY','Turkey','TUR',792,90),
	(219,'TM','TURKMENISTAN','Turkmenistan','TKM',795,7370),
	(220,'TC','TURKS AND CAICOS ISLANDS','Turks and Caicos Islands','TCA',796,1649),
	(221,'TV','TUVALU','Tuvalu','TUV',798,688),
	(222,'UG','UGANDA','Uganda','UGA',800,256),
	(223,'UA','UKRAINE','Ukraine','UKR',804,380),
	(224,'AE','UNITED ARAB EMIRATES','United Arab Emirates','ARE',784,971),
	(225,'GB','UNITED KINGDOM','United Kingdom','GBR',826,44),
	(226,'US','UNITED STATES','United States','USA',840,1),
	(227,'UM','UNITED STATES MINOR OUTLYING ISLANDS','United States Minor Outlying Islands',NULL,NULL,1),
	(228,'UY','URUGUAY','Uruguay','URY',858,598),
	(229,'UZ','UZBEKISTAN','Uzbekistan','UZB',860,998),
	(230,'VU','VANUATU','Vanuatu','VUT',548,678),
	(231,'VE','VENEZUELA','Venezuela','VEN',862,58),
	(232,'VN','VIET NAM','Viet Nam','VNM',704,84),
	(233,'VG','VIRGIN ISLANDS, BRITISH','Virgin Islands, British','VGB',92,1284),
	(234,'VI','VIRGIN ISLANDS, U.S.','Virgin Islands, U.s.','VIR',850,1340),
	(235,'WF','WALLIS AND FUTUNA','Wallis and Futuna','WLF',876,681),
	(236,'EH','WESTERN SAHARA','Western Sahara','ESH',732,212),
	(237,'YE','YEMEN','Yemen','YEM',887,967),
	(238,'ZM','ZAMBIA','Zambia','ZMB',894,260),
	(239,'ZW','ZIMBABWE','Zimbabwe','ZWE',716,263);

/*!40000 ALTER TABLE `Country` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shop_CatalogueCategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shop_CatalogueCategories`;

CREATE TABLE `shop_CatalogueCategories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `description` tinytext,
  `left` int(11) unsigned NOT NULL,
  `right` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `shop_CatalogueCategories` WRITE;
/*!40000 ALTER TABLE `shop_CatalogueCategories` DISABLE KEYS */;

INSERT INTO `shop_CatalogueCategories` (`id`, `name`, `description`, `left`, `right`)
VALUES
	(1,'terminal tackle','Lures, etc..',1,12),
	(2,'sandeels','Kystwoblere der ligner tobis',3,4),
	(3,'release tackle parts','Release tackle parts',8,11),
	(4,'hooks','Hooks',9,10),
	(5,'gobies','Kystwoblere der ligner kutling',5,6),
	(6,'woblers','Kystwoblere',2,7);

/*!40000 ALTER TABLE `shop_CatalogueCategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shop_CatalogueCategoryProducts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shop_CatalogueCategoryProducts`;

CREATE TABLE `shop_CatalogueCategoryProducts` (
  `category_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  KEY `category_id` (`category_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `shop_CatalogueCategoryProducts_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `shop_CatalogueCategories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `shop_CatalogueCategoryProducts_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `shop_Products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `shop_CatalogueCategoryProducts` WRITE;
/*!40000 ALTER TABLE `shop_CatalogueCategoryProducts` DISABLE KEYS */;

INSERT INTO `shop_CatalogueCategoryProducts` (`category_id`, `product_id`)
VALUES
	(1,2),
	(1,3),
	(1,4);

/*!40000 ALTER TABLE `shop_CatalogueCategoryProducts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shop_CatalogueTagProducts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shop_CatalogueTagProducts`;

CREATE TABLE `shop_CatalogueTagProducts` (
  `tag_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `shop_CatalogueTagProducts_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `shop_CatalogueTags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `shop_CatalogueTagProducts_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `shop_Products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `shop_CatalogueTagProducts` WRITE;
/*!40000 ALTER TABLE `shop_CatalogueTagProducts` DISABLE KEYS */;

INSERT INTO `shop_CatalogueTagProducts` (`tag_id`, `product_id`)
VALUES
	(1,2),
	(1,3),
	(1,4);

/*!40000 ALTER TABLE `shop_CatalogueTagProducts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shop_CatalogueTags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shop_CatalogueTags`;

CREATE TABLE `shop_CatalogueTags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `shop_CatalogueTags` WRITE;
/*!40000 ALTER TABLE `shop_CatalogueTags` DISABLE KEYS */;

INSERT INTO `shop_CatalogueTags` (`id`, `name`)
VALUES
	(1,'wobler');

/*!40000 ALTER TABLE `shop_CatalogueTags` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shop_DeliveryOptions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shop_DeliveryOptions`;

CREATE TABLE `shop_DeliveryOptions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(11) DEFAULT NULL,
  `label` int(11) DEFAULT NULL,
  `price` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table shop_Entity
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shop_Entity`;

CREATE TABLE `shop_Entity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table shop_EntityAddresses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shop_EntityAddresses`;

CREATE TABLE `shop_EntityAddresses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `entity_id` int(11) unsigned NOT NULL,
  `address_id` int(11) unsigned NOT NULL,
  `type` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table shop_EntityEmails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shop_EntityEmails`;

CREATE TABLE `shop_EntityEmails` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `entity_id` int(11) unsigned NOT NULL,
  `email_id` int(11) unsigned NOT NULL,
  `type` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table shop_EntitySessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shop_EntitySessions`;

CREATE TABLE `shop_EntitySessions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `entity_id` int(11) unsigned NOT NULL,
  `status` enum('open','packaged','shipped','void','discarded') NOT NULL DEFAULT 'open',
  `cart_contents` text,
  `invoice_id` int(11) unsigned DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `entity_id` (`entity_id`),
  CONSTRAINT `shop_EntitySessions_ibfk_1` FOREIGN KEY (`entity_id`) REFERENCES `shop_Entity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table shop_InventoryShipments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shop_InventoryShipments`;

CREATE TABLE `shop_InventoryShipments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hash` varchar(64) NOT NULL DEFAULT '',
  `product_id` int(11) unsigned NOT NULL,
  `original_amount` smallint(6) unsigned NOT NULL DEFAULT '0',
  `current_amount` smallint(6) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shipment_id` (`hash`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `shop_InventoryShipments_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `shop_Products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `shop_InventoryShipments` WRITE;
/*!40000 ALTER TABLE `shop_InventoryShipments` DISABLE KEYS */;

INSERT INTO `shop_InventoryShipments` (`id`, `hash`, `product_id`, `original_amount`, `current_amount`, `created`)
VALUES
	(1,'aaa',2,100,100,'0000-00-00 00:00:00'),
	(2,'aab',3,100,100,'0000-00-00 00:00:00'),
	(3,'abb',4,100,100,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `shop_InventoryShipments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shop_Invoices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shop_Invoices`;

CREATE TABLE `shop_Invoices` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hash` varchar(255) DEFAULT NULL,
  `items` text NOT NULL,
  `total` int(11) NOT NULL,
  `currency` char(3) NOT NULL DEFAULT '',
  `vat_value` smallint(4) unsigned DEFAULT '25',
  `delivery` text NOT NULL,
  `billing` text NOT NULL,
  `contact_info` text NOT NULL,
  `service` text NOT NULL,
  `transaction_id` int(11) unsigned NOT NULL,
  `file_path` varchar(255) NOT NULL DEFAULT '',
  `last_printout` datetime DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  CONSTRAINT `shop_Invoices_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `shop_Transactions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `shop_Invoices` WRITE;
/*!40000 ALTER TABLE `shop_Invoices` DISABLE KEYS */;

INSERT INTO `shop_Invoices` (`id`, `hash`, `items`, `total`, `currency`, `vat_value`, `delivery`, `billing`, `contact_info`, `service`, `transaction_id`, `file_path`, `last_printout`, `created`)
VALUES
	(1,NULL,'a:0:{}',0,'C:4',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":57:{O:8:\"stdClass\":2:{s:6:\"emails\";a:0:{}s:6:\"phones\";a:0:{}}}','',42,'s:0:\"\";',NULL,'2015-10-30 15:30:10'),
	(2,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":5}\";}',50000,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":892:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:11:\"Hasselvej 3\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:8:\"Vanløse\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":470:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":384:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":298:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":212:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":126:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":41:{O:8:\"stdClass\":1:{s:4:\"code\";s:4:\"2720\";}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',29,'/tmp/',NULL,'2015-11-17 15:20:30'),
	(3,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":5}\";}',51500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":978:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:11:\"Hasselvej 3\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:8:\"Vanløse\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":556:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":470:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":384:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":298:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":212:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":126:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":41:{O:8:\"stdClass\":1:{s:4:\"code\";s:4:\"2720\";}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',30,'/tmp/',NULL,'2015-11-17 15:22:38'),
	(4,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":5}\";}',51500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":1150:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:11:\"Hasselvej 3\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:8:\"Vanløse\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":728:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":642:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":556:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":470:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":384:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":298:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":212:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":126:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":41:{O:8:\"stdClass\":1:{s:4:\"code\";s:4:\"2720\";}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',31,'/tmp/',NULL,'2015-11-17 15:46:02'),
	(5,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":5}\";}',51500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":1322:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:11:\"Hasselvej 3\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:8:\"Vanløse\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":900:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":814:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":728:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":642:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":556:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":470:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":384:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":298:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":212:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":126:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":41:{O:8:\"stdClass\":1:{s:4:\"code\";s:4:\"2720\";}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',32,'/tmp/',NULL,'2015-11-17 15:47:30'),
	(6,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":5}\";}',51500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":806:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:11:\"Hasselvej 3\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:8:\"Vanløse\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":384:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":298:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":212:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":126:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":41:{O:8:\"stdClass\":1:{s:4:\"code\";s:4:\"2720\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":250:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:0:\"\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',33,'/tmp/',NULL,'2015-11-18 15:43:13'),
	(7,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";}',11500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":806:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:11:\"Hasselvej 3\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:8:\"Vanløse\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":384:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":298:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":212:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":126:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":41:{O:8:\"stdClass\":1:{s:4:\"code\";s:4:\"2720\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',34,'/tmp/',NULL,'2015-11-20 15:04:38'),
	(8,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";}',11500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":794:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:11:\"Hasselvej 3\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',35,'/tmp/',NULL,'2015-11-20 15:08:59'),
	(9,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":2}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',31500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":782:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',64,'/tmp/',NULL,'2015-11-20 15:43:13'),
	(10,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":2}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',31500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":782:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',65,'/tmp/',NULL,'2015-11-20 15:43:19'),
	(11,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";}',11500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":782:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":250:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:0:\"\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',66,'/tmp/',NULL,'2015-11-20 16:21:28'),
	(12,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";}',10000,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":782:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":250:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:0:\"\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":103:{O:8:\"stdClass\":3:{s:4:\"name\";s:12:\"Store pickup\";s:5:\"label\";s:17:\"Pick up at :store\";s:5:\"price\";i:0;}}',67,'/tmp/',NULL,'2015-11-20 16:25:49'),
	(13,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";}',10000,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":782:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":103:{O:8:\"stdClass\":3:{s:4:\"name\";s:12:\"Store pickup\";s:5:\"label\";s:17:\"Pick up at :store\";s:5:\"price\";i:0;}}',68,'/tmp/',NULL,'2015-11-20 16:29:41'),
	(14,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":782:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',71,'/tmp/',NULL,'2015-11-20 16:51:49'),
	(15,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":782:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',72,'/tmp/',NULL,'2015-11-20 16:54:05'),
	(16,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":1298:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',73,'/tmp/',NULL,'2015-11-20 17:05:42'),
	(17,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":1384:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":982:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',74,'/tmp/',NULL,'2015-11-20 17:05:44'),
	(18,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":1471:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":1068:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":982:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',75,'/tmp/',NULL,'2015-11-20 17:05:44'),
	(19,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":1558:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":1155:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1068:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":982:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',76,'/tmp/',NULL,'2015-11-20 17:05:45'),
	(20,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":1645:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":1242:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1155:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1068:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":982:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',77,'/tmp/',NULL,'2015-11-20 17:05:46'),
	(21,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":1732:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":1329:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1242:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1155:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1068:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":982:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',78,'/tmp/',NULL,'2015-11-20 17:05:47'),
	(22,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":1819:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":1416:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1329:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1242:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1155:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1068:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":982:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',79,'/tmp/',NULL,'2015-11-20 17:06:10'),
	(23,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":1906:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":1503:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1416:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1329:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1242:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1155:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1068:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":982:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',80,'/tmp/',NULL,'2015-11-20 17:06:11'),
	(24,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":1993:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":1590:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1503:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1416:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1329:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1242:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1155:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1068:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":982:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',81,'/tmp/',NULL,'2015-11-20 17:06:12'),
	(25,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":2080:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":1677:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1590:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1503:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1416:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1329:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1242:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1155:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1068:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":982:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',82,'/tmp/',NULL,'2015-11-20 17:06:13'),
	(26,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":2167:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":1764:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1677:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1590:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1503:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1416:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1329:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1242:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1155:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1068:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":982:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',83,'/tmp/',NULL,'2015-11-20 17:06:19'),
	(27,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":2254:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":1851:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1764:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1677:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1590:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1503:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1416:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1329:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1242:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1155:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1068:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":982:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',84,'/tmp/',NULL,'2015-11-20 17:07:07'),
	(28,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":2341:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":1938:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1851:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1764:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1677:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1590:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1503:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1416:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1329:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1242:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1155:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1068:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":982:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',85,'/tmp/',NULL,'2015-11-20 17:07:09'),
	(29,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":2428:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":2025:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1938:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1851:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1764:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1677:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1590:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1503:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1416:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1329:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1242:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1155:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1068:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":982:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',86,'/tmp/',NULL,'2015-11-20 17:07:09'),
	(30,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":2515:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":2112:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":2025:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1938:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1851:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1764:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1677:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1590:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1503:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1416:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1329:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1242:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1155:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1068:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":982:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',87,'/tmp/',NULL,'2015-11-20 17:07:37'),
	(31,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":2602:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":2199:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":2112:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":2025:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1938:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1851:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1764:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1677:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1590:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1503:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1416:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1329:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1242:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1155:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1068:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":982:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',88,'/tmp/',NULL,'2015-11-20 17:07:38'),
	(32,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":2689:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":2286:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":2199:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":2112:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":2025:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1938:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1851:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1764:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1677:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1590:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1503:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1416:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1329:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1242:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1155:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1068:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":982:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',89,'/tmp/',NULL,'2015-11-20 17:07:39'),
	(33,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":2776:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":2373:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":2286:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":2199:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":2112:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":2025:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1938:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1851:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1764:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1677:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1590:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1503:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1416:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1329:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1242:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1155:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":1068:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":982:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":269:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:18:\"ak@fitnessworld.dk\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',90,'/tmp/',NULL,'2015-11-20 17:08:40'),
	(34,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":768:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":250:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:0:\"\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',91,'/tmp/',NULL,'2015-11-20 17:11:11'),
	(35,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";}',11500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":768:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":250:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:0:\"\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',92,'/tmp/',NULL,'2015-11-20 17:13:21'),
	(36,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";}',10000,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":57:{O:8:\"stdClass\":2:{s:6:\"emails\";a:0:{}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":103:{O:8:\"stdClass\":3:{s:4:\"name\";s:12:\"Store pickup\";s:5:\"label\";s:17:\"Pick up at :store\";s:5:\"price\";i:0;}}',93,'/tmp/',NULL,'2015-11-20 17:14:02'),
	(37,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";}',11500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":768:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":250:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:0:\"\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',94,'/tmp/',NULL,'2015-11-20 17:14:36'),
	(38,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";}',11500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":854:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":250:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:0:\"\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',95,'/tmp/',NULL,'2015-11-20 17:14:43'),
	(39,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";}',11500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":940:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":250:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:0:\"\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',96,'/tmp/',NULL,'2015-11-20 17:17:34'),
	(40,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";}',11500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":1026:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":250:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:0:\"\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',97,'/tmp/',NULL,'2015-11-20 17:17:36'),
	(41,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";}',11500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":1112:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":250:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:0:\"\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',98,'/tmp/',NULL,'2015-11-20 17:17:37'),
	(42,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";}',11500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":1198:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":250:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:0:\"\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',99,'/tmp/',NULL,'2015-11-20 17:17:38'),
	(43,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";}',11500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":1284:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":250:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:0:\"\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',100,'/tmp/',NULL,'2015-11-20 17:18:01'),
	(44,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";}',11500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":1370:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":982:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":896:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":810:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":724:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":250:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:0:\"\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',101,'/tmp/',NULL,'2015-11-20 17:18:02'),
	(45,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";}',11500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":768:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":250:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:0:\"\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',102,'/tmp/',NULL,'2015-11-20 17:21:50'),
	(46,NULL,'a:1:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";}',11500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":768:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":250:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:0:\"\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',103,'/tmp/',NULL,'2015-11-20 17:26:20'),
	(47,NULL,'a:1:{i:0;s:24:\"{\"product\":3,\"amount\":2}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":768:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":250:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:0:\"\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',104,'/tmp/',NULL,'2015-11-20 17:52:21'),
	(48,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',21500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":768:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":250:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:0:\"\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',105,'/tmp/',NULL,'2016-01-10 13:10:53'),
	(49,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":1}\";i:1;s:24:\"{\"product\":3,\"amount\":2}\";}',31500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":768:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":250:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:0:\"\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',106,'/tmp/',NULL,'2016-01-10 13:30:34'),
	(50,NULL,'a:2:{i:0;s:24:\"{\"product\":2,\"amount\":2}\";i:1;s:24:\"{\"product\":3,\"amount\":1}\";}',31500,'DKK',NULL,'C:39:\"supervillainhqphalconcontactsAddress\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhqphalconcontactsAddress\":1026:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhqphalconcontactsPostalCode\":638:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":552:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":466:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhqphalconcontactsPostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhqphalconintlCountry\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhqphalconintlCountry\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhqphalconcontactsContactInfo\":268:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhqphalconmailEmail\":3:{s:40:\"\0supervillainhqphalconmailEmail\0valid\";N;s:44:\"\0supervillainhqphalconmailEmail\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:17:\"andkrup@gmail.com\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhqphalconmicroshopDeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',110,'/tmp/',NULL,'2016-01-10 15:14:25'),
	(51,'93be01b953a8867b72218b5b1bcd3f0f','a:2:{i:0;s:24:\"{\"product\":2,\"amount\":2}\";i:1;s:24:\"{\"product\":3,\"amount\":2}\";}',41500,'DKK',NULL,'C:39:\"supervillainhq\\phalcon\\contacts\\Address\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhq\\phalcon\\contacts\\Address\":768:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhq\\phalcon\\intl\\Country\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhq\\phalcon\\intl\\Country\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhq\\phalcon\\contacts\\ContactInfo\":268:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhq\\phalcon\\mail\\Email\":3:{s:40:\"\0supervillainhq\\phalcon\\mail\\Email\0valid\";N;s:44:\"\0supervillainhq\\phalcon\\mail\\Email\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:17:\"andkrup@gmail.com\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhq\\phalcon\\microshop\\DeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',111,'/tmp/',NULL,'2016-01-10 15:17:42'),
	(52,'ab31b811c30b1538d8d3464f45f7cdbf','a:1:{i:0;s:38:\"{\"product\":2,\"price\":10000,\"amount\":1}\";}',1501,'DKK',NULL,'C:39:\"supervillainhq\\phalcon\\contacts\\Address\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhq\\phalcon\\contacts\\Address\":768:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhq\\phalcon\\intl\\Country\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhq\\phalcon\\intl\\Country\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhq\\phalcon\\contacts\\ContactInfo\":268:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhq\\phalcon\\mail\\Email\":3:{s:40:\"\0supervillainhq\\phalcon\\mail\\Email\0valid\";N;s:44:\"\0supervillainhq\\phalcon\\mail\\Email\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:17:\"andkrup@gmail.com\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhq\\phalcon\\microshop\\DeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',115,'/tmp/',NULL,'2016-01-10 17:58:46'),
	(53,'c8259be08b270fc2de3a8d9a151d71b7','a:1:{i:0;s:38:\"{\"product\":2,\"price\":10000,\"amount\":1}\";}',11500,'DKK',25,'C:39:\"supervillainhq\\phalcon\\contacts\\Address\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhq\\phalcon\\contacts\\Address\":768:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:0:\"\";s:3:\"att\";N;s:8:\"address1\";s:0:\"\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:0:\"\";s:10:\"postalcode\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":380:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":294:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":208:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":122:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":37:{O:8:\"stdClass\":1:{s:4:\"code\";s:0:\"\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhq\\phalcon\\intl\\Country\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhq\\phalcon\\intl\\Country\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhq\\phalcon\\contacts\\ContactInfo\":268:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhq\\phalcon\\mail\\Email\":3:{s:40:\"\0supervillainhq\\phalcon\\mail\\Email\0valid\";N;s:44:\"\0supervillainhq\\phalcon\\mail\\Email\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:17:\"andkrup@gmail.com\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhq\\phalcon\\microshop\\DeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',116,'/tmp/',NULL,'2016-01-10 18:08:59'),
	(54,'37c9d96ec34eb1be1c60b3aa122cd8ba','a:1:{i:0;s:38:\"{\"product\":2,\"price\":10000,\"amount\":2}\";}',21500,'DKK',25,'C:39:\"supervillainhq\\phalcon\\contacts\\Address\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhq\\phalcon\\contacts\\Address\":806:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:11:\"Hasselvej 3\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:8:\"Vanløse\";s:10:\"postalcode\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":384:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":298:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":212:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":126:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":41:{O:8:\"stdClass\":1:{s:4:\"code\";s:4:\"2720\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhq\\phalcon\\intl\\Country\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhq\\phalcon\\intl\\Country\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhq\\phalcon\\contacts\\ContactInfo\":268:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhq\\phalcon\\mail\\Email\":3:{s:40:\"\0supervillainhq\\phalcon\\mail\\Email\0valid\";N;s:44:\"\0supervillainhq\\phalcon\\mail\\Email\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:17:\"andkrup@gmail.com\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhq\\phalcon\\microshop\\DeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',117,'/tmp/',NULL,'2016-01-12 18:51:12'),
	(55,'0ecd0e6b50a4c181e75ded788b6fa9a2','a:1:{i:0;s:38:\"{\"product\":2,\"price\":10000,\"amount\":2}\";}',21500,'DKK',25,'C:39:\"supervillainhq\\phalcon\\contacts\\Address\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhq\\phalcon\\contacts\\Address\":806:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:11:\"Hasselvej 3\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:8:\"Vanløse\";s:10:\"postalcode\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":384:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":298:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":212:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":126:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":41:{O:8:\"stdClass\":1:{s:4:\"code\";s:4:\"2720\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhq\\phalcon\\intl\\Country\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhq\\phalcon\\intl\\Country\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhq\\phalcon\\contacts\\ContactInfo\":268:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhq\\phalcon\\mail\\Email\":3:{s:40:\"\0supervillainhq\\phalcon\\mail\\Email\0valid\";N;s:44:\"\0supervillainhq\\phalcon\\mail\\Email\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:17:\"andkrup@gmail.com\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhq\\phalcon\\microshop\\DeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',118,'/tmp/',NULL,'2016-01-12 20:43:09'),
	(56,'66615c441af250d41ca4851440bf46a6','a:1:{i:0;s:38:\"{\"product\":3,\"price\":10000,\"amount\":4}\";}',41500,'DKK',25,'C:39:\"supervillainhq\\phalcon\\contacts\\Address\":157:{O:8:\"stdClass\":9:{s:8:\"fullname\";N;s:3:\"att\";N;s:8:\"address1\";N;s:8:\"address2\";N;s:4:\"city\";N;s:10:\"postalcode\";N;s:6:\"region\";N;s:7:\"country\";N;s:2:\"id\";N;}}','C:39:\"supervillainhq\\phalcon\\contacts\\Address\":806:{O:8:\"stdClass\":9:{s:8:\"fullname\";s:13:\"Anders Krarup\";s:3:\"att\";N;s:8:\"address1\";s:11:\"Hasselvej 3\";s:8:\"address2\";s:0:\"\";s:4:\"city\";s:8:\"Vanløse\";s:10:\"postalcode\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":384:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":298:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":212:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":126:{O:8:\"stdClass\":1:{s:4:\"code\";C:42:\"supervillainhq\\phalcon\\contacts\\PostalCode\":41:{O:8:\"stdClass\":1:{s:4:\"code\";s:4:\"2720\";}}}}}}}}}}s:6:\"region\";N;s:7:\"country\";C:35:\"supervillainhq\\phalcon\\intl\\Country\":110:{O:8:\"stdClass\":1:{s:4:\"name\";C:35:\"supervillainhq\\phalcon\\intl\\Country\":32:{O:8:\"stdClass\":1:{s:4:\"name\";N;}}}}s:2:\"id\";N;}}','C:43:\"supervillainhq\\phalcon\\contacts\\ContactInfo\":268:{O:8:\"stdClass\":2:{s:6:\"emails\";a:1:{s:7:\"contact\";O:33:\"supervillainhq\\phalcon\\mail\\Email\":3:{s:40:\"\0supervillainhq\\phalcon\\mail\\Email\0valid\";N;s:44:\"\0supervillainhq\\phalcon\\mail\\Email\0confirmed\";N;s:15:\"\0*\0emailAddress\";s:17:\"andkrup@gmail.com\";}}s:6:\"phones\";a:0:{}}}','C:48:\"supervillainhq\\phalcon\\microshop\\DeliveryService\":115:{O:8:\"stdClass\":3:{s:4:\"name\";s:11:\"PostDanmark\";s:5:\"label\";s:27:\"PostDanmark Standard Letter\";s:5:\"price\";i:1500;}}',119,'/tmp/',NULL,'2016-01-12 20:47:36');

/*!40000 ALTER TABLE `shop_Invoices` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shop_OrderProducts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shop_OrderProducts`;

CREATE TABLE `shop_OrderProducts` (
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `amount` tinyint(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`order_id`,`product_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `shop_OrderProducts_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `shop_Orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `shop_OrderProducts_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `shop_Products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table shop_Orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shop_Orders`;

CREATE TABLE `shop_Orders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` varchar(255) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table shop_ProductAssets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shop_ProductAssets`;

CREATE TABLE `shop_ProductAssets` (
  `product_id` int(11) unsigned DEFAULT NULL,
  `asset_id` int(11) unsigned DEFAULT NULL,
  KEY `product_id` (`product_id`),
  KEY `asset_id` (`asset_id`),
  CONSTRAINT `shop_ProductAssets_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `shop_Products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `shop_ProductAssets_ibfk_2` FOREIGN KEY (`asset_id`) REFERENCES `cms_Assets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `shop_ProductAssets` WRITE;
/*!40000 ALTER TABLE `shop_ProductAssets` DISABLE KEYS */;

INSERT INTO `shop_ProductAssets` (`product_id`, `asset_id`)
VALUES
	(2,2),
	(3,3),
	(4,4);

/*!40000 ALTER TABLE `shop_ProductAssets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shop_Products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shop_Products`;

CREATE TABLE `shop_Products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `price` int(11) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `shop_Products` WRITE;
/*!40000 ALTER TABLE `shop_Products` DISABLE KEYS */;

INSERT INTO `shop_Products` (`id`, `parent_id`, `name`, `description`, `price`, `active`, `created`)
VALUES
	(1,NULL,'2-bissen original wobler','Denne gennemløbswobler er det ypperste endegrej til kysten. Knaldhårdt testet ved Stevns af ihærdige havørredfiskere, denne model er så effektiv at den har lavet ravage blandt flere erfarne blinkproducenter.',10000,1,'0000-00-00 00:00:00'),
	(2,1,'2-bissen wobler, grøn','Denne gennemløbswobler er det ypperste endegrej til kysten. Knaldhårdt testet ved Stevns af ihærdige havørredfiskere, denne model er så effektiv at den har lavet ravage blandt flere erfarne blinkproducenter. Denne variant vejer ca. 20 gr, og har grøn ryg med hvid mave.',10000,1,'0000-00-00 00:00:00'),
	(3,1,'2-bissen wobler, sort','Denne gennemløbswobler er det ypperste endegrej til kysten. Knaldhårdt testet ved Stevns af ihærdige havørredfiskere, denne model er så effektiv at den har lavet ravage blandt flere erfarne blinkproducenter. Denne variant vejer ca. 20 gr, og har sort ryg med hvid mave og sølvfarvede sider.',10000,1,'0000-00-00 00:00:00'),
	(4,1,'2-bissen wobler, orange','Denne gennemløbswobler er det ypperste endegrej til kysten. Knaldhårdt testet ved Stevns af ihærdige havørredfiskere, denne model er så effektiv at den har lavet ravage blandt flere erfarne blinkproducenter. Denne variant vejer ca. 20 gr, og har orange ryg med hvid mave og pink sider.',10000,1,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `shop_Products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shop_PromotionProducts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shop_PromotionProducts`;

CREATE TABLE `shop_PromotionProducts` (
  `promotion_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `rebate_price` smallint(6) unsigned DEFAULT NULL,
  KEY `promotion_id` (`promotion_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `shop_PromotionProducts_ibfk_1` FOREIGN KEY (`promotion_id`) REFERENCES `shop_Promotions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `shop_PromotionProducts_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `shop_Products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table shop_Promotions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shop_Promotions`;

CREATE TABLE `shop_Promotions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(160) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `created` datetime NOT NULL,
  `default_rebate_percentage` smallint(6) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table shop_Transactions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shop_Transactions`;

CREATE TABLE `shop_Transactions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `gateway_id` varchar(32) DEFAULT NULL,
  `error_code` mediumint(8) DEFAULT NULL,
  `gateway` varchar(32) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  `authorized` datetime DEFAULT NULL,
  `authorization_rejected` datetime DEFAULT NULL,
  `captured` datetime DEFAULT NULL,
  `capture_rejected` datetime DEFAULT NULL,
  `refunded` datetime DEFAULT NULL,
  `rejected` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `archived` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `shop_Transactions` WRITE;
/*!40000 ALTER TABLE `shop_Transactions` DISABLE KEYS */;

INSERT INTO `shop_Transactions` (`id`, `gateway_id`, `error_code`, `gateway`, `created`, `authorized`, `authorization_rejected`, `captured`, `capture_rejected`, `refunded`, `rejected`, `deleted`, `archived`)
VALUES
	(1,NULL,NULL,'','2015-10-30 15:56:13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,NULL,NULL,'epay','2015-10-30 16:00:03',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,NULL,NULL,'epay','2015-11-02 17:53:04',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(4,NULL,NULL,'epay','2015-11-02 17:54:59',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(5,NULL,NULL,'epay','2015-11-02 17:56:10',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(6,NULL,NULL,'epay','2015-11-02 17:56:40',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(7,NULL,NULL,'epay','2015-11-02 17:57:48',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(8,NULL,NULL,'epay','2015-11-02 17:58:33',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(9,NULL,NULL,'epay','2015-11-02 17:59:37',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(10,NULL,NULL,'epay','2015-11-02 18:00:58',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(11,NULL,NULL,'epay','2015-11-02 18:02:12',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(12,NULL,NULL,'epay','2015-11-02 18:02:15',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(13,NULL,NULL,'epay','2015-11-02 18:02:53',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(14,NULL,NULL,'epay','2015-11-02 18:04:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(15,NULL,NULL,'epay','2015-11-02 18:04:56',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(16,NULL,NULL,'epay','2015-11-02 18:08:21',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(17,NULL,NULL,'epay','2015-11-02 19:12:42',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(18,NULL,NULL,'epay','2015-11-02 20:38:10',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(19,NULL,NULL,'epay','2015-11-02 22:31:02',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(20,NULL,NULL,'epay','2015-11-04 15:59:37',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(21,NULL,NULL,'epay','2015-11-11 15:35:42',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(22,NULL,NULL,'epay','2015-11-12 15:44:09',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(23,NULL,NULL,'epay','2015-11-12 22:22:11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(24,NULL,NULL,'epay','2015-11-12 23:16:04',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(25,NULL,NULL,'epay','2015-11-13 13:55:11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(26,NULL,NULL,'epay','2015-11-13 14:22:13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(27,NULL,NULL,'epay','2015-11-13 14:59:36',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(28,NULL,NULL,'epay','2015-11-17 15:07:23',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(29,NULL,NULL,'epay','2015-11-17 15:20:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(30,NULL,NULL,'epay','2015-11-17 15:22:38',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(31,NULL,NULL,'epay','2015-11-17 15:46:02',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(32,NULL,NULL,'epay','2015-11-17 15:47:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(33,NULL,NULL,'epay','2015-11-18 15:43:13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(34,NULL,NULL,'epay','2015-11-20 15:04:38',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(35,NULL,NULL,'epay','2015-11-20 15:08:59',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(36,NULL,NULL,'epay','2015-11-20 15:10:20',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(37,NULL,NULL,'epay','2015-11-20 15:13:11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(38,NULL,NULL,'epay','2015-11-20 15:13:15',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(39,NULL,NULL,'epay','2015-11-20 15:13:39',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(40,NULL,NULL,'epay','2015-11-20 15:13:41',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(41,NULL,NULL,'epay','2015-11-20 15:14:02',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(42,NULL,NULL,'epay','2015-11-20 15:18:15',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(43,NULL,NULL,'epay','2015-11-20 15:18:48',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(44,NULL,NULL,'epay','2015-11-20 15:19:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(45,NULL,NULL,'epay','2015-11-20 15:19:22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(46,NULL,NULL,'epay','2015-11-20 15:19:54',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(47,NULL,NULL,'epay','2015-11-20 15:21:40',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(48,NULL,NULL,'epay','2015-11-20 15:22:07',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(49,NULL,NULL,'epay','2015-11-20 15:23:26',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(50,NULL,NULL,'epay','2015-11-20 15:23:48',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(51,NULL,NULL,'epay','2015-11-20 15:27:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(52,NULL,NULL,'epay','2015-11-20 15:28:07',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(53,NULL,NULL,'epay','2015-11-20 15:28:18',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(54,NULL,NULL,'epay','2015-11-20 15:29:15',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(55,NULL,NULL,'epay','2015-11-20 15:30:08',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(56,NULL,NULL,'epay','2015-11-20 15:30:11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(57,NULL,NULL,'epay','2015-11-20 15:30:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(58,NULL,NULL,'epay','2015-11-20 15:30:35',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(59,NULL,NULL,'epay','2015-11-20 15:33:05',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(60,NULL,NULL,'epay','2015-11-20 15:33:52',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(61,NULL,NULL,'epay','2015-11-20 15:34:18',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(62,NULL,NULL,'epay','2015-11-20 15:36:50',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(63,NULL,NULL,'epay','2015-11-20 15:41:06',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(64,NULL,NULL,'epay','2015-11-20 15:43:13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(65,'55891435',NULL,'epay','2015-11-20 15:43:19','2016-01-10 14:05:51',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(66,'55893661',NULL,'epay','2015-11-20 16:21:28','2016-01-12 23:22:46',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(67,NULL,NULL,'epay','2015-11-20 16:25:49',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(68,NULL,NULL,'epay','2015-11-20 16:29:41',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(69,NULL,NULL,'epay','2015-11-20 16:32:23',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(70,NULL,NULL,'epay','2015-11-20 16:41:26',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(71,NULL,NULL,'epay','2015-11-20 16:51:49',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(72,NULL,NULL,'epay','2015-11-20 16:54:05',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(73,NULL,NULL,'epay','2015-11-20 17:05:42',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(74,NULL,NULL,'epay','2015-11-20 17:05:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(75,NULL,NULL,'epay','2015-11-20 17:05:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(76,NULL,NULL,'epay','2015-11-20 17:05:45',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(77,NULL,NULL,'epay','2015-11-20 17:05:46',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(78,NULL,NULL,'epay','2015-11-20 17:05:47',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(79,NULL,NULL,'epay','2015-11-20 17:06:10',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(80,NULL,NULL,'epay','2015-11-20 17:06:11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(81,NULL,NULL,'epay','2015-11-20 17:06:12',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(82,NULL,NULL,'epay','2015-11-20 17:06:13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(83,NULL,NULL,'epay','2015-11-20 17:06:19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(84,NULL,NULL,'epay','2015-11-20 17:07:07',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(85,NULL,NULL,'epay','2015-11-20 17:07:09',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(86,NULL,NULL,'epay','2015-11-20 17:07:09',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(87,NULL,NULL,'epay','2015-11-20 17:07:37',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(88,NULL,NULL,'epay','2015-11-20 17:07:38',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(89,NULL,NULL,'epay','2015-11-20 17:07:39',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(90,NULL,NULL,'epay','2015-11-20 17:08:40',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(91,NULL,NULL,'epay','2015-11-20 17:11:10',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(92,NULL,NULL,'epay','2015-11-20 17:13:21',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(93,NULL,NULL,'epay','2015-11-20 17:14:02',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(94,NULL,NULL,'epay','2015-11-20 17:14:36',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(95,NULL,NULL,'epay','2015-11-20 17:14:43',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(96,NULL,NULL,'epay','2015-11-20 17:17:34',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(97,NULL,NULL,'epay','2015-11-20 17:17:36',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(98,NULL,NULL,'epay','2015-11-20 17:17:37',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(99,NULL,NULL,'epay','2015-11-20 17:17:38',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(100,NULL,NULL,'epay','2015-11-20 17:18:01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(101,NULL,NULL,'epay','2015-11-20 17:18:02',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(102,NULL,NULL,'epay','2015-11-20 17:21:50',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(103,NULL,NULL,'epay','2015-11-20 17:26:20',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(104,'55898645',NULL,'epay','2015-11-20 17:52:21','2015-11-20 18:52:36',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(105,'59919158',NULL,'epay','2016-01-10 13:10:53','2016-01-12 23:18:46',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(106,'59920281',NULL,'epay','2016-01-10 13:30:34','2016-01-10 16:06:09',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(107,NULL,NULL,'epay','2016-01-10 15:06:39',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(108,NULL,NULL,'epay','2016-01-10 15:07:52',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(109,NULL,NULL,'epay','2016-01-10 15:10:04',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(110,NULL,NULL,'epay','2016-01-10 15:14:25',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(111,'59927196',NULL,'epay','2016-01-10 15:17:42','2016-01-10 16:18:03',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(112,NULL,NULL,'epay','2016-01-10 16:57:01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(113,NULL,NULL,'epay','2016-01-10 17:01:14',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(114,NULL,NULL,'epay','2016-01-10 17:01:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(115,NULL,-5516,'epay','2016-01-10 17:58:46',NULL,NULL,NULL,NULL,NULL,'2016-01-12 21:29:52',NULL,NULL),
	(116,'59940354',NULL,'epay','2016-01-10 18:08:59','2016-01-12 23:18:08',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(117,'60102447',NULL,'epay','2016-01-12 18:51:12','2016-01-12 19:51:25',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(118,NULL,NULL,'epay','2016-01-12 20:43:09',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(119,NULL,-5516,'epay','2016-01-12 20:47:36',NULL,NULL,NULL,NULL,NULL,'2016-01-12 22:06:00',NULL,NULL);

/*!40000 ALTER TABLE `shop_Transactions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table TobissenWobler
# ------------------------------------------------------------

DROP TABLE IF EXISTS `TobissenWobler`;

CREATE TABLE `TobissenWobler` (
  `product_id` int(11) unsigned NOT NULL,
  `weight_gr` int(11) unsigned NOT NULL,
  `color_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `color_id` (`color_id`),
  CONSTRAINT `TobissenWobler_ibfk_1` FOREIGN KEY (`color_id`) REFERENCES `Colors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `TobissenWobler_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `shop_Products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `TobissenWobler` WRITE;
/*!40000 ALTER TABLE `TobissenWobler` DISABLE KEYS */;

INSERT INTO `TobissenWobler` (`product_id`, `weight_gr`, `color_id`)
VALUES
	(2,20,1),
	(3,20,2),
	(4,20,3);

/*!40000 ALTER TABLE `TobissenWobler` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
