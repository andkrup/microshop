<?php
namespace supervillainhq\lexcorp\microshop{
	use supervillainhq\tobissen\shop\TobissenWobler;
	use Phalcon\Di;

	class MicroCartItem implements CartItem{
		private $product;
		private $amount;
		private $price;

		function __construct(Product $product, $amount, $price = NAN){
			$this->product = $product;
			$this->amount = $amount;
			$this->price = $price;
		}

		function active(){
			return true;
		}

		function amount($amount = 0){
			if($amount > 0){
				$this->amount = $amount;
			}
			return $this->amount;
		}

		function product(Product $product = null){
			return $this->product;
		}

		function price(){
			if(!is_nan($this->price)){
				return $this->price;
			}
			return $this->product->price();
		}

		function total(){
			$productPrice = $this->price();
			return $productPrice * $this->amount;
		}

		function equals(CartItem $item){
			return $this->product->id() == $item->product->id();
		}

		public function serialize () {
			$simple = (object) [
					'product' => $this->product->id(),
					'price' => $this->product->price(),
					'amount' => $this->amount
			];
			return json_encode($simple);
		}

		public function unserialize ($serialized) {
			$simple = json_decode($serialized);
			$this->product(new TobissenWobler());
			$this->amount = $simple->amount;
			$this->price = $simple->price;
		}
		static public function hydrate($serialized) {
			$simple = json_decode($serialized);
			$product = Di::getDefault()->getObjectmapper('product', ['id' => $simple->product])->get();
			$price = property_exists($simple, 'price') ? intval($simple->price) : NAN;
			$item = new MicroCartItem($product, intval($simple->amount), $price);
			return $item;
		}
	}
}