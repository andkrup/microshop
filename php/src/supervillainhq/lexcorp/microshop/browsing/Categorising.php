<?php
namespace supervillainhq\lexcorp\microshop\browsing{
	trait Categorising{
		protected $name;
		protected $uri;
		protected $parent;
		protected $root;
		protected $siblings;
		protected $children;

		function name( $name = null){
			if(is_null($name)){
				return $this->name;
			}
			$this->name = $name;
		}
		function uri( $uri = null){
			if(is_null($uri)){
				return $this->uri;
			}
			$this->uri = $uri;
		}
		function hasParentCategory(){
			return is_null($this->parent);
		}
		function parentCategory(Category $category = null){
			if(is_null($category)){
				return $this->parent;
			}
			$this->parent = $category;
		}
		function rootCategory(Category $category = null){
			if(is_null($category)){
				return $this->root;
			}
			$this->root = $category;
		}

		function resetSiblings(array $siblings = []){
			$this->siblings = $siblings;
		}
		function addSibling(Category $sibling){
			array_push($this->siblings, $sibling);
		}
		function removeSibling(Category $sibling){
			$c = count($this->siblings);
			for($i = 0; $i < $c; $i++){
				if($this->siblings[$i] == $sibling){
					array_splice($this->siblings, $i, 0);
				}
			}
		}
		function hasSibling(Category $sibling){
			return in_array($this->siblings, $sibling);
		}
		function hasSiblings(){
			return is_array($this->siblings) && count($this->siblings) > 0;
		}
		function getSibling($index){
			return $this->siblings[$index];
		}
		function siblings(){
			return $this->siblings;
		}


		function resetChildren(array $childrens = []){
			$this->children = $childrens;
		}
		function addChild(Category $children){
			array_push($this->children, $children);
		}
		function removeChild(Category $children){
			$c = count($this->children);
			for($i = 0; $i < $c; $i++){
				if($this->children[$i] == $children){
					array_splice($this->children, $i, 0);
				}
			}
		}
		function hasChild(Category $children){
			return in_array($this->children, $children);
		}
		function hasChildren(){
			return is_array($this->children) && count($this->children) > 0;
		}
		function getChild($index){
			return $this->children[$index];
		}
		function children(){
			return $this->children;
		}

		function isRoot(){
			return is_null($this->parent);
		}

		function isParentOf(Category $category){
			return false;
		}
		function isChildOf(Category $category){
			return false;
		}
		function isSiblingOf(Category $category){
			return false;
		}
	}
}