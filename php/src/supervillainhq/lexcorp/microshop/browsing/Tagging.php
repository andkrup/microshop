<?php
namespace supervillainhq\lexcorp\microshop\browsing{
	trait Tagging{
		private $tags;

		function resetTags(array $tags = []){
			$this->tags = $tags;
		}
		function addTag($tag){
			array_push($this->tags, $tag);
		}
		function removeTag($tag){
			$c = count($this->tags);
			for($i = 0; $i < $c; $i++){
				if($this->tags[$i] == $tag){
					array_splice($this->tags, $i, 0);
				}
			}
		}
		function hasTag($tag){
			return in_array($this->tags, $tag);
		}
		function getTag($index){
			return $this->tags[$index];
		}
		function tags(){
			return $this->tags;
		}
	}
}