<?php
namespace supervillainhq\lexcorp\microshop\browsing{
	use supervillainhq\lexcorp\microshop\Product;
	use supervillainhq\lexcorp\microshop\db\ProductMapper;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\cms\db\AssetMapper;
	use supervillainhq\spectre\cms\assets\FileAsset;

	class TagsNavigator implements Navigator, Taggable{
		use Tagging;

		function findCatalogueItems(array $search){
			$sql = "select
					p.id as product_id, p.name as product_name, p.description as product_description, p.price as product_price
					, a.id as asset_id, a.name as asset_name, a.src as asset_filepath
				from shop_CatalogueTags ct
				left join shop_CatalogueTagProducts ctp on ctp.tag_id = ct.id
				left join shop_Products p on p.id = ctp.product_id
				left join shop_ProductAssets pa on pa.product_id = p.id
				left join cms_Assets a on a.id = pa.asset_id
				where p.active = 1
				and ct.name in (:search);";
			$query = SqlQuery::create($sql);
			$searchPhrase = implode(',', $search);
			$parameters = ['search' => $searchPhrase];
			$query->query($parameters);
			$rows = $query->fetchAll();
			$items = [];
			foreach ($rows as $row){
				$product = Product::inflate(new ProductMapper($row));
				$asset = FileAsset::inflate(new AssetMapper($row));
				$item = new TaggedCatalogueItem($product, [$asset]);
				array_push($items, $item);
			}
			return $items;
		}

		function __construct(){
			$this->resetTags();
			$sql = "select id, name from shop_CatalogueTags;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			foreach ($rows as $row){
				$row = (object) $row;
				$tag = new Tag();
				$tag->id(intval($row->id));
				$tag->name(trim($row->name));
				array_push($this->tags, $tag);
			}
		}
	}
}