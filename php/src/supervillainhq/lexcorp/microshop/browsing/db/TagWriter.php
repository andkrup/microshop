<?php
namespace supervillainhq\lexcorp\microshop\browsing\db{
	use supervillainhq\core\db\DataWriter;
	use supervillainhq\core\db\ParameterContainer;
	use supervillainhq\core\db\Reading;
	use supervillainhq\lexcorp\microshop\browsing\Tag;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\DependencyInjecting;

	class TagWriter implements DataWriter{
		use DependencyInjecting, ParameterContainer, Reading;

		function __construct($data){
			if(!is_null($data)){
				if($data instanceof Tag){
					$this->addParameter('id', $data->id());
					$this->addParameter('name', $data->name());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('tag_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}
		function create(){
			$sql = "insert into shop_CatalogueTags
					(name)
					values (:name);";
			$parameters = [
					'name' => $this->name
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
			return $query->lastInsertId();
		}
		function update(){
			$sql = "update shop_CatalogueTags set
					name = :name
					where id = :id;";
			$parameters = [
					'name' => $this->name,
					'id' => $this->id,
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
		}

		function delete(){
			var_dump('DELETE');exit;
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
					case 'price':
						return intval($this->getParameter($name));
					case 'name':
						return stripslashes(trim($this->getParameter($name)));
				}
			}
			return null;
		}
	}
}