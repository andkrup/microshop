<?php
namespace supervillainhq\lexcorp\microshop\browsing\db{
	use supervillainhq\lexcorp\microshop\browsing\Tag;
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\db\Mapper;

	class TagMapper extends Mapper implements DataMapper, DataReader{

	    function __construct($data = null, $lazyLoad = true){
	    	parent::__construct($data, $lazyLoad);
	    	if(!is_null($data)){
	    		if($data instanceof Tag){
	    			$this->addParameter('id', $data->id());
	    			$this->addParameter('name', $data->name());
	    			$this->addParameter('description', $data->description());
	    			$this->addParameter('price', $data->price());
	    		}
	    		elseif (is_array($data)){
	    			$this->resetParameters();
	    			$keys = array_keys($data);
	    			foreach ($keys as $key){
	    				$k = str_ireplace('tag_', '', $key);
	    				$this->addParameter($k, $data[$key]);
	    			}
	    		}
	    	}
	    }

	    function find(){
	    	$searchables = ['id', 'name', 'type'];
	    	while (count($searchables) > 0){
	    		$search = array_shift($searchables);
	    		if($this->hasParameterAtKey($search)){
	    			$value = $this->getParameter($search);
    				$sql = "select
								cc.id as tag_id, cc.name as tag_name, cc.description as tag_description
							from shop_CatalogueCategories cc
							where cc.{$search} = :{$search};";
    				$query = SqlQuery::create($sql);
    				$query->query(["{$search}" => $value]);
    				$row = $query->fetch();
    				if(isset($row)){
    					$tagMapper = $this->getDI()->getObjectmapper('tag', (array) $row);
    					$tag = $tagMapper->inflate();
    					return $tag;
    				}
	    		}
	    	}
	    	return null;
	    }

	    function get(){
	    	$sql = "select
						ct.id as tag_id, ct.name as tag_name
					from shop_CatalogueTags ct
					where ct.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(["id" => $this->getParameter('id')]);
			$row = $query->fetch();
			if(isset($row)){
				$tagMapper = $this->getDI()->getObjectmapper('tag', (array) $row);
				return $tagMapper->inflate();
			}
	    }

	    function exists(){
	    }

	    function all(){
    		$sql = "select
						ct.id as tag_id, ct.name as tag_name
					from shop_CatalogueTags ct;";
	    	$query = SqlQuery::create($sql);
	    	$query->query();
	    	$rows = $query->fetchAll();
	    	$tags = [];
	    	foreach ($rows as $row){
	    		$tagMapper = $this->getDI()->getObjectmapper('tag', (array) $row);
	    		array_push($tags, $tagMapper->inflate());
	    	}
	    	return $tags;
	    }

	    function reset(array $data = null){}

	    function inflate(){
	    	return Tag::inflate($this);
	    }

	    function __get($name){
	    	if($this->hasParameterAtKey($name)){
	    		switch($name){
	    			case 'id':
	    				return intval($this->getParameter($name));
	    			case 'name':
	    			case 'description':
	    				return stripslashes(trim($this->getParameter($name)));
	    			case 'active':
	    				return true == $this->getParameter($name);
	    		}
	    	}
	    	return null;
	    }
	}
}