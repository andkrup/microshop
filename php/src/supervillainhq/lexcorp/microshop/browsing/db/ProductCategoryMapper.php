<?php
namespace supervillainhq\lexcorp\microshop\browsing\db{
	use supervillainhq\lexcorp\microshop\browsing\ProductCategory;
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\db\Mapper;

	class ProductCategoryMapper extends Mapper implements DataMapper, DataReader{

		function __construct($data = null, $lazyLoad = true){
			parent::__construct($data, $lazyLoad);
			if(!is_null($data)){
				if($data instanceof ProductCategory){
					$this->addParameter('id', $data->id());
					$this->addParameter('name', $data->name());
					$this->addParameter('description', $data->description());
					$this->addParameter('price', $data->price());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('category_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}

		function find(){
			$searchables = ['id', 'name', 'type'];
			while (count($searchables) > 0){
				$search = array_shift($searchables);
				if($this->hasParameterAtKey($search)){
					$value = $this->getParameter($search);
    				$sql = "select
								cc.id as category_id, cc.name as category_name, cc.description as category_description
							from shop_CatalogueCategories cc
							where cc.{$search} = :{$search};";
    				$query = SqlQuery::create($sql);
    				$query->query(["{$search}" => $value]);
    				$row = $query->fetch();
    				if(isset($row)){
    					$mapper = $this->getDI()->getObjectmapper('category', (array) $row);
    					$category = ProductCategory::inflate($mapper);
    					return $category;
    				}
				}
			}
			return null;
		}
		function get(){
			if(!$this->lazyLoad){
				// fetch the category + the partial tree
				$sql = "select
							cc.id as category_id, cc.name as category_name, cc.description as category_description
							, p.id as product_id, p.name as product_name, p.description as product_description
						from shop_CatalogueCategories cc
						left join shop_CatalogueCategoryProducts ccp on ccp.category_id = cc.id
						left join shop_Products p on p.id = ccp.product_id
						where cc.id = :id
						union
						select
							cc1.id as category_id, cc1.name as category_name, cc1.description as category_description
							, p.id as product_id, p.name as product_name, p.description as product_description
						from shop_CatalogueCategories cc1
						inner join (select cc1.id
							from shop_CatalogueCategories cc2, shop_CatalogueCategories cc1
							where cc2.`left` < cc1.`left`
							and cc2.`right` > cc1.`right`
							and cc2.left = (select `left`
								from shop_CatalogueCategories
								where id = :id
							)
							order by cc2.`right` - cc1.`right` asc
							) as children on children.id = cc1.id
						left join shop_CatalogueCategoryProducts ccp on ccp.category_id = cc1.id
						left join shop_Products p on p.id = ccp.product_id;";
			}
			else{
				// fetch only the category
				$sql = "select
							cc.id as category_id, cc.name as category_name, cc.description as category_description
							, p.id as product_id, p.name as product_name, p.description as product_description
						from shop_CatalogueCategories cc
						left join shop_CatalogueCategoryProducts ccp on ccp.category_id = cc.id
						left join shop_Products p on p.id = ccp.product_id
						where cc.id = :id;";
			}

			$query = SqlQuery::create($sql);
			$query->query(["id" => $this->getParameter('id')]);
			$rows = $query->fetchAll();
			$category = null;
			foreach ($rows as $row){
    			if(is_null($category)){
    				$category = $this->getDI()->getObjectmapper('category', (array) $row)->inflate();
    			}
				// the mapper is determined by the domain so it can also inflate the correct product class instance
				if($product = $this->getDI()->getObjectmapper('product', (array) $row)->inflate()){
					$category->addProduct($product);
				}
			}
			return $category;
		}
		function exists(){}
		function all(){
    		$sql = "select
						cc.id as category_id, cc.name as category_name, cc.description as category_description
						, p.id as product_id, p.name as product_name, p.description as product_description
					from shop_CatalogueCategories cc
					left join shop_CatalogueCategoryProducts ccp on ccp.category_id = cc.id
					left join shop_Products p on p.id = ccp.product_id;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$categories = [];
			$current = null;
			foreach ($rows as $row){
				$mapper = $this->getDI()->getObjectmapper('category', (array) $row);
    			$category = ProductCategory::inflate($mapper);
				if(!$category->equals($current)){
					$current = $category;
					array_push($categories, $current);
				}
				$mapper = $this->getDI()->getObjectmapper('product', (array) $row);
				if(!$mapper->emptyParameters('product')){
					// the mapper is determined by the domain so it can also inflate the correct product class instance
					$product = $mapper->inflate();
					if(!is_null($product)){
						$current->addProduct($product);
						unset($product);
					}
				}
			}
			return $categories;
		}
		function hierachical($flatArray = false){
			$sql = "select
						cc1.name as category_name, cc1.description as category_description
						, (select id
							from shop_CatalogueCategories cc2
							where cc2.`left` < cc1.`left`
							and cc2.`right` > cc1.`right`
							order by cc2.`right` - cc1.`right` asc
							limit 1) as category_parent_id, cc1.id as category_id
						, p.id as product_id, p.name as product_name, p.description as product_description
					from shop_CatalogueCategories cc1
					left join shop_CatalogueCategoryProducts ccp on ccp.category_id = cc1.id
					left join shop_Products p on p.id = ccp.product_id
					order by (cc1.`right` - cc1.`left`) desc;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$categories = [];
			$treeRefs = [];
			$parentRefs = [];
			$current = null;
			foreach ($rows as $row){
				$mapper = $this->getDI()->getObjectmapper('category', (array) $row);
				if($category = $mapper->inflate()){
					if(!$category->equals($current)){
						$current = $category;
						// set a reference to the parent category
						$treeRefs[$mapper->id] = $mapper->parent_id;

						$categories[$current->id()] = $current;
					}
					$mapper = $this->getDI()->getObjectmapper('product', (array) $row);
					// the mapper is determined by the domain so it can also inflate the correct product class instance
					if($product = $mapper->inflate()){
						$current->addProduct($product);
						unset($product);
					}
				}
			}
			// set parent & child references on each element and create a tree structure
			$root = null;
			foreach ($categories as $categoryId => $category){
				foreach ($treeRefs as $catId => $parentId){
					if(is_null($root) && is_null($parentId)){
						$root = $category;
						break;
					}
					if(!is_null($parentId)){
						if($categoryId == $catId){
							$parent = $categories[$parentId];
							$category->parentCategory($parent);
							$parent->addChild($category);
							break;
						}
					}
				}
			}
			if($flatArray){
				return self::flattenToArray($root);
			}
			return $root;
		}
		static function flattenToArray(ProductCategory $category, array $return = null){
			if(is_null($return)){
				// ensure that the root category is preserved in the flat array as the first element
				$return = [$category];
			}
			$children = $category->children();
			while (!empty($children)){
				$child = array_shift($children);
				array_push($return, $child);
				$return = self::flattenToArray($child, $return);
			}
			return $return;
		}

		function reset(array $data = null){}

		function inflate(){
			return ProductCategory::inflate($this);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
						return intval($this->getParameter($name));
					case 'parent_id':
						$value = $this->getParameter($name);
						if(is_null($value)){
							return null;
						}
						return intval($value);
					case 'name':
					case 'description':
						return stripslashes(trim($this->getParameter($name)));
					case 'active':
						return true == $this->getParameter($name);
				}
			}
			return null;
		}
	}
}