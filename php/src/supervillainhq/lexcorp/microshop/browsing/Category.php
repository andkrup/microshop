<?php
namespace supervillainhq\lexcorp\microshop\browsing{
	/**
	 * A product category
	 *
	 * @author ak
	 *
	 */
	interface Category{
		function name($name = null);
		function uri($uri = null);
		function parentCategory(Category $category = null);
		function rootCategory(Category $category = null);
		function resetSiblings(array $siblings = []);
		function addSibling(Category $sibling);
		function removeSibling(Category $sibling);
		function hasSibling(Category $sibling);
		function hasSiblings();
		function getSibling($index);
		function siblings();
		function resetChildren(array $childrens = []);
		function addChild(Category $children);
		function removeChild(Category $children);
		function hasChild(Category $children);
		function hasChildren();
		function getChild($index);
		function children();
		function isParentOf(Category $category);
		function isChildOf(Category $category);
		function isSiblingOf(Category $category);

		function equals(Category $category = null);
	}
}