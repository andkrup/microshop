<?php
namespace supervillainhq\lexcorp\microshop\browsing{
	use supervillainhq\core\db\DataAware;
	use supervillainhq\lexcorp\microshop\Product;
	use supervillainhq\spectre\db\DataReader;

	class ProductCategory implements Category{
		use Categorising, DataAware;

		protected $description;
		protected $products;


		function description( $description = null){
			if(is_null($description)){
				return $this->description;
			}
			$this->description = $description;
		}

		function resetProducts(array $products = []){
			$this->products = $products;
		}
		function addProduct(Product $product){
			array_push($this->products, $product);
		}
		function removeProduct(Product $product){
			$c = count($this->products);
			for($i = 0; $i < $c; $i++){
				if($this->products[$i] == $product){
					array_splice($this->products, $i, 0);
				}
			}
		}
		function hasProduct(Product $product){
			return in_array($this->products, $product);
		}
		function getProduct($index){
			return $this->products[$index];
		}
		function products(){
			return $this->products;
		}

		function __construct(){
			$this->resetChildren();
			$this->resetSiblings();
			$this->resetProducts();
		}

		static function inflate(DataReader $reader){
			$instance = new ProductCategory();
			$instance->id = $reader->id;
			$instance->name = $reader->name;
			$instance->description = $reader->description;
			return $instance;
		}

		function equals(Category $category = null){
			if(is_null($category)){
				return false;
			}
			return $category->id() == $this->id();
		}
	}
}