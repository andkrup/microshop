<?php
namespace supervillainhq\lexcorp\microshop\browsing{
	interface Navigator{
		function findCatalogueItems(array $search);
	}
}