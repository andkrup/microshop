<?php
namespace supervillainhq\lexcorp\microshop\browsing{
	use supervillainhq\core\db\DataAware;
	use supervillainhq\spectre\db\DataReader;

	class Tag{
		use DataAware;

		protected $name;

		function name($name = null){
			if(is_null($name)){
				return $this->name;
			}
			$this->name = $name;
		}

		function __toString(){
			return "{$this->name}";
		}

		function equals(Tag $tag = null){
			if(is_null($tag)){
				return false;
			}
			return $tag->id() == $this->id();
		}

		static function inflate(DataReader $reader){
			$instance = new Tag();
			$instance->id = $reader->id;
			$instance->name = $reader->name;
			return $instance;
		}
	}
}