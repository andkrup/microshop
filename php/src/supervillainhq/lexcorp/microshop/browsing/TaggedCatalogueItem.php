<?php
namespace supervillainhq\lexcorp\microshop\browsing{
	use supervillainhq\lexcorp\microshop\CatalogueItem;
	use supervillainhq\lexcorp\microshop\Product;
	use supervillainhq\spectre\cms\assets\FileAsset;

	class TaggedCatalogueItem implements CatalogueItem, Taggable{
		use Tagging;

		private $assets;
		private $product;

		function __construct(Product $product = null, array $assets = []){
			$this->product = $product;
			$this->resetAssets($assets);
		}

		function product(Product $product = null){
			if(is_null($product)){
				return $this->product;
			}
			$this->product = $product;
		}

		function resetAssets(array $assets = []){
			$this->assets = $assets;
		}
		function addAsset(FileAsset $asset){
			array_push($this->assets, $asset);
		}
		function removeAsset(FileAsset $asset){
			$c = count($this->assets);
			for($i = 0; $i < $c; $i++){
				if($this->assets[$i] == $asset){
					array_splice($this->assets, $i, 0);
				}
			}
		}
		function hasAsset(FileAsset $asset){
			return in_array($this->assets, $asset);
		}
		function getAsset($index){
			return $this->assets[$index];
		}
		function assets(){
			return $this->assets;
		}

	}
}