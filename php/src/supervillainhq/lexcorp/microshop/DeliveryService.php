<?php
namespace supervillainhq\lexcorp\microshop{
	use supervillainhq\core\Objectible;

	class DeliveryService implements \Serializable{
		use Objectible;

		private $name;
		private $label;
		private $price;

		function name( $name = null){
			if(is_null($name)){
				return $this->name;
			}
			$this->name = $name;
		}
		function label( $label = null){
			if(is_null($label)){
				return $this->label;
			}
			$this->label = $label;
		}
		function price( $price = null){
			if(is_null($price)){
				return $this->price;
			}
			$this->price = $price;
		}

		function serialize () {
			$object = $this->toObject();
			return serialize($object);
		}
		function unserialize ($serialized) {
		// handle the situations where an already unserialized object is passed by recursed unserialize calls
			if(is_null($serialized)){
				return;
			}
			elseif(is_object($serialized)){
				$this->name($serialized->name);
				$this->label($serialized->label);
				$this->price($serialized->price);
			}
			else{
				// $serialized is a string-interpretation and should be unserialized
				if(!($serialized instanceof DeliveryService)){
					$serialized = unserialize($serialized); // $serialized is now a stdClass/simple object
				}
				$this->name($serialized->name);
				$this->label($serialized->label);
				$this->price($serialized->price);
			}
		}

		static function fromObject($object){
			$instance = new DeliveryService();
			$instance->name = trim($object->name);
			$instance->label = trim($object->label);
			$instance->price = intval($object->price);
			return $instance;
		}
	}
}