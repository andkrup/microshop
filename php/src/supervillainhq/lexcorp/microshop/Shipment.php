<?php
namespace supervillainhq\lexcorp\microshop{
	use supervillainhq\core\db\DataAware;
	use supervillainhq\core\date\Date;
	use supervillainhq\spectre\db\DataReader;

	class Shipment{
		use DataAware;

		private $hash;
		private $created;
		private $product;
		private $originalAmount;
		private $currentAmount;


		function hash($string = null){
			if(is_null($string)){
				return $this->hash;
			}
			$this->hash = $string;
		}
		function created(\DateTime $date = null){
			if(is_null($date)){
				return $this->created;
			}
			$this->created = $date;
		}
		function product(Product $product = null){
			if(is_null($product)){
				return $this->product;
			}
			$this->product = $product;
		}
		function originalAmount($int = null){
			if(is_null($int)){
				return $this->originalAmount;
			}
			$this->originalAmount = $int;
		}
		function currentAmount($int = null){
			if(is_null($int)){
				return $this->currentAmount;
			}
			$this->currentAmount = $int;
		}

		static function inflate(DataReader $reader){
			$instance = new Shipment();
			$instance->hash = $reader->hash;
			$instance->created = new Date($reader->created);
			$instance->originalAmount = $reader->original_amount;
			$instance->currentAmount = $reader->current_amount;
			return $instance;
		}
	}
}