<?php
namespace supervillainhq\lexcorp\microshop{
	use supervillainhq\core\date\Date;
	use supervillainhq\core\db\DataAware;
	use supervillainhq\spectre\db\DataReader;

	class Product{
		use DataAware;

		protected $name;
		protected $description;
		protected $price;
		protected $created;
		protected $active;

		function name($name = null){
			if(!is_null($name)){
				$this->name = $name;
			}
			return $this->name;
		}
		function description($text = null){
			if(is_null($text)){
				return $this->description;
			}
			$this->description = $text;
		}
		function price($number = null){
			if(is_null($number)){
				return $this->price;
			}
			$this->price = $number;
		}
		function created(\DateTime $datetime = null){
			if(is_null($datetime)){
				return $this->created;
			}
			$this->created = $datetime;
		}
		function active($active = null){
			if(is_null($active)){
				return $this->active;
			}
			$this->active = $active;
		}

		function __construct(){
			$this->price = 0;
		}

		function __toString(){
			return "{$this->name}";
		}


		static function inflate(DataReader $reader){
			$instance = new Product();
			$instance->id = $reader->id;
			$instance->name = $reader->name;
			$instance->price = $reader->price;
			$instance->description = $reader->description;
			$instance->created = new Date($reader->created);
			$instance->active = $reader->active == true;
			return $instance;
		}
	}
}