<?php
namespace supervillainhq\lexcorp\microshop\http\formvalidation{
	use Phalcon\Validation;
	use Phalcon\Validation\Validator\Email;
	use Phalcon\Validation\Validator\Regex;

	class ContactInformationValidation extends Validation{
		public function initialize(){
			$this->add('email', new Email(['message' => 'Valid email required']));
			$this->add('phone', new Regex(['message' => 'Invalid phone', 'pattern' => '/[0-9]{8}/', 'allowEmpty' => true]));
		}
	}
}