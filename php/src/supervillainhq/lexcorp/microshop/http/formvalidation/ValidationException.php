<?php
namespace supervillainhq\lexcorp\microshop\http\formvalidation{
	use Phalcon\Validation\Message\Group;

	class ValidationException extends \Exception{
		private $messageGroup;

		function messageGroup(Group $group = null){
			if(is_null($group)){
				return $this->messageGroup;
			}
			$this->messageGroup = $group;
		}

		function __construct(Group $validationMessages, $message = '', $code = null, $previous = null){
			$this->messageGroup = $validationMessages;
			parent::__construct($message, $code, $previous);
		}

	}
}