<?php
namespace supervillainhq\lexcorp\microshop\http\formvalidation{
	use Phalcon\Validation;
	use Phalcon\Validation\Validator\StringLength;

	class AddressValidation extends Validation{
		public function initialize(){
			$this->add('fullname', new StringLength(['message' => 'First name and family name', 'min' => 3]));
			$this->add('att', new StringLength(['message' => 'Invalid ATT', 'min' => 3, 'allowEmpty' => true]));
			$this->add('address1', new StringLength(['message' => 'Address field required', 'min' => 3]));
			$this->add('address2', new StringLength(['message' => 'Address2 field invalid', 'min' => 3, 'allowEmpty' => true]));
			$this->add('postalcode', new StringLength(['message' => 'Invalid postal code', 'min' => 3, 'max' => 4]));
			$this->add('city', new StringLength(['message' => 'Invalid city', 'min' => 3]));
			$this->add('country', new StringLength(['message' => 'Invalid city', 'min' => 3, 'allowEmpty' => true]));
		}
	}
}