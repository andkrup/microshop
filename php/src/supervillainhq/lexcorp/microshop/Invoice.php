<?php
namespace supervillainhq\lexcorp\microshop{
	use supervillainhq\core\db\DataAware;
	use supervillainhq\core\date\Date;
	use supervillainhq\core\Objectible;
	use supervillainhq\lexcorp\microshop\payment\Transaction;
	use supervillainhq\lexcorp\microshop\payment\Currency;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\contacts\Address;
	use supervillainhq\spectre\contacts\ContactInfo;

	class Invoice implements \Serializable{
		use DataAware;
		use Objectible;

		private $created;
		private $hash;
		private $accepted;
		private $transaction;
		private $items;
		private $currency;
		private $total;
		private $vat;
		private $deliveryAddress;
		private $billingAddress;
		private $deliveryService;
		private $contactInfo;

		function created(Date $date = null){
			if(is_null($date)){
				return $this->created;
			}
			$this->created = $date;
		}
		function hash( $string = null){
			if(is_null($string)){
				return $this->hash;
			}
			$this->hash = $string;
		}
		function items(array $items = null){
			if(is_null($items)){
				return $this->items;
			}
			$this->items = $items;
		}
		function currency(Currency $currency = null){
			if(is_null($currency)){
				return $this->currency;
			}
			$this->currency = $currency;
		}
		function vatValue($number = null){
			if(is_null($number)){
				return round($this->vat / 100, 2);
			}
			$this->vat = $number;
		}
		function delivery(Address $address = null){
			if(is_null($address)){
				return $this->deliveryAddress;
			}
			$this->deliveryAddress = $address;
		}
		function billing(Address $address = null){
			if(is_null($address)){
				return $this->billingAddress;
			}
			$this->billingAddress = $address;
		}
		function deliveryService(DeliveryService $agent = null){
			if(is_null($agent)){
				return $this->deliveryService;
			}
			$this->deliveryService = $agent;
		}
		function contactInformation(ContactInfo $contactInfo = null){
			if(is_null($contactInfo)){
				return $this->contactInfo;
			}
			$this->contactInfo = $contactInfo;
		}
		function transaction(Transaction $transaction = null){
			if(is_null($transaction)){
				return $this->transaction;
			}
			$this->transaction = $transaction;
		}

		function __construct(Date $created = null){
			if(is_null($created)){
				$created = new Date();
			}
			$this->created = $created;
			$this->resetAccepted();
		}


		function resetAccepted(array $accepted = []){
			$this->accepted = $accepted;
		}
		function addAccepted($key, $accepted){
			$this->accepted[$key] = $accepted;
		}
		function removeAccepted($accepted){
			$c = count($this->accepted);
			for($i = 0; $i < $c; $i++){
				if($this->accepted[$i] == $accepted){
					array_splice($this->accepted, $i, 0);
				}
			}
		}
		function removeAcceptedAt($key){
			unset($this->accepted[$key]);
		}
		function hasAccepted($accepted){
			return in_array($this->accepted, $accepted);
		}
		function hasAcceptedAtKey($key){
			return array_key_exists($key, $this->accepted);
		}
		function getAccepted($key){
			return $this->accepted[$key];
		}
		function accepted(){
			return $this->accepted;
		}

		function total(){
			return $this->total;
		}
		function vat(){
			return ($this->total() / $this->vat) * 100;
		}

		function serialize () {
			$object = (object) [
				'created' => Date::fromObject($this->created->toObject()), // weird work-around forcing the serialization to not use references
				'accepted' => $this->accepted,
			];
			$serialized = serialize($object);
			return $serialized;
		}
		function unserialize ($serialized) {
			// handle the situations where an already unserialized object is passed by recursed unserialize calls
			if(is_null($serialized)){
				return;
			}
			elseif(is_object($serialized)){
				$date = Date::fromObject($serialized->created);
				$this->created($date);
				$this->resetAccepted($serialized->accepted);
			}
			else{
				// $serialized is a string-interpretation and should be unserialized
				if(!($serialized instanceof Invoice)){
					$serialized = unserialize($serialized); // $serialized is now a stdClass/simple object
				}
				$date = Date::fromObject($serialized->created);
				$this->created($date);
				$this->resetAccepted($serialized->accepted);
			}
		}

		/**
		 * Save to database for storage
		 */
		function save(){}

		/**
		 * Load from database and print as Pdf
		 */
		static function printAsPdf($invoice, $pdfTemplate = null){}

		static function inflate(DataReader $reader){
			$instance = new Invoice();
			$instance->id = $reader->id;
			$instance->hash = $reader->hash;
			$instance->total = $reader->total;
			$instance->vat = $reader->vat_value;
			$instance->currency = $reader->currency;
			$instance->items = $reader->items;
			$instance->billingAddress = $reader->billing_address;
			$instance->deliveryAddress = $reader->delivery_address;
			$instance->deliveryService = $reader->delivery_service;
			$instance->contactInfo = $reader->contact_information;
			$instance->created = $reader->created;
			return $instance;
		}

		/**
		 * Fill in all the required information that an invoice needs to have and lock it for future edits
		 * @param Invoice $invoice The current invoice
		 * @param Cart $cart The cart instance that holds the items
		 * @param Entity $entity The shopper entity that has the invoice and delivery addresses and contact information
		 * @param DeliveryService $service An object that holds information about the chosen delivery method and its costs
		 * @param Transaction $transaction The virgin related transation object that will later contain information about payment transactions
		 */
		static function export(Invoice $invoice, Cart $cart, Entity $entity, DeliveryService $service, Transaction $transaction){
			$cartItems = $cart->items();
			$total = $cart->total() + $service->price();
			$currency = $cart->currency();
			$billingAddress = $entity->getAddress('invoice');
			$deliveryAddress = $entity->getAddress('delivery');
			$contactInfo = $entity->contactInfo();
			$hash = md5(time() . rand() . $invoice->id());

			$serialized = [
					'invoice' => serialize($invoice),
					'items' => serialize($cartItems),
					'hash' => $hash,
					'total' => $total,
					'currency' => $currency->code(),
					'vat' => $cart->vatValue() * 100,
					'billing' => serialize($billingAddress),
					'delivery' => serialize($deliveryAddress),
					'contactinfo' => serialize($contactInfo),
					'service' => serialize($service),
					'transaction' => $transaction->id(),
					'filepath' => '/tmp/',
			];
			return $serialized;
		}
	}
}