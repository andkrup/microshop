<?php

namespace supervillainhq\lexcorp\microshop\front {
	use Phalcon\Config;
	use Phalcon\DiInterface;
	use Phalcon\Mvc\View;

	/**
	 * Created by ak.
	 */
	class MicroFront {

		protected $di;
		protected $frontendAppDir;

		function __construct(DiInterface $di, Config $config){
			$this->di = $di;

			$this->frontendAppDir = $config->shopfront->frontend_appdir;
//			var_dump($config->shopfront);exit;
//			$this->shopName = $config->microshop->{'general-info'}->name;
//			$this->shopCvr = $config->microshop->{'general-info'}->cvr;
		}

		/**
		 * Wrapper for view->partial()
		 * @param $partialPath
		 * @param array $params
		 */
		function partial($partialPath, array $params = []){
//			$dir = getcwd();
//			var_dump("{$this->frontendAppDir}/{$partialPath}");exit;
			$view = new View();
			return $view->partial("{$this->frontendAppDir}/views/{$partialPath}", $params);
		}
	}
}
