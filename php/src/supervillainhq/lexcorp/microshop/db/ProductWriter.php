<?php
namespace supervillainhq\lexcorp\microshop\db{
	use supervillainhq\core\db\DataWriter;
	use supervillainhq\core\db\ParameterContainer;
	use supervillainhq\core\db\Reading;
	use supervillainhq\lexcorp\microshop\Product;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\DependencyInjecting;

	class ProductWriter implements DataWriter{
		use DependencyInjecting, ParameterContainer, Reading;

		function __construct($data){
			if(!is_null($data)){
				if($data instanceof Product){
					$this->addParameter('id', $data->id());
					$this->addParameter('name', $data->name());
					$this->addParameter('description', $data->description());
					$this->addParameter('price', $data->price());
					$this->addParameter('created', $data->price());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('product_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}
		function create(){
			$sql = "insert into shop_Products
					(name, price, description, size)
					values (:name, :price, :description, :size);";
			$parameters = [
					'name' => $this->name,
					'brand_id' => $this->brand,
					'type' => $this->type,
					'size' => $this->size,
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
			return $query->lastInsertId();
		}
		function update(){
			$sql = "update shop_Products set
					name = :name,
					brand_id = :brand,
					type = :type,
					size = :size
					where id = :id;";
			$parameters = [
					'name' => $this->name,
					'brand' => $this->brand,
					'type' => $this->type,
					'size' => $this->size,
					'id' => $this->id,
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
		}
		function delete(){}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
					case 'price':
						return intval($this->getParameter($name));
					case 'name':
						return stripslashes(trim($this->getParameter($name)));
				}
			}
			return null;
		}
	}
}