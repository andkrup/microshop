<?php
namespace supervillainhq\lexcorp\microshop\db{
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\db\Mapper;
	use supervillainhq\lexcorp\microshop\Product;

	class BaseProductMapper extends Mapper implements DataMapper, DataReader{

	    function __construct($data = null, $lazyLoad = true){
	    	parent::__construct($data, $lazyLoad);
	    	if(!is_null($data)){
	    		if($data instanceof Product){
	    			$this->addParameter('id', $data->id());
	    			$this->addParameter('name', $data->name());
	    			$this->addParameter('description', $data->description());
	    			$this->addParameter('price', $data->price());
	    		}
	    		elseif (is_array($data)){
	    			$this->resetParameters();
	    			$keys = array_keys($data);
	    			foreach ($keys as $key){
	    				$k = str_ireplace('product_', '', $key);
	    				$this->addParameter($k, $data[$key]);
	    			}
	    		}
	    	}
	    }

	    function find(){
	    	$searchables = ['id', 'name', 'type'];
	    	while (count($searchables) > 0){
	    		$search = array_shift($searchables);
	    		if($this->hasParameterAtKey($search)){
	    			$value = $this->getParameter($search);
    				$sql = "select
								p.id as product_id, p.name as product_name, p.description as product_description
							from shop_Products p
							where p.{$search} = :{$search};";
    				$query = SqlQuery::create($sql);
    				$query->query(["{$search}" => $value]);
    				$row = $query->fetch();
    				if(isset($row)){
    					$mapper = new ProductMapper((array ) $row);
    					$product = $mapper->inflate();
    					return $product;
    				}
	    		}
	    	}
	    	return null;
	    }
	    function get(){
			var_dump($this->getParameter('id'));
	    	$sql = "select
					p.id as product_id, p.name as product_name, p.description as product_description
				from shop_Products p
				where p.id = :id
	    		and p.parent_id is null;";
			$query = SqlQuery::create($sql);
			$query->query(["id" => $this->getParameter('id')]);
			$row = $query->fetch();
			if(isset($row)){
				$mapper = new ProductMapper((array) $row);
				$product = $mapper->inflate();
				return $product;
			}
	    }
	    function exists(){}
	    function all(){
    		$sql = "select
						p.id as product_id, p.name as product_name, p.description as product_description
					from Products p;";
	    	$query = SqlQuery::create($sql);
	    	$query->query();
	    	$rows = $query->fetchAll();
	    	$products = [];
	    	foreach ($rows as $row){
	    		$product = Product::create(new ProductMapper((array) $row));
	    		array_push($products, $product);
	    	}
	    	return $products;
	    }

	    function reset(array $data = null){}

	    function inflate(){
	    	return Product::inflate($this);
	    }

	    function __get($name){
	    	if($this->hasParameterAtKey($name)){
	    		switch($name){
	    			case 'id':
	    			case 'price':
	    				return intval($this->getParameter($name));
	    			case 'name':
	    			case 'description':
	    				return stripslashes(trim($this->getParameter($name)));
	    		}
	    	}
	    	return null;
	    }
	}
}