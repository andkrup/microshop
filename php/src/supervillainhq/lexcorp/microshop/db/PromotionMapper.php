<?php
namespace supervillainhq\lexcorp\microshop\db{
	use supervillainhq\lexcorp\microshop\Promotion;
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\Mapper;
	use supervillainhq\spectre\db\SqlQuery;

	class PromotionMapper extends Mapper implements DataMapper, DataReader{

	    function __construct($data = null, $lazyLoad = true){
	    	parent::__construct($data, $lazyLoad);
	    	if(!is_null($data)){
	    		if($data instanceof Promotion){
	    			$this->addParameter('id', $data->id());
	    			$this->addParameter('name', $data->name());
	    			$this->addParameter('brand', $data->brand());
	    			$this->addParameter('type', $data->type());
	    			$this->addParameter('size', $data->size());
	    		}
	    		elseif (is_array($data)){
	    			$this->resetParameters();
	    			$keys = array_keys($data);
	    			foreach ($keys as $key){
	    				$k = str_ireplace('promotion_', '', $key);
	    				$this->addParameter($k, $data[$key]);
	    			}
	    		}
	    	}
	    }

	    function find(){
	    	$searchables = ['id', 'name', 'type'];
	    	while (count($searchables) > 0){
	    		$search = array_shift($searchables);
	    		if($this->hasParameterAtKey($search)){
	    			$value = $this->getParameter($search);
    				$sql = "select
								p.id as promotion_id, p.name as promotion_name, p.description as promotion_description
							from shop_Promotions p
							where p.{$search} = :{$search};";
    				$query = SqlQuery::create($sql);
    				$query->query(["{$search}" => $value]);
    				$row = $query->fetch();
    				if(isset($row)){
    					$mapper = new PromotionMapper((array) $row);
    					$promotion = $mapper->inflate();
    					return $promotion;
    				}
	    		}
	    	}
	    	return null;
	    }
	    function get(){
	    	$sql = "select
					p.id as promotion_id, p.name as promotion_name, p.price as promotion_price, p.description as promotion_description
				from shop_Promotions p
				where p.id = :id
	    		and p.parent_id is not null;";
			$query = SqlQuery::create($sql);
			$query->query(["id" => $this->getParameter('id')]);
			$row = $query->fetch();
			if(isset($row)){
				$mapper = new PromotionMapper((array) $row);
				$promotion = $mapper->inflate();
				return $promotion;
			}
	    }
	    function exists(){}

	    function all(){
    		$sql = "select
						p.id as promotion_id, p.name as promotion_name, p.description as promotion_description
					from Promotions p;";
	    	$query = SqlQuery::create($sql);
	    	$query->query();
	    	$rows = $query->fetchAll();
	    	$promotions = [];
	    	foreach ($rows as $row){
	    		$promotion = Promotion::create(new PromotionMapper((array) $row));
	    		array_push($promotions, $promotion);
	    	}
	    	return $promotions;
	    }

	    function reset(array $data = null){}

	    function inflate(){
	    	return Promotion::inflate($this);
	    }

	    function __get($name){
	    	if($this->hasParameterAtKey($name)){
	    		switch($name){
	    			case 'id':
	    				return intval($this->getParameter($name));
	    			case 'price':
	    				return floatval($this->getParameter($name));
	    			case 'name':
	    			case 'description':
	    				return stripslashes(trim($this->getParameter($name)));
	    		}
	    	}
	    	return null;
	    }
	}
}