<?php
namespace supervillainhq\lexcorp\microshop\db{
	use supervillainhq\lexcorp\microshop\Shipment;
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\db\Mapper;
	use supervillainhq\tobissen\shop\TobissenWobler;
	use supervillainhq\tobissen\db\TobissenWoblerMapper;

	class ShipmentMapper extends Mapper implements DataMapper, DataReader{
	    function __construct($data = null, $lazyLoad = true){
	    	parent::__construct($data, $lazyLoad);
	    	if(!is_null($data)){
	    		if($data instanceof Shipment){
	    			$this->addParameter('id', $data->id());
	    			$this->addParameter('leader_dia', $data->leaderDiameter());
	    		}
	    		elseif (is_array($data)){
	    			$keys = array_keys($data);
	    			foreach ($keys as $key){
	    				$k = str_ireplace('shipment_', '', $key);
	    				$this->addParameter($k, $data[$key]);
	    			}
	    		}
	    	}
	    }

	    function find(){}
	    function get(){}
	    function exists(){}
	    function all(){
	    	$sql = "select
	    				iss.id as shipment_id, iss.hash as shipment_hash, iss.original_amount as shipment_original_amount, iss.current_amount as shipment_current_amount, iss.created as shipment_created
	    				, p.id as product_id, p.name as product_name
					from shop_InventoryShipments iss
					left join shop_Products p on p.id = iss.product_id;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$shipments = [];
			foreach ($rows as $row){
				$shipmentMapper = new ShipmentMapper((array) $row);
				$productMapper = new TobissenWoblerMapper((array) $row);
				$shipment = $shipmentMapper->inflate();
				$product = $productMapper->inflate();
				$shipment->product($product);
				array_push($shipments, $shipment);
			}
			return $shipments;
	    }

	    function reset(array $data = null){}

	    function inflate(){
	    	return Shipment::inflate($this);
	    }

	    function __get($name){
	    	if($this->hasParameterAtKey($name)){
				switch($name){
					case 'hash':
						return stripslashes(trim($this->getParameter($name)));
					case 'product_id':
					case 'amount':
					case 'original_amount':
					case 'current_amount':
						return intval($this->getParameter($name));
				}
			}
	    	return null;
	    }
	}
}