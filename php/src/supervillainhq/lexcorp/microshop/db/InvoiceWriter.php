<?php
namespace supervillainhq\lexcorp\microshop\db{

	use supervillainhq\core\db\DataWriter;
	use supervillainhq\core\db\ParameterContainer;
	use supervillainhq\core\db\Reading;
	use supervillainhq\lexcorp\microshop\Invoice;
	use supervillainhq\spectre\DependencyInjecting;
	use supervillainhq\spectre\db\SqlQuery;

	class InvoiceWriter implements DataWriter{
		use DependencyInjecting, ParameterContainer, Reading;

		function __construct($data){
			if(!is_null($data)){
				if($data instanceof Invoice){
					$this->addParameter('id', $data->id());
					$this->addParameter('hash', $data->hash());
// 					$this->addParameter('items', $data->id());
// 					$this->addParameter('total', $data->id());
// 					$this->addParameter('currency', $data->id());
// 					$this->addParameter('delivery', $data->id());
// 					$this->addParameter('billing', $data->id());
// 					$this->addParameter('contactinfo', $data->id());
// 					$this->addParameter('service', $data->id());
// 					$this->addParameter('filepath', $data->id());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('invoice_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}
		function create(){
			$sql = "insert into shop_Invoices
					(items, hash, total, currency, delivery, billing, contact_info, service, transaction_id, file_path, created)
					values (:items, :hash, :total, :currency, :delivery, :billing, :contactinfo, :service, :transaction, :filepath, now());";
			$parameters = [
					'items' => $this->items,
					'hash' => $this->hash,
					'total' => $this->total,
					'currency' => $this->currency,
					'delivery' => $this->delivery,
					'billing' => $this->billing,
					'contactinfo' => $this->contactinfo,
					'service' => $this->service,
					'transaction' => $this->transaction,
					'filepath' => $this->filepath
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
			return $query->lastInsertId();
		}
		function update(){
			$sql = "update shop_Invoices set
					line_dia = :line_dia
					where id = :id;";
			$parameters = [
					'id' => $this->id,
					'line_dia' => $this->line_dia
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
		}
		function delete(){}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'transaction':
					case 'total':
					case 'vat_value':
					case 'vat_value':
						return intval($this->getParameter($name));
					case 'items':
					case 'currency':
					case 'delivery':
					case 'billing':
					case 'contactinfo':
					case 'service':
					case 'filepath':
					case 'hash':
						return trim($this->getParameter($name));
				}
			}
			return null;
		}
	}
}