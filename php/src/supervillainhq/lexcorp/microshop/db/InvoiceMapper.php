<?php
namespace supervillainhq\lexcorp\microshop\db{
	use supervillainhq\core\date\Date;
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\Mapper;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\lexcorp\microshop\Invoice;
	use supervillainhq\lexcorp\microshop\MicroCartItem;
	use supervillainhq\lexcorp\microshop\payment\Currency;

	class InvoiceMapper extends Mapper implements DataMapper, DataReader{
	    function __construct($data = null, $lazyLoad = true){
			parent::__construct($data, $lazyLoad);
			if(!is_null($data)){
				if($data instanceof Invoice){
					$this->addParameter('id', $data->id());
					$this->addParameter('created', $data->created());
					$this->addParameter('hash', $data->hash());
					$this->addParameter('transaction', $data->transaction()->id());
				}
				elseif (is_array($data)){
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('invoice_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
	    }

	    function find(){
			$searchables = ['id', 'transaction_id', 'hash'];
			while (count($searchables) > 0){
				$search = array_shift($searchables);
				if($this->hasParameterAtKey($search)){
					$value = $this->getParameter($search);
					$sql = "select
								ics.id as invoice_id
									, ics.hash as invoice_hash
									, ics.items as invoice_items
									, ics.total as invoice_total
									, ics.vat_value as invoice_vat_value
									, ics.transaction_id as invoice_transaction_id
									, ics.currency as invoice_currency
									, ics.delivery as invoice_delivery_address
									, ics.billing as invoice_billing_address
									, ics.service as invoice_delivery_service
									, ics.contact_info as invoice_contact_information
								, t.id as transaction_id, t.gateway as transaction_gateway, t.gateway_id as transaction_gateway_id
							from shop_Invoices ics
							left join shop_Transactions t on t.id = ics.transaction_id
							where ics.{$search} = :{$search};";
					$query = SqlQuery::create($sql);
					$query->query(["{$search}" => $value]);
					$row = $query->fetch();
					if(isset($row)){
						$invoiceMapper = $this->getDI()->getObjectmapper('invoice', (array) $row);
						$transactionMapper = $this->getDI()->getObjectmapper('transaction', (array) $row);

						$instance = $invoiceMapper->inflate();
						$instance->transaction($transactionMapper->inflate());
						return $instance;
					}
				}
			}
			return null;
	    }

	    function get(){
			$sql = "select
					ics.id as invoice_id
						, ics.hash as invoice_hash
						, ics.items as invoice_items
						, ics.total as invoice_total
						, ics.vat_value as invoice_vat_value
						, ics.transaction_id as invoice_transaction_id
						, ics.currency as invoice_currency
						, ics.delivery as invoice_delivery_address
						, ics.billing as invoice_billing_address
						, ics.service as invoice_delivery_service
						, ics.contact_info as invoice_contact_information
					, t.id as transaction_id, t.gateway as transaction_gateway, t.gateway_id as transaction_gateway_id
					from shop_Invoices ics
					left join shop_Transactions t on t.id = ics.transaction_id
					where ics.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->id]);
			$row = $query->fetch();
			$invoiceMapper = $this->getDI()->getObjectmapper('invoice', (array) $row);
			$transactionMapper = $this->getDI()->getObjectmapper('transaction', (array) $row);
			$instance = $invoiceMapper->inflate();
			$instance->transaction($transactionMapper->inflate());
			return $instance;
	    }
	    function exists(){}
	    function all(){
			$sql = "select
						ics.id as invoice_id
							, ics.hash as invoice_hash
							, ics.items as invoice_items
							, ics.total as invoice_total
							, ics.vat_value as invoice_vat_value
							, ics.transaction_id as invoice_transaction_id
							, ics.currency as invoice_currency
							, ics.delivery as invoice_delivery_address
							, ics.billing as invoice_billing_address
							, ics.service as invoice_delivery_service
							, ics.contact_info as invoice_contact_information
						, t.id as transaction_id, t.gateway as transaction_gateway, t.error_code as transaction_error_code, t.gateway_id as transaction_gateway_id
					from shop_Invoices ics
					left join shop_Transactions t on t.id = ics.transaction_id;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$invoices = [];
			foreach ($rows as $row){
				$invoiceMapper = $this->getDI()->getObjectmapper('invoice', (array) $row);
				$invoice = $invoiceMapper->inflate();
				$transactionMapper = $this->getDI()->getObjectmapper('transaction', (array) $row);
				if($transaction = $transactionMapper->inflate()){
					$invoice->transaction($transaction);
				}
				array_push($invoices, $invoice);
			}
			return $invoices;
	    }

	    function reset(array $data = null){}

	    function inflate(){
			return Invoice::inflate($this);
	    }

	    function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'hash':
						return stripslashes(trim($this->getParameter($name)));
					case 'id':
					case 'product_id':
					case 'amount':
					case 'original_amount':
					case 'current_amount':
					case 'total':
					case 'vat_value':
					case 'transaction':
					case 'transaction_id':
						return intval($this->getParameter($name));
					case 'created':
						return Date::createFromFormat('Y-m-d H:i:s', $this->getParameter($name));
					case 'currency':
						return new Currency($this->getParameter($name));
					case 'items':
						$items = unserialize($this->getParameter($name));
						if(is_array($items)){
							$cartItems = [];
							foreach ($items as $item){
								$cartItem = MicroCartItem::hydrate($item);
								array_push($cartItems, $cartItem);
							}
							return $cartItems;
						}
						return $items;
					case 'billing_address':
					case 'delivery_address':
					case 'delivery_service':
					case 'contact_information':
						return unserialize($this->getParameter($name));
				}
			}
			return null;
	    }
	}
}