<?php
namespace supervillainhq\lexcorp\microshop\db{
	use supervillainhq\lexcorp\microshop\Invoice;
	use supervillainhq\lexcorp\microshop\payment\Transaction;
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\db\Mapper;

	class TransactionMapper extends Mapper implements DataMapper, DataReader{
		function __construct($data = null, $lazyLoad = true){
			parent::__construct($data, $lazyLoad);
			if(!is_null($data)){
				if($data instanceof Invoice){
					$this->addParameter('id', $data->id());
					$this->addParameter('leader_dia', $data->leaderDiameter());
				}
				elseif (is_array($data)){
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('transaction_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}

		function find(){
			if($this->hasParameterAtKey('invoice_id')){
				return $this->findByInvoice();
			}
			$searchables = ['id', 'gateway_id'];
			while (count($searchables) > 0){
				$search = array_shift($searchables);
				if($this->hasParameterAtKey($search)){
					$value = $this->getParameter($search);
					$sql = "select
								t.id as transaction_id,
								t.gateway as transaction_gateway,
								t.error_code as transaction_error_code,
								t.amount as transaction_amount,
								t.gateway_id as transaction_gateway_id
							from shop_Transactions t
							where t.{$search} = :{$search};";
					$query = SqlQuery::create($sql);
					$query->query(["{$search}" => $value]);
					$row = $query->fetch();
					if(isset($row)){
						$mapper = $this->getDI()->getObjectmapper('transaction', (array) $row);
						$instance = $mapper->inflate();
						return $instance;
					}
				}
			}
			return null;
		}
		private function findByInvoice(){
			$sql = "select
						t.id as transaction_id,
						t.gateway as transaction_gateway,
						t.error_code as transaction_error_code,
						t.amount as transaction_amount,
						t.gateway_id as transaction_gateway_id
					from shop_Transactions t
					left join shop_Invoices i on i.transaction_id = t.id
					where i.id = :invoice_id;";
			$query = SqlQuery::create($sql);
			$query->query(['invoice_id' => $this->invoice_id]);
			$row = $query->fetch();
			$mapper = $this->getDI()->getObjectmapper('transaction', (array) $row);
			return $mapper->inflate();
		}

		function get(){
			$sql = "select
						t.id as transaction_id,
						t.gateway as transaction_gateway,
						t.error_code as transaction_error_code,
						t.gateway_id as transaction_gateway_id,
						t.amount as transaction_amount,
						ics.id as invoice_id
					from shop_Transactions t
					left join shop_Invoices ics on ics.transaction_id = t.id
					where t.id = :id;";
// 			$sql = "select
// 						ics.id as invoice_id
// 							, ics.hash as invoice_hash
// 							, ics.items as invoice_items
// 							, ics.total as invoice_total
// 							, ics.vat_value as invoice_vat_value
// 							, ics.transaction_id as invoice_transaction_id
// 							, ics.currency as invoice_currency
// 							, ics.delivery as invoice_delivery_address
// 							, ics.billing as invoice_billing_address
// 							, ics.service as invoice_delivery_service
// 							, ics.contact_info as invoice_contact_information
// 						, t.id as transaction_id,
// 						t.gateway as transaction_gateway,
// 						t.error_code as transaction_error_code,
// 						t.gateway_id as transaction_gateway_id
// 					from shop_Transactions t
// 					left join shop_Invoices ics on ics.transaction_id = t.id
// 					where t.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(["id" => $this->id]);
			$row = $query->fetch();
			if(isset($row)){
				$mapper = $this->getDI()->getObjectmapper('transaction', (array) $row);
// 				$invoiceMapper = $this->getDI()->getObjectmapper('invoice', (array) $row);
				$transaction = $mapper->inflate();
// 				if($invoice = $invoiceMapper->get()){
// 					$transaction->invoice($invoice);
// 				}
				return $transaction;
			}
		}

		function exists(){}
		function all(){
			$sql = "select
						t.id as transaction_id,
						t.gateway as transaction_gateway,
						t.error_code as transaction_error_code,
						t.amount as transaction_amount,
						t.gateway_id as transaction_gateway_id
					from shop_Transactions t;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$transactions = [];
			foreach ($rows as $row){
				$transactionMapper = $this->getDI()->getObjectmapper('transaction', (array) $row);
				$transaction = $transactionMapper->inflate();
				array_push($transactions, $transaction);
			}
			return $transactions;
		}

		function reset(array $data = null){}

		function inflate(){
			return Transaction::inflate($this);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'hash':
					case 'gateway':
						return stripslashes(trim($this->getParameter($name)));
					case 'id':
					case 'gateway_id':
					case 'invoice_id':
					case 'amount':
					case 'original_amount':
					case 'current_amount':
					case 'error_code':
						return intval($this->getParameter($name));
				}
			}
			return null;
		}
	}
}