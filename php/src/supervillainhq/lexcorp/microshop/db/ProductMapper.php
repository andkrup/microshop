<?php
namespace supervillainhq\lexcorp\microshop\db{
	use supervillainhq\lexcorp\microshop\Product;
	use supervillainhq\lexcorp\microshop\browsing\Tag;
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\db\Mapper;

	class ProductMapper extends Mapper implements DataMapper, DataReader{

	    function __construct($data = null, $lazyLoad = true){
	    	parent::__construct($data, $lazyLoad);
	    	if(!is_null($data)){
	    		if($data instanceof Product){
	    			$this->addParameter('id', $data->id());
	    			$this->addParameter('name', $data->name());
	    			$this->addParameter('description', $data->description());
	    			$this->addParameter('price', $data->price());
	    		}
	    		elseif($data instanceof Tag){
	    			$this->addParameter('tag_id', $data->id());
	    		}
	    		elseif (is_array($data)){
	    			$this->resetParameters();
	    			$keys = array_keys($data);
	    			foreach ($keys as $key){
	    				$k = str_ireplace('product_', '', $key);
	    				$this->addParameter($k, $data[$key]);
	    			}
	    		}
	    	}
	    }

	    function find(){
	    	if($this->hasParameterAtKey('tag_id')){
	    		var_dump('find by tag');exit;
	    	}
	    	$searchables = ['id', 'name', 'type'];
	    	while (count($searchables) > 0){
	    		$search = array_shift($searchables);
	    		if($this->hasParameterAtKey($search)){
	    			$value = $this->getParameter($search);
    				$sql = "select
								p.id as product_id, p.name as product_name, p.description as product_description, p.active as product_active
							from shop_Products p
							where p.{$search} = :{$search};";
    				$query = SqlQuery::create($sql);
    				$query->query(["{$search}" => $value]);
    				$row = $query->fetch();
    				if(isset($row)){
    					$mapper = $this->getDI()->getObjectmapper('product', (array) $row);
    					return $mapper->inflate();
    				}
	    		}
	    	}
	    	return null;
	    }

		    /**
	     *
	     * @return array A list of all the products that is tagged with the tag
	     */
	    function tagged(){
    		$sql = "select
						p.id as product_id, p.name as product_name, p.description as product_description
					from shop_CatalogueTags ct
					left join shop_CatalogueTagProducts ctp on ctp.tag_id = ct.id
					left join shop_Products p on p.id = ctp.product_id
    				where ct.id = :id;";
	    	$query = SqlQuery::create($sql);
	    	$query->query(['id' => $this->getParameter('tag_id')]);
	    	$rows = $query->fetchAll();
	    	$products = [];
	    	foreach ($rows as $row){
	    		$productMapper = $this->getDI()->getObjectmapper('product', (array) $row);
	    		$product = $productMapper->inflate();
	    		if(!is_null($product)){
	    			array_push($products, $product);
	    		}
	    	}
	    	return $products;
	    }


	    function get(){
	    	$sql = "select
					p.id as product_id, p.name as product_name, p.price as product_price, p.description as product_description, p.active as product_active
				from shop_Products p
				where p.id = :id
	    		and p.parent_id is not null;";
			$query = SqlQuery::create($sql);
			$query->query(["id" => $this->getParameter('id')]);
			$row = $query->fetch();
			if(isset($row)){
				$mapper = $this->getDI()->getObjectmapper('product', (array) $row);
				return $mapper->inflate();
			}
	    }
	    function exists(){}
	    function all(){
    		$sql = "select
						p.id as product_id, p.name as product_name, p.description as product_description, p.active as product_active
					from shop_Products p
    				where p.parent_id is not null;";
	    	$query = SqlQuery::create($sql);
	    	$query->query();
	    	$rows = $query->fetchAll();
	    	$products = [];
	    	foreach ($rows as $row){
	    		$mapper = $this->getDI()->getObjectmapper('product', (array) $row);
	    		if($product = $mapper->inflate()){
	    			array_push($products, $product);
	    		}
	    	}
	    	return $products;
	    }
	    function reset(array $data = null){}

	    function inflate(){
	    	return Product::inflate($this);
	    }

	    function __get($name){
	    	if($this->hasParameterAtKey($name)){
	    		switch($name){
	    			case 'id':
	    			case 'price':
	    				return intval($this->getParameter($name));
	    			case 'name':
	    			case 'description':
	    				return stripslashes(trim($this->getParameter($name)));
	    			case 'active':
	    				return true == $this->getParameter($name);
	    		}
	    	}
	    	return null;
	    }
	}
}