<?php
namespace supervillainhq\lexcorp\microshop\db{

	use supervillainhq\core\db\DataWriter;
	use supervillainhq\core\db\ParameterContainer;
	use supervillainhq\core\db\Reading;
	use supervillainhq\lexcorp\microshop\payment\Transaction;
	use supervillainhq\spectre\DependencyInjecting;
	use supervillainhq\spectre\db\SqlQuery;

	class TransactionWriter implements DataWriter{
		use DependencyInjecting, ParameterContainer, Reading;

		function __construct($data){
			if(!is_null($data)){
				if($data instanceof Transaction){
					$this->addParameter('id', $data->id());
					$this->addParameter('status', $data->status());
					$this->addParameter('gateway', $data->gateway()->name());
					$this->addParameter('gateway_id', $data->gatewayTransactionID());
					$this->addParameter('amount', $data->amount());
// 					$this->addParameter('currency', $data->id());
// 					$this->addParameter('delivery', $data->id());
// 					$this->addParameter('billing', $data->id());
// 					$this->addParameter('contactinfo', $data->id());
// 					$this->addParameter('filepath', $data->id());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('transaction_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}
		function create(){
			$sql = "insert into shop_Transactions
					(gateway, created)
					values (:gateway, now());";
			$parameters = [
					'gateway' => $this->gateway
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
			return $query->lastInsertId();
		}
		function update(){
			$sql = "update shop_Transactions set
					amount = :amount,
					error_code = :error_code,
					authorized = :authorized,
					rejected = :rejected,
					captured = :captured,
					capture_rejected = :capture_rejected,
					gateway_id = :gateway_id
					where id = :id;";
			$parameters = [
					'id' => $this->id,
					'amount' => $this->amount,
					'gateway_id' => $this->gateway_id,
					'error_code' => $this->error_code,
					'rejected' => $this->rejected,
					'captured' => $this->captured,
					'capture_rejected' => $this->capture_rejected,
					'authorized' => $this->authorized
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
		}
		function delete(){}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
					case 'amount':
					case 'error_code':
						return intval($this->getParameter($name));
					case 'gateway':
					case 'gateway_id':
						return stripslashes(trim($this->getParameter($name)));
					case 'authorized':
					case 'rejected':
					case 'captured':
					case 'capture_rejected':
						$value = $this->getParameter($name);
						if($value instanceof \DateTime){
							return $value->format('Y-m-d H:i:s');
						}
						$return = stripslashes(trim($value));
						return strlen($return) > 0 ? $return : null;
				}
			}
			return null;
		}
	}
}