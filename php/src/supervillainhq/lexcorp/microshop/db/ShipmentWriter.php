<?php
namespace supervillainhq\lexcorp\microshop\db{
	use supervillainhq\core\db\DataWriter;
	use supervillainhq\core\db\ParameterContainer;
	use supervillainhq\core\db\Reading;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\DependencyInjecting;

	class ShipmentWriter implements DataWriter{
		use DependencyInjecting, ParameterContainer, Reading;

		function __construct($data){
			if(!is_null($data)){
				if($data instanceof Shipment){
					$this->addParameter('id', $data->id());
					$this->addParameter('line_dia', $data->lineDiameter());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('shipment_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}
		function create(){
			$sql = "insert into shop_InventoryShipments
					(hash, product_id, original_amount, current_amount, created)
					values (:hash, :product_id, :amount, :amount, now());";
			$parameters = [
					'hash' => $this->hash,
					'product_id' => $this->product_id,
					'amount' => $this->amount
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
		}
		function update(){
			$sql = "update shop_InventoryShipments set
					line_dia = :line_dia
					where id = :id;";
			$parameters = [
					'id' => $this->id,
					'line_dia' => $this->line_dia
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
		}
		function delete(){}

		function __get($name){
			$val = parent::__get($name);
			if(is_null($val)){
				if($this->hasParameterAtKey($name)){
					switch($name){
						case 'hash':
							return stripslashes(trim($this->getParameter($name)));
						case 'product_id':
						case 'amount':
						case 'original_amount':
						case 'current_amount':
							return intval($this->getParameter($name));
					}
				}
			}
			return $val;
		}
	}
}