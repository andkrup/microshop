<?php
namespace supervillainhq\lexcorp\microshop{
	use Phalcon\DiInterface;
	use Phalcon\Session\Bag;
	use Phalcon\Http\Request;
	use supervillainhq\lexcorp\microshop\payment\Transaction;
	use supervillainhq\lexcorp\microshop\http\formvalidation\ContactInformationValidation;
	use supervillainhq\lexcorp\microshop\http\formvalidation\ValidationException;
	use supervillainhq\lexcorp\microshop\http\formvalidation\AddressValidation;
	use supervillainhq\core\contacts\PhoneNumber;
	use supervillainhq\core\contacts\PostalCode;
	use supervillainhq\core\intl\Country;
	use supervillainhq\core\mail\Email;
	use supervillainhq\spectre\auth\AuthUser;
	use supervillainhq\spectre\contacts\Address;

	class CheckoutSession{
		private $session;
		private $di;
		private $user;

		function __construct(DiInterface $di, AuthUser $user = null){
			$this->session = new Bag('checkoutsession');
			$this->session->setDI($di);

			$this->di = $di;
			$this->user = $user;

			// create default objects in case if none exists
			$entity = $this->session->get('entity');
			if(is_null($entity)){
				$entity = new Entity();
				$invoiceaddress = new Address();
				$entity->addAddress('invoice', $invoiceaddress);
				$deliveryaddress = new Address();
				$entity->addAddress('delivery', $deliveryaddress);
			}
			$this->session->set('entity', $entity);

			$service = $this->session->get('deliveryservice');
			if(is_null($service)){
				$service = $this->di->getShop()->getDeliveryService('pickup');
			}
			$this->session->set('deliveryservice', $service);

			$invoice = $this->session->get('invoice');
			if(is_null($invoice)){
				$invoice = new Invoice();
			}
			$this->session->set('invoice', $invoice);
		}

		function cart(){
			return $this->di->getCart();
		}

			/**
		 * Get an Entity instance with the current users information. This sets or creates the
		 * entity in this current session
		 *
		 * @param Cart $cart The current cart in session.
		 * @param AuthUser $user (optional) The currently authorized user in session.
		 * @return \supervillainhq\lexcorp\microshop\Entity
		 */
		function entity(){
			return $this->session->get('entity');
		}

		function deliveryService(){
			return $this->session->get('deliveryservice');
		}

		function invoice(){
			return $this->session->get('invoice');
		}

		function transaction(){
			if($this->isAuthorized()){
				$transactionId = intval($this->session->get('transactionId'));
				$transaction = $this->di->getObjectmapper('transaction', ['gateway_id' => $transactionId])->find();
				return $transaction;
			}
			return null;
		}

		function errorCode(){
			return intval($this->session->get('errorCode'));
		}

		function destroy(){
			if($this->isAuthorized()){
				$this->session->destroy();
				return true;
			}
			return false;
		}

		/**
		 * Extract contact info from posted parameters.
		 *
		 * @param RequestInterface $request
		 */
		function updateContactInformation(Request $request){
			// validate input
			self::validateContactInformation($request);
			$context = 'contact';
			$formdata = (object) $request->get($context);
			$entity = $this->session->get('entity');
			if(property_exists($formdata, 'email')){
				$email = $formdata->email;
				$entity->addEmail($context, new Email($email)); // overwrites at key if already present
			}
			if(property_exists($formdata, 'phone')){
				$phone = $formdata->phone;
				$entity->addPhone($context, new PhoneNumber($phone)); // overwrites at key if already present
			}
			$this->session->set('entity', $entity);
		}
		private static function validateContactInformation(Request $request){
			$validation = new ContactInformationValidation();
			$messages = $validation->validate($request->get('contact'));
			if($messages->count()>0){
				throw new ValidationException($messages, 'failed to validate contact information');
			}
		}

		/**
		 * Extract invoice address info from posted parameters.
		 *
		 * @param RequestInterface $request
		 */
		function updateInvoiceAddress(Request $request){
			$context = 'invoice';
			// validate input
			self::validateAddress($request, $context);
			$formdata = (object) $request->get($context);
			$this->updateAddress($formdata, $context);
		}
		private static function validateAddress(Request $request, $addressContext){
			$validation = new AddressValidation();
			$messages = $validation->validate($request->get($addressContext));
			if($messages->count()>0){
				throw new ValidationException($messages, 'failed to validate contact information');
			}
		}

		/**
		 * Extract delivery address info from posted parameters.
		 *
		 * @param RequestInterface $request
		 */
		function updateDeliveryAddress(Request $request){
			$context = $request->has('alternate_delivery_address') ? 'delivery' : 'invoice';
			$formdata = (object) $request->get($context);
			$this->updateAddress($formdata, $context);
		}

		private function updateAddress($formdata, $context){
			$address = new Address();
			if(property_exists($formdata, 'fullname')){
				$address->fullname($formdata->fullname);
			}
			if(property_exists($formdata, 'att')){
				$address->att($formdata->att);
			}
			if(property_exists($formdata, 'address1')){
				$address->address1($formdata->address1);
			}
			if(property_exists($formdata, 'address2')){
				$address->address2($formdata->address2);
			}
			if(property_exists($formdata, 'postalcode')){
				$address->postalcode(new PostalCode($formdata->postalcode));
			}
			if(property_exists($formdata, 'city')){
				$address->city($formdata->city);
			}
			if(property_exists($formdata, 'country')){
				$address->country(Country::get($formdata->country));
			}
			$entity = $this->session->get('entity');
			$entity->addAddress($context, $address);
			$this->session->set('entity', $entity);
		}

		function updateDeliveryService(Request $request){
			$selectedDelivery = $request->get('delivery');
			if($this->di->getShop()->hasDeliveryServiceAtKey($selectedDelivery)){
				$service = $this->di->getShop()->getDeliveryService($selectedDelivery);
				$this->session->set('deliveryservice', $service);
			}
		}

		function updateLegal(Request $request){
			$termsAgreed = $request->has('terms');
			$invoice = $this->session->get('invoice');
			$invoice->addAccepted('terms', $termsAgreed);
			$this->session->set('invoice', $invoice);
		}

		function finalize(Transaction $transaction){
			// Prep invoice so it is ready for a payment transaction. This means that from this time on, the invoice cannot be changed
			$data = Invoice::export($this->invoice(), $this->cart(), $this->entity(), $this->deliveryService(), $transaction);
			$invoiceId = $this->di->getObjectwriter('invoice', (array) $data)->create();

			$transaction->invoiceId($invoiceId);
		}

		function authorized($transactionId){
			$this->session->set('transactionId', intval($transactionId));
		}
		function cancelled(Transaction $transaction, $errorCode){
// 			var_dump($transaction);exit;
			$this->session->set('errorCode', $errorCode);
		}

		function isAuthorized(){
			$transactionId = intval($this->session->get('transactionId'));
			return !is_nan($transactionId);
		}
		function isCancelled(){
			$errorCode = intval($this->session->get('errorCode'));
			return !is_nan($errorCode);
		}
	}
}