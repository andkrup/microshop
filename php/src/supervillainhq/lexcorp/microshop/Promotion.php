<?php
namespace supervillainhq\lexcorp\microshop{
	use supervillainhq\core\db\DataAware;

	class Promotion{
		use DataAware;

		protected $products;
		protected $rebatePercentage;
		protected $title;
		protected $start;
		protected $end;
		protected $created;

		function title($title = null){
			if(is_null($title)){
				return $this->title;
			}
			$this->title = $title;
		}
		function start(\DateTime $date = null){
			if(is_null($date)){
				return $this->start;
			}
			$this->start = $date;
		}
		function end(\DateTime $date = null){
			if(is_null($date)){
				return $this->end;
			}
			$this->end = $date;
		}
		function created(\DateTime $date = null){
			if(is_null($date)){
				return $this->created;
			}
			$this->created = $date;
		}
		function rebate($percentage = null){
			if(is_null($percentage)){
				return $this->rebatePercentage;
			}
			$this->rebatePercentage = $percentage;
		}

		function resetProducts(array $products = []){
			$this->products = $products;
		}
		function addProduct($key, Product $product){
			$this->products[$key] = $product;
		}
		function removeProduct(Product $product){
			$c = count($this->products);
			for($i = 0; $i < $c; $i++){
				if($this->products[$i] == $product){
					array_splice($this->products, $i, 0);
				}
			}
		}
		function removeProductAt($key){
			unset($this->products[$key]);
		}
		function hasProduct(Product $product){
			return in_array($this->products, $product);
		}
		function hasProductAtKey($key){
			return array_key_exists($key, $this->products);
		}
		function getProduct($key){
			return $this->products[$key];
		}
		function products(){
			return $this->products;
		}

	}
}