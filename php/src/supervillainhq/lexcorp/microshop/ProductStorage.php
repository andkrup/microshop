<?php
namespace supervillainhq\lexcorp\microshop{
	interface ProductStorage{
		function name($name = null);
		function reload();
		function shipments();
		function currentTotal();
	}
}