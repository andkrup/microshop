<?php
namespace supervillainhq\lexcorp\microshop{
	interface CartItem extends \Serializable{
		function active();
		function amount($amount = 0);
		function product(Product $product = null);
		function equals(CartItem $item);
	}
}