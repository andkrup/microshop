<?php
namespace supervillainhq\lexcorp\microshop{
	interface Inventory{
		function update();
		function total();

		function resetStorages(array $storages = []);
		function addStorage(ProductStorage $storage);
		function removeStorage(ProductStorage $storage);
		function hasStorage(ProductStorage $storage);
		function getStorage($index);
		function storages();
	}
}