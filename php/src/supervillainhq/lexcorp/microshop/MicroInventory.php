<?php
namespace supervillainhq\lexcorp\microshop{
	class MicroInventory implements Inventory{
		private $storages;

		function __construct(){
			$this->resetStorages();
		}

		function update(){}

		function total(){
			$sum = 0;
			foreach ($this->storages as $storage){
				$sum += intval($storage->currentTotal());
			}
			return $sum;
		}

		function resetStorages(array $storages = []){
			$this->storages = $storages;
		}
		function addStorage(ProductStorage $storage){
			array_push($this->storages, $storage);
		}
		function removeStorage(ProductStorage $storage){
			$c = count($this->storages);
			for($i = 0; $i < $c; $i++){
				if($this->storages[$i] == $storage){
					array_splice($this->storages, $i, 0);
				}
			}
		}
		function hasStorage(ProductStorage $storage){
			return in_array($this->storages, $storage);
		}
		function getStorage($index){
			return $this->storages[$index];
		}
		function storages(){
			return $this->storages;
		}
	}
}