<?php
namespace supervillainhq\lexcorp\microshop{
	class MicroOrder implements Order{
		const STATUS_OPEN = 'open'; // when user has placed the order
		const STATUS_PACKAGED = 'packaged'; // order has been assembled and ready for shipping
		const STATUS_SHIPPED = 'shipped'; // order has been shipped
		const STATUS_VOID = 'void'; // order has been nullified
		const STATUS_DISCARDED = 'discarded'; // order has been deleted
		const STATUS_COMPLETED = 'completed'; // order has been completed and invoice created

		private $status;

		function __construct(){
			$this->status = self::STATUS_OPEN;
		}

		function status(){
			return $this->status;
		}
	}
}