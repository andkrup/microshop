<?php
namespace supervillainhq\lexcorp\microshop{
	use supervillainhq\core\db\DataAware;
	use supervillainhq\spectre\contacts\Address;
	use supervillainhq\spectre\contacts\Communicative;
	use supervillainhq\spectre\contacts\ContactInfo;

	class Entity implements \Serializable{
		use DataAware;
		use Communicative;

		const INVOICE = 'invoice';
		const DELIVERY = 'delivery';

		private $cart;
		private $addresses;


		function cart(Cart $cart = null){
			if(is_null($cart)){
				return $this->cart;
			}
			$this->cart = $cart;
		}

		function __construct(){
			$this->resetAddresses();
			$this->resetEmails();
		}


		function resetAddresses(array $addresses = []){
			$this->addresses = $addresses;
		}
		function addAddress($key, Address $address){
			if(!is_array($this->addresses)){
				$this->resetAddresses();
			}
			$this->addresses[$key] = $address;
		}
		function removeAddress(Address $address){
			$c = count($this->addresses);
			for($i = 0; $i < $c; $i++){
				if($this->addresses[$i] == $address){
					array_splice($this->addresses, $i, 0);
				}
			}
		}
		function removeAddressAt($key){
			unset($this->addresses[$key]);
		}
		function hasAddress(Address $address){
			return in_array($this->addresses, $address);
		}
		function hasAddressAtKey($key){
			if(!is_array($this->addresses)){
				$this->resetAddresses();
				return false;
			}
			return array_key_exists($key, $this->addresses);
		}
		function getAddress($key){
			return $this->addresses[$key];
		}
		function addresses(){
			return $this->addresses;
		}


		function hasAlternateDeliveryAddress(){
			if($this->hasAddressAtKey(self::INVOICE) && $this->hasAddressAtKey(self::DELIVERY)){
				return !$this->getAddress(self::INVOICE)->equals($this->getAddress(self::DELIVERY)) && !$this->getAddress(self::DELIVERY)->blank();
			}
			return false;
		}


		function contactInfo(){
			if(!is_array($this->emails)){
				$this->emails = [];
			}
			if(!is_array($this->phones)){
				$this->phones = [];
			}
			return new ContactInfo($this->emails, $this->phones);
		}


		function serialize(){
			$addresses = [];
			if(!empty($this->addresses)){
				foreach ($this->addresses as $key => $address){
					$addresses[$key] = $address;
				}
			}
			$emails = [];
			if(!empty($this->emails)){
				foreach ($this->emails as $key => $email){
					$emails[$key] = $email;
				}
			}
			$object = (object) [
					'addresses' => $addresses,
					'emails' => $emails,
			];
			return serialize($object);
		}

		function unserialize ($serialized) {
			$data = unserialize($serialized);
			foreach ($data->addresses as $key => $address){
				$this->addAddress($key, $address);
			}
			foreach ($data->emails as $key => $email){
				$this->addEmail($key, $email);
			}
		}

		static function hydrate($serialized){
			return unserialize($serialized);
		}
	}
}