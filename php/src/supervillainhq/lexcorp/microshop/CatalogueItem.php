<?php
namespace supervillainhq\lexcorp\microshop{
	use supervillainhq\spectre\cms\assets\FileAsset;

	interface CatalogueItem{
		function product(Product $product = null);

		function resetAssets(array $assets = []);
		function addAsset(FileAsset $asset);
		function removeAsset(FileAsset $asset);
		function hasAsset(FileAsset $asset);
		function getAsset($index);
		function assets();
	}
}