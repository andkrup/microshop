<?php
namespace supervillainhq\lexcorp\microshop{
	use Phalcon\DiInterface;
	use Phalcon\Config;
	use supervillainhq\lexcorp\microshop\front\MicroFront;
	use supervillainhq\lexcorp\microshop\inventories\LocalMySqlProductStorage;
	use supervillainhq\lexcorp\microshop\commands\CreateShipmentCommand;
	use supervillainhq\lexcorp\microshop\payment\Transaction;
	use supervillainhq\spectre\contacts\Address;
	use supervillainhq\spectre\auth\AuthUser;

	final class MicroShop{
		private $di;
		private $enabled;
		private $shopName;
		private $shopCvr;
		private $shopAddresses;
		private $deliveryServices;
		private $inventory;
		private $catalog;
		private $checkoutSession;
		private $session;


		function enabled(){
			return $this->enabled;
		}
		function inventoryEnabled(){
			return !is_null($this->inventory());
		}
		function name($name = null){
			if(is_null($name)){
				return $this->shopName;
			}
			$this->shopName = $name;
		}
		function cvr($cvr = null){
			if(is_null($cvr)){
				return $this->shopCvr;
			}
			$this->shopCvr = $cvr;
		}

		function inventory(Inventory $inventory = null){
			if(is_null($inventory)){
				return $this->inventory;
			}
			$this->inventory = $inventory;
		}
		function catalog(Catalog $catalog = null){
			if(is_null($catalog)){
				return $this->catalog;
			}
			$this->catalog = $catalog;
		}

		function gateway(){
			return $this->di->getPaymentGateway();
		}


		function resetAddresses(array $addresses = []){
			$this->shopAddresses = $addresses;
		}
		function addAddress($key, Address $address){
			$this->shopAddresses[$key] = $address;
		}
		function removeAddress(Address $address){
			$c = count($this->shopAddresses);
			for($i = 0; $i < $c; $i++){
				if($this->shopAddresses[$i] == $address){
					array_splice($this->shopAddresses, $i, 0);
				}
			}
		}
		function removeAddressAt($key){
			unset($this->shopAddresses[$key]);
		}
		function hasAddress(Address $address){
			return in_array($this->shopAddresses, $address);
		}
		function hasAddressAtKey($key){
			return array_key_exists($key, $this->shopAddresses);
		}
		function getAddress($key){
			return $this->shopAddresses[$key];
		}
		function addresses(){
			return $this->shopAddresses;
		}

		function resetDeliveryServices(array $deliveryServices = []){
			$this->deliveryServices = $deliveryServices;
		}
		function addDeliveryService($key, DeliveryService $deliveryService){
			$this->deliveryServices[$key] = $deliveryService;
		}
		function removeDeliveryService(DeliveryService $deliveryService){
			$c = count($this->deliveryServices);
			for($i = 0; $i < $c; $i++){
				if($this->deliveryServices[$i] == $deliveryService){
					array_splice($this->deliveryServices, $i, 0);
				}
			}
		}
		function removeDeliveryServiceAt($key){
			unset($this->deliveryServices[$key]);
		}
		function hasDeliveryService(DeliveryService $deliveryService){
			return in_array($this->deliveryServices, $deliveryService);
		}
		function hasDeliveryServiceAtKey($key){
			return array_key_exists($key, $this->deliveryServices);
		}
		function getDeliveryService($key){
			return $this->deliveryServices[$key];
		}
		function deliveryServices(){
			return $this->deliveryServices;
		}



		function __construct(DiInterface $di, Config $config){
			$this->resetAddresses();
			$this->resetDeliveryServices();
			$this->di = $di;

			if(is_null($config->microshop)){
				throw new \Exception("Missing configuration. The configuration file 'microshop.json' seems to be missing or the json content is invalid.");
			}
			$this->enabled = $config->microshop->{'general-info'}->enabled;
			$this->shopName = $config->microshop->{'general-info'}->name;
			$this->shopCvr = $config->microshop->{'general-info'}->cvr;
			$addressesData = $config->microshop->{'general-info'}->addresses;
			foreach ($addressesData as $key => $addressData){
				$mapper = $this->di->getObjectmapper('address', (array) $addressData);
				$this->addAddress($key, Address::inflate($mapper));
			}

			$deliveryOptions = $config->microshop->{'delivery-options'};
			foreach ($deliveryOptions as $key => $deliveryOption){
				$this->addDeliveryService($key, DeliveryService::fromObject($deliveryOption));
			}

			$this->catalog(MicroCatalog::load($config, $di));

			if($config->microshop->{'general-info'}->inventory){
				$this->inventory(new MicroInventory());
				// load all storage providers as defined in config
				foreach ($config->microshop->inventories as $productInventory){
					$storage = null;
					$provider = $productInventory->dataprovider;
					switch($provider){
						case 'mysql':
							$storage = new LocalMySqlProductStorage($di);
							$storage->name($productInventory->name);
							break;
					}
					if(!is_null($storage)){
						$this->inventory()->addStorage($storage);
					}
				}
			}
		}

		function executeCommand($command){
			$result = false;
			switch ($command){
				case 'inventory/createshipment':
					$shipment = func_get_arg(1);
					$command = new CreateShipmentCommand($shipment);
					break;
			}
			if(isset($command)){
				try{
					$result = $command->execute();
				}
				catch (\Exception $exception){
					return false;
				}
			}
			return $result;
		}

		function listInventoryNames(){
			$storages = $this->inventory()->storages();
			$names = [];
			foreach ($storages as $storage){
				array_push($names, $storage->name());
			}
			return implode(', ', $names);
		}

		/**
		 * Return a CheckoutSession object that can
		 *
		 * @param AuthUser $user
		 * @return \supervillainhq\lexcorp\microshop\CheckoutSession
		 */
		function getCheckoutSession(AuthUser $user = null){
			if(!isset($this->checkoutSession)){
				$this->checkoutSession = new CheckoutSession($this->di, $user);
			}
			return $this->checkoutSession;
		}

		function initTransaction($amount){
			$transaction = Transaction::initiate($this->di->getConfig(), $amount);
			return $transaction;
		}


		function validateCurrentTransaction($invoiceId, $gatewayTransactionID){
			var_dump($invoiceId);
			var_dump($gatewayTransactionID);
// 			$transaction = $this->session->get('transaction');
//			var_dump($transaction);
// 			$invoice = $this->di->getObjectmapper('transaction', ['invoice_id' => $invoiceId])->get();
			exit;
// 			var_dump($this->session);
			exit;
			if(!is_null($transaction)){
				$session = $this->getCheckoutSession();
				$transaction->gatewayTransactionID($gatewayTransactionID);

				$session->finalize($transaction);
			}
		}

		function authorizeTransaction($invoiceId, $gatewayTransactionId){
			$transaction = $this->di->getObjectmapper('transaction', ['invoice_id' => $invoiceId])->find();
			$transaction->gatewayTransactionID($gatewayTransactionId);
			Transaction::authorizeTransaction($transaction);
			// serialise transaction in session
			$session = $this->getCheckoutSession();
			$session->authorized($gatewayTransactionId);
			return $transaction;
		}

		function cancelTransaction($invoiceId, $errorCode = 0){
			$transaction = $this->di->getObjectmapper('transaction', ['invoice_id' => $invoiceId])->find();
// 			if(!$transaction->writable()){
// 				throw new \Exception('Transaction is finalised');
// 			}
			Transaction::rejectTransaction($transaction, $errorCode);
			// cancel transaction in session
			$session = $this->getCheckoutSession();
			$session->cancelled($transaction, $errorCode);
			return $transaction;
		}


		function getInvoiceForPrintout($invoiceId){
			$transaction = $this->di->getObjectmapper('transaction', ['invoice_id' => $invoiceId])->find();
		}


		function getPaymentWindowURL(Transaction $transaction){
			$config = $this->di->getConfig();
			$options = [
					'currency' => $transaction->currency()->code(),
					'orderid' => $transaction->invoiceId(),
					'callbackurl' => $config->payment->gateway->callback_url,
					'accepturl' => $config->payment->gateway->accept_url,
					'cancelurl' => $config->payment->gateway->cancel_url,
					'instantcallback' => 1,
			];
			return $transaction->gateway()->paymentWindowURL($transaction->amount(), (object) $options);
		}

		function getInvoiceURI(Invoice $invoice, $baseUrl = null){
			if(is_null($baseUrl)){
				$baseUrl = $_SERVER['SERVER_NAME'];
			}
			$hash = $invoice->hash();
			return "http://{$baseUrl}/shop/pdf/{$hash}";
		}

		function mailInvoiceToUser(Invoice $invoice, $pdfUrl, $template = null){
		}

		/**
		 * Get an Entity instance with the current users information. This sets or creates the
		 * entity in this current session
		 *
		 * @return \supervillainhq\lexcorp\microshop\Entity
		 */
		function currentVisitor(){
			return $this->getCheckoutSession()->entity();
		}


		static function initiateServices(DiInterface $di, Config $config){
			/**
			 * Access the cart API
			 */
			$di->set('cart', function () use ($di) {
				return new MicroCart($di);
			});

			/**
			 * Access the shop API
			 */
			$di->set('shop', function () use ($di, $config) {
				return new MicroShop($di, $config);
			});

			/**
			 * Access the shop gui/skin/template
			 */
			$di->set('shopfront', function () use ($di, $config) {
				return new MicroFront($di, $config);
			});

			/**
			 * Set up the interface for the payment  gateway
			 */
			$di->set('paymentGateway', function () use ($di, $config) {
				$gateway = new Gateway($config);
				return $gateway;
			});

			// inject the classpaths that we need
		}
	}
}