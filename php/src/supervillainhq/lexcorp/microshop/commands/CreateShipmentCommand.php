<?php
namespace supervillainhq\lexcorp\microshop\commands{
	use supervillainhq\core\Command;

	class CreateShipmentCommand implements Command{
		private $shipmentProduct;
		private $shipmentAmount;

		function __construct(array $data){
			$data = (object) $data;
			$this->shipmentProduct = $data->product_id;
			$this->shipmentAmount = intval($data->amount);
		}

		function execute(){
// 			var_dump($this->shipmentProduct);
// 			var_dump($this->shipmentAmount);
// 			exit;
			return true;
		}
	}
}