<?php
namespace supervillainhq\lexcorp\microshop{
	use supervillainhq\lexcorp\microshop\browsing\Navigator;

	interface Catalog{
		function navigationType();
		function navigator(Navigator $navigator = null);
		function resetProducts(array $products = []);
		function addProduct(Product $product);
		function removeProduct(Product $product);
		function hasProduct(Product $product);
		function getProduct($index);
		function products();
	}
}