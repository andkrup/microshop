<?php
namespace supervillainhq\lexcorp\microshop\payment\epay{
	class TransactionInformationType{
		public $group; // s:string"/>
		public $authamount; // s:int"/>
		public $currency; // s:int"/>
		public $cardtypeid; // s:int"/>
		public $capturedamount; // s:int"/>
		public $creditedamount; // s:int"/>
		public $orderid; // s:string"/>
		public $description; // s:string"/>
		public $authdate; // s:dateTime"/>
		public $captureddate; // s:dateTime"/>
		public $deleteddate; // s:dateTime"/>
		public $crediteddate; // s:dateTime"/>
		public $status; // tns:TransactionStatus"/>
		public $history; // tns:ArrayOfTransactionHistoryInfo"/>
		public $transactionid; // s:long"/>
		public $cardholder; // s:string"/>
		public $mode; // tns:PayMode"/>
		public $msc; // s:boolean"/>
		public $fraudStatus; // s:int"/>
		public $FraudMessage; // s:string"/>
		public $payerCountryCode; // s:string"/>
		public $issuedCountryCode; // s:string"/>
		public $fee; // s:int"/>
		public $splitpayment; // s:boolean"/>
		public $acquirer; // tns:AcquirerType"/>
		public $tcardno; // s:string"/>
		public $expmonth; // s:int"/>
		public $expyear; // s:int"/>
	}
}