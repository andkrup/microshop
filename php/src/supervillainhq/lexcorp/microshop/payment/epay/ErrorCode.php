<?php
namespace supervillainhq\lexcorp\microshop\payment\epay{
	class ErrorCode{
		private $code;
		private $message;

		function code(){
			return $this->code;
		}
		function message(){
			return $this->message;
		}

		private function __construct($code){
			$this->code = $code;
			$this->message = self::lookupMessage($code);
		}

		function __toString(){
			return "{$this->message}";
		}

		static function translate($integer){
			return new ErrorCode($integer);
		}

		private static function lookupMessage($code){
			switch ($code){
				case -10006: return 'Afbrudt';
				case -5604: return 'Gebyret kan ikke udregnes for den anvendte korttype.';
				case -5603: return 'Butikken tillader ikke den anvendte korttype/betalingskort';
				case -5602: return 'Der er anvendt en ugyldig valutakode.';
				case -5601: return 'Gebyret kan ikke udregnes for den anvendte korttype.';
				case -5600: return 'Kortnummeret er ikke angivet korrekt - ugyldigt prefix (skal være på 6 tegn).';
				case -5516: return 'Betalingsvinduet er lukket af brugeren';
				case -5514: return 'Kundens session er enten udløbet, eller også er betalingen ikke startet korrekt.';
				case -5511: return 'Der er opstået en fejl! Start forfra med din betaling.';
				case -5509: return 'Du får fejlen Not valid data, når du prøver at åbne Standard Betalingsvinduet. Du får denne fejl, fordi ePay ikke kan finde dataene til transaktionen. Denne fejl opstår, fordi brugeren/kunden har været inaktivt i over 20 minutter!';
				case -5508: return 'Du får fejlen No valid domains i created for the company, når du prøver at åbne Standard Betalingsvinduet. Du får denne fejl, fordi du ikke har oprettet et domænet til jeres konto i betalingssystemet. I din administration til betalingssystemet under Instillinger og Betalingssystemet, kan du se det/de domæne(r), som er tilknyttet jeres konto.';
				case -5507: return 'Du får fejlen URL not allowed for relaying, når du prøver at åbne Standard Betalingsvinduet. Du får denne fejl, fordi det domæne som du åbner betalingsvinduet fra, ikke er oprettet i betalingssystemet. I din administration til betalingssystemet under Instillinger og Betalingssystemet, kan du se det/de domæne(r), som er tilknyttet jeres konto.';
				case -5506: return 'Du får fejlen Invalid merchantnumber, når du prøver at åbne Standard Betalingsvinduet. Du får denne fejl, fordi det indløsningsnummer / merchantnumber du anvender ikke er oprettet i Betalingssystemet! Tjek derfor om du anvender det rigtige merchantnumber. I din administration til betalingssystemet under Instillinger og Betalingssystemet, kan du se de merchants, som er tilknyttet jeres konto.';
				case -5505: return 'Du får fejlen No cardtypes defined, når du prøver at åbne Standard Betalingsvinduet. Det er fordi, der ikke er sat nogle betalingskort op på jeres konto i betalingssystemet.';
				case -5504: return 'Du får fejlen Invalid currencycode, når du prøver at åbne Standard Betalingsvinduet. Du får denne fejl, fordi du anvender en ugyldig currencycode. Du kan fra din administration til betalingssystemet under Support og Valutakoder se listen over valutakoder, der kan anvendes.';
				case -5503: return 'De data som du sender til betalingsvinduet er ikke korrekte! Du får en beskrivelse af hvilke data, som det er, der ikke er angivet korrekte.';
				case -5502: return 'Du får fejlen Invalid company, når du prøver at åbne Standard Betalingsvinduet. Du får denne fejl, fordi du ikke har aktiveret betalingsvinduet endnu. Du skal aktivere betalingsvinduet fra din administration til betalingssystemet under Instillinger og Betalingsvinduet.';
				case -5501: return 'Du får fejlen Window not activated, når du prøver at åbne Standard Betalingsvinduet. Du får denne fejl, fordi du ikke har aktiveret betalingsvinduet endnu. Du skal aktivere betalingsvinduet fra din administration til betalingssystemet under Instillinger og Betalingsvinduet.';
				case -2003: return 'Afvist - Kortudstederens land / region matcher ikke med det land betalingen kommer fra.';
				case -2002: return 'Afvist - Ikke sikre betalinger fra land / region accepteres ikke.';
				case -2001: return 'Afvist - Betalinger fra land / region accepteres ikke.';
				case -2000: return 'Afvist - Betalinger fra din IP Addresse accepteres ikke.';
				case -1602: return 'PBS test-gateway er desværre nede i øjeblikket, prøv igen senere.';
				case -1601: return 'Svar sendt til betalingssystemet var forventet at være fra en netbank, men er invalid. Invalide data var posted.';
				case -1600: return 'Session fra netbank er allerede brugt. Samme session kan ikke benyttes igen.';
				case -1312: return 'Afvist - betalingen kunne ikke hæves - prøv igen eller konktakt PayPal';
				case -1303: return 'Afvist - Betalingen kunne ikke hæves - afvist af EWIRE.';
				case -1302: return 'Afvist - Fejl i ewire MD5 data. Kontroller at MD5 data er opsat i både EWIRE og ePay.';
				case -1301: return 'Afvist - EWIRE forretningsnummer var ikke fundet - Kontroller at EWIRE forretningsnummeret er opsat i ePay.';
				case -1300: return 'Du forsøger at betale med en betalingsform, som forretningen ikke accepterer. Prøv en anden betalingsform eller kontakt forretningen.';
				case -1200: return 'Ukendt valutakode. Du kan kun benytte de valutakoder, som du kan se under menupunktet Support og Valutakoder i administrationen.';
				case -1100: return 'Invalid data modtaget hos betalingssystemet. Du skal huske at sende beløbet i mindste enhed / minor units (DKK skal fx. angives i øre), og må ikke bruge komma eller punktum (separatortegn). Derudover skal du være opmærksom på at fremsende felterne cardno, expmonth og expyear, der er nødvendige for at kunne gennemføre en korttransaktion.';
				case -1024: return 'Ugyldigt kortnummer indtastet. Venligst ret de intastede oplysninger og prøv igen.';
				case -1023: return 'Transaktionen er allerede hævet';
				case -1022: return 'Transaktionen er afvist, da forretningen har valgt at blokere alle transaktioner, der matcher kortnummeret.';
				case -1021: return 'Der kan på en transaktion max udføres en operation hvert 15 minut. Vent 15 minutter og prøv igen.';
				case -1020: return 'Transaktionen er slettet';
				case -1019: return 'Forkert kodeord benyttet til webservice adgang!';
				case -1018: return 'Forkert test-kort benyttet! Du finder de gyldige testoplysninger under menuen Support - Test Oplysninger, når du har logget ind i administrationen til betalingssystemet.';
				case -1017: return 'Ingen adgang til PCI nødvendig funktion!';
				case -1016: return 'Der er driftsforstyrelser hos indløseren. Dette er en offline procedure. Vent venligst et kort øjeblik, og prøv igen.';
				case -1015: return 'Valuta kode var ikke fundet. Du skal her undersøg hvilke valuta koder du kan gennemføre betalinger med.';
				case -1014: return 'Transaktionen kunne ikke gennnemføres som 3D Secure. Transaktionen afvises derfor. Prøv et andet betalingskort eller kontakt forretningen.';
				case -1012: return 'Afvist - Kan ikke forny denne korttype.';
				case -1011: return 'MD5 stempling var ikke valid.';
				case -1010: return 'Korttype var ikke fundet i den angivne liste af forud definerede korttyper (feltet cardtype). Hvis du ønsker at kunne tage imod denne type kort skal du tilføje korttypen til denne liste.';
				case -1009: return 'Abonnementsbetaling var ikke fundet.';
				case -1008: return 'Transaktion kunne ikke findes.';
				case -1007: return 'Der er afvigelser i beløb hævet / til rådighed. Undersøg beløbet der hæves / krediteres mod det beløb der er autoriseret / hævet. Bemærk hvis der er tale om en Euroline transaktion, og transaktionen er hævet, kan den først krediteres den efterfølgende dag.';
				case -1006: return 'Produkt ikke tilgængelig.';
				case -1005: return 'Driftsforstyrrelser - prøv igen senere.';
				case -1004: return 'Fejlkoden blev ikke fundet.';
				case -1003: return 'Ikke åbent for ipadresse for remote interface.';
				case -1002: return 'Forretningsnummer ikke oprettet i betalingssystemet.';
				case -1001: return 'Ordrenummeret eksisterer allerede.';
				case -1000: return 'Kommunikations forstyrrelser til PBS.';
				case -23: return 'PBS test gateway unavailable.';
				case -4: return 'Kommunikations forstyrrelser til PBS.';
				case -3: return 'Kommunikations forstyrrelser til PBS.';
				case 4000: return 'eDankort / PBS 3D secure / Banker - betaling afbrudt af bruger';
				case 4001: return 'SOLO - brugeren afbrød betalingen';
				case 4002: return 'SOLO - brugeren blev afvist';
				case 4003: return 'SOLO - fejl i MAC (MD5)';
				case 4100: return 'Afvist - Intet svar';
				case 4101: return 'Afvist - Ring til kortudstederen';
				case 4102: return 'Afvist - Ring til kortudstederen og behold kortet (svindel)';
				case 4103: return 'Betalingen blev afvist. Det kan eventuelt skyldes du har tastet forkerte oplysninger. Prøv igen, og hvis fejlen bliver ved med at opstå skal du kontakte forretningen.';
				case 4104: return 'Afvist - Systemfejl - intet svar';
				case 4105: return 'Afvist - ukendt fejl';
				case 4106: return 'Afvist - Kort ikke godkendt af VISA / MasterCard / JCB (3D Secure). Du skal prøve igen.';
				case 4107: return 'Afvist - Kan ikke frigive Euroline (SEB) betalinger (understøttes ikke)';
				case 4108: return 'Afvist - Kan ikke forny Euroline (SEB) betalinger (understøttes ikke)';
				case 4109: return 'Afvist - Kort kunne ikke blive godkendt af 3D secure';
				case 4110: return 'Afvist - Der skete en fejl under godkendelse af 3D secure';
				case 4111: return 'Afvist - Kortet kunne ikke findes hos 3D secure';
				case 4121: return 'Transaktionen blev annulleret af kunde hos ViaBill';
				case 4122: return 'Transaktionen blev afvist af ViaBill';
				case 10004: return 'Betalingen gennem Danske Bank blev afbrudt.';
				case 10005: return 'Betalingen gennem Danske Bank blev afbrudt.';
			}
		}
	}
}