<?php
namespace supervillainhq\lexcorp\microshop\payment\epay\api{
	class CreditResponse extends EpayResponse{
		public $creditResult; // bool
		public $pbsResponse; // int

		function __construct($data = null){
			parent::__construct($data);
			$data = (object) $data;
			$this->creditResult = $data->creditResult;
			$this->pbsResponse = $data->pbsResponse;
		}
	}
}