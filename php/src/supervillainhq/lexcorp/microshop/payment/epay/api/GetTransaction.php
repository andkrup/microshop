<?php
namespace supervillainhq\lexcorp\microshop\payment\epay\api{
	use supervillainhq\lexcorp\microshop\payment\Card;

	class GetTransaction extends EpayRequest{
		public $merchantnumber; // int
		public $cardnumber; // string
		public $cardtypeid = ''; // int
		public $cardname = ''; // string
		public $pwd; // string
// 		public $invoice; // string
// 		public $pbsResponse = ''; // int
		public $epayresponse = ''; // int

		function __construct(Card $card){
			parent::__construct('/remote/payment/gettransaction');
			$this->cardnumber = $card->number();
// 			$this->cardtypeid = $card->type()->id();
// 			$this->cardname = $card->name();
// 			$this->cardtypeid = '';
// 			$this->cardname = '';
		}
	}
}