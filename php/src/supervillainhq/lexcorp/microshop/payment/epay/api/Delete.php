<?php
namespace supervillainhq\lexcorp\microshop\payment\epay\api{
	use supervillainhq\lexcorp\microshop\payment\Transaction;

	class Delete extends EpayRequest{
		public $merchantnumber; // int
		public $transactionid; // long
		public $group = ''; // string
		public $pwd; // string
// 		public $invoice; // string
		public $pbsResponse = ''; // int
		public $epayresponse = ''; // int

		function __construct(Transaction $transaction){
			parent::__construct('/remote/payment/delete');
			$this->transactionid = $transaction->gatewayTransactionID();
		}
	}
}