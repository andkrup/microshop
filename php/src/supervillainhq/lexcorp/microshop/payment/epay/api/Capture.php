<?php
namespace supervillainhq\lexcorp\microshop\payment\epay\api{
	use supervillainhq\lexcorp\microshop\payment\Transaction;

	class Capture extends EpayRequest{

		public $merchantnumber; // int
		public $transactionid; // long
		public $amount; // int
// 		public $group; // string
		public $pwd; // string
// 		public $invoice; // string
		public $pbsResponse = ''; // int
		public $epayresponse = ''; // int

		function __construct(Transaction $transaction){
			parent::__construct('/remote/payment/capture');
			$this->transactionid = $transaction->gatewayTransactionID();
// 			$this->transactionid = 60102447;
			$this->amount = $transaction->amount();
// 			$this->invoice = $transaction->invoiceId(); // kun til storebox, loyalitetsprogrammer, fakturabetaling, mm.
		}
	}
}