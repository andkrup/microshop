<?php
namespace supervillainhq\lexcorp\microshop\payment\epay\api{
	class DeleteResponse extends EpayResponse{
		public $deleteResult; // bool
		public $cardtypeid; // int
		public $cardname; // string

		function __construct($data = null){
			parent::__construct($data);
			$data = (object) $data;
			$this->deleteResult = $data->deleteResult;
			$this->cardtypeid = $data->cardtypeid;
			$this->cardname = $data->cardname;
		}
	}
}