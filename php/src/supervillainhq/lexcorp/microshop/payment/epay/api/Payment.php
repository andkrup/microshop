<?php
namespace supervillainhq\lexcorp\microshop\payment\epay\api{
	use Phalcon\Config;

	class Payment extends SoapService{
		private $config;
		private $apiKey;
		private $apiHost;

		function __construct(Config $config){
			$this->config = $config;
			$apiCfg = $this->config->payment->api;
			$this->apiKey = $apiCfg->{'api-key'};
			$this->apiHost = $apiCfg->host;
			$wsdl = $apiCfg->services->{'payment'}->wsdl;

			parent::__construct("{$this->apiHost}{$wsdl}");
		}

		function __call($name, array $arguments){
			$request = $arguments[0];
			$request->pwd = $this->apiKey;
			$request->merchantnumber = intval($this->config->payment->gateway->{'merchant-id'});

			$response = null;
			try{
				$response = call_user_func_array([$this->client, $name], [$request]);
			}
			catch (\SoapFault $fault){
				throw new EpayException($fault, $name, $request);
			}
			return $this->parseResponse($name, $response);
		}

		private function parseResponse($name, $raw = null){
			$type = $this->getReturnType($name);
			$classpath = "supervillainhq\\lexcorp\\microshop\\payment\\epay\\api\\{$type}";
			if(class_exists($classpath)){
				$reflector = new \ReflectionClass($classpath);
				$typedResponse = $reflector->newInstanceArgs([$raw]);
				return $typedResponse;
			}
			return $raw;
		}
	}
}