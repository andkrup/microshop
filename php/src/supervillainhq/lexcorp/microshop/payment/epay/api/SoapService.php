<?php
namespace supervillainhq\lexcorp\microshop\payment\epay\api{
	class SoapService{
		protected $client;
		protected $remoteMethods;
		protected $remoteTypes;
		private $wsdl;

		function __construct($wsdl){
			$this->wsdl = $wsdl;
			$this->client = new \SoapClient($this->wsdl);
			$this->remoteMethods = $this->client->__getFunctions();
			$this->remoteTypes = $this->client->__getTypes();
		}

		/**
		 * Investigate the wsdl for a specific function
		 *
		 * @param string $name
		 * @return boolean
		 */
		function hasFunction($name){
			foreach ($this->remoteMethods as $rMethod){
				if(stristr($rMethod, " {$name}(")){
					return true;
				}
			}
			return false;
		}

		/**
		 * Get the return type of a function defined in the wsdl
		 *
		 * @param string $name
		 * @return string|boolean
		 */
		function getReturnType($name){
			foreach ($this->remoteMethods as $rMethod){
				if(stristr($rMethod, " {$name}(")){
					$words = explode(' ', $rMethod);
					return ucfirst($words[0]);
				}
			}
			return false;
		}
	}
}