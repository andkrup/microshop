<?php
namespace supervillainhq\lexcorp\microshop\payment\epay\api{
	class CaptureResponse extends EpayResponse{
		public $captureResult;
		public $pbsResponse;

		function __construct($data = null){
			parent::__construct($data);
			$data = (object) $data;
			$this->captureResult = $data->captureResult;
			$this->pbsResponse = $data->pbsResponse;
		}
	}
}