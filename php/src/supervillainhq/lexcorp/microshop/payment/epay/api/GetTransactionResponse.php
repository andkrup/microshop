<?php
namespace supervillainhq\lexcorp\microshop\payment\epay\api{
	class GetTransactionResponse extends EpayResponse{
		public $gettransactionResult; // bool
		public $transactionInformation; // TransactionInformationType

		function __construct($data = null){
			parent::__construct($data);
			$data = (object) $data;
			$this->transactionInformation = $data->transactionInformation;
			$this->cardtypeid = $data->cardtypeid;
			$this->cardname = $data->cardname;
		}
	}
}