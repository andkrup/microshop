<?php
namespace supervillainhq\lexcorp\microshop\payment\epay\api{
	class EpayException extends \Exception{
		public $method;
		public $request;

		function __construct(\SoapFault $fault, $method, $request){
			$this->method = $method;
			$this->request = $request;
			parent::__construct($fault->getMessage(), $fault->getCode());
		}
	}
}