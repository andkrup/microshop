<?php
namespace supervillainhq\lexcorp\microshop\payment\epay\api{
	class DeleteResponse extends EpayResponse{
		public $deleteResult; // bool

		function __construct($data = null){
			parent::__construct($data);
			$data = (object) $data;
			$this->deleteResult = $data->deleteResult;
		}
	}
}