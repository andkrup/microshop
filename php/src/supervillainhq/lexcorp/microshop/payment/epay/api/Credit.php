<?php
namespace supervillainhq\lexcorp\microshop\payment\epay\api{
	use supervillainhq\lexcorp\microshop\payment\Transaction;

	class Credit extends EpayRequest{
		public $merchantnumber; // int
		public $transactionid; // long
		public $amount; // int
// 		public $group; // string
		public $pwd; // string
// 		public $invoice; // string
		public $pbsResponse = ''; // int
		public $epayresponse = ''; // int

		function __construct(Transaction $transaction){
			parent::__construct('/remote/payment/credit');
			$this->transactionid = $transaction->gatewayTransactionID();
			$this->amount = $transaction->amount();
		}
	}
}