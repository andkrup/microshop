<?php
namespace supervillainhq\lexcorp\microshop\payment\epay{
	use supervillainhq\lexcorp\microshop\payment\epay\api\Payment;
	use Phalcon\Config;

	class Epay{
		private $config;
		private $merchantId;
		private $orderId;
		private $hash;
		private $currency;
		private $windowState;

		function __construct(Config $config){
			$this->config = $config;
			$this->merchantId = $config->payment->gateway->{'merchant-id'};
			$this->hashKey = $this->config->payment->gateway->{'md5-key'};
			$this->currency = "DKK";
			$this->windowState = 3;
		}

		function orderID(){
			return $this->orderId;
		}

		private function md5Hash(array $parameters){
			return md5(implode("", array_values($parameters)) . $this->hashKey);
		}
		function md5Validate($md5, array $parameters){
			unset($parameters['_url']);
			unset($parameters['hash']);
			$hash = md5(implode("", array_values($parameters)) . $this->hashKey);
			return $hash === $md5;
		}

		function paymentWindow($amount, $options){
			$endpoint = "https://ssl.ditonlinebetalingssystem.dk/integration/ewindow/Default.aspx";
			$parameters = [
					'merchantnumber' => $this->merchantId,
					'amount' => $amount,
					'windowstate' => $this->windowState,
			];
			foreach ($options as $key => $option){
				$parameters[$key] = $option;
				if($key == 'orderid'){
					$this->orderId = intval($option);
				}
			}
			$this->hash = $this->md5Hash($parameters);
			$parameters['hash'] = $this->hash;

			$query = http_build_query($parameters);
			return "{$endpoint}?{$query}";
		}

		/**
		 * Assuming that magic calls are for the remote api
		 *
		 * @param string $name
		 * @param array $arguments
		 * @return mixed|NULL Returns a typed response object, or null upon failure
		 * @throws EpayException
		 */
		function __call($name, array $arguments){
			$paymentApi = new Payment($this->config);
			if($paymentApi->hasFunction($name)){
				$response = call_user_func_array([$paymentApi, $name], $arguments);
				return $response;
			}
			return null;
		}
	}
}