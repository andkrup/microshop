<?php
namespace supervillainhq\lexcorp\microshop\payment\epay{
	class TransactionStatus{
		const PAYMENT_UNDEFINED = 1;
		const PAYMENT_NEW = 1;
		const PAYMENT_CAPTURED = 1;
		const PAYMENT_DELETED = 1;
		const PAYMENT_INSTANT_CAPTURE_FAILED = 1;
		const PAYMENT_SUBSCRIPTION_INI = 1;
		const PAYMENT_RENEW = 1;
		const PAYMENT_EUROLINE_WAIT_CAPTURE = 1;
		const PAYMENT_EUROLINE_WAIT_CREDIT = 1;
		const PARTLY_TRANSACTION_HIDDEN = 1;
	}
}