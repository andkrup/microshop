<?php
namespace supervillainhq\lexcorp\microshop\payment{
	class Currency implements \Serializable{
		private $code;


		function code($code = null){
			if(is_null($code)){
				return $this->code;
			}
			$this->code = $code;
		}

		function __construct($code = null){
			if(is_null($code)){
				$code = 'DKK';
			}
			$this->code = $code;
		}

		function serialize () {
			$object = (object) [
				'code' => $this->code
			];
			return serialize($object);
		}
		function unserialize ($serialized) {
			// handle the situations where an already unserialized object is passed by recursed unserialize calls
			if(is_null($serialized)){
				return;
			}
			elseif(is_object($serialized)){
				$this->code($serialized->code);
			}
			else{
				// $serialized is a string-interpretation and should be unserialized
				if(!($serialized instanceof Currency)){
					$serialized = unserialize($serialized); // $serialized is now a stdClass/simple object
				}
				if($serialized instanceof Currency){
					$this->code($serialized->code);
				}
			}
		}

		function equals(Currency $currency){
			return $currency->code == $this->code;
		}

		function __toString(){
			return "{$this->code}";
		}
	}
}