<?php
namespace supervillainhq\lexcorp\microshop\payment{
	class Card{
		protected $number;
		protected $type;
		protected $name;

		function number( $number = null){
			if(is_null($number)){
				return $this->number;
			}
			$this->number = $number;
		}
		function type(CardType $type = null){
			if(is_null($type)){
				return $this->type;
			}
			$this->type = $type;
		}
		function name( $name = null){
			if(is_null($name)){
				return $this->name;
			}
			$this->name = $name;
		}
	}
}