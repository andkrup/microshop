<?php
namespace supervillainhq\lexcorp\microshop\payment{
	use Phalcon\Config;
	use supervillainhq\lexcorp\microshop\payment\epay\Epay;
	use Phalcon\Http\RequestInterface;

	class Gateway{
		private $config;
		private $name;


		function name(){
			return $this->config->payment->gateway->id;
		}

		function merchantId(){
			return $this->config->payment->gateway->{'merchant-id'};
		}

		function __construct(Config $config){
			$this->config = $config;
		}

		/**
		 * Assume that all magic method calls to __call should invoke a remote api method
		 * @param string $name
		 * @param array $arguments
		 */
		function __call($name, array $arguments){
			$epay = new Epay($this->config);
			return call_user_func_array([$epay, $name], $arguments);
		}

		function paymentWindowURL($amount, $options){
			$epay = new Epay($this->config);
			return $epay->paymentWindow($amount, $options);
		}

		function validateRequest(RequestInterface $request){
			$params = $request->getQuery();
			$md5 = $request->getQuery('hash');
			$epay = new Epay($this->config);
			return $epay->md5Validate($md5, $params);
		}
	}
}