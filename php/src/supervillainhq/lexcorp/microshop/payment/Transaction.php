<?php
namespace supervillainhq\lexcorp\microshop\payment{
	use Phalcon\Config;
	use Phalcon\Di;
	use supervillainhq\core\Objectible;
	use supervillainhq\core\date\Date;
	use supervillainhq\core\db\DataAware;
	use supervillainhq\lexcorp\microshop\payment\epay\api\Capture;
	use supervillainhq\lexcorp\microshop\Invoice;
	use supervillainhq\spectre\db\DataReader;

	class Transaction implements \Serializable{
		use DataAware;
		use Objectible;

		const REJECTED = -50; // transaction has rejected or cancelled
		const DELETED = -45; // transaction has been nullified before money was captured
		const REFUNDED = -40; // buyer has been credited the amount that was previously captured
		const CAPTURE_REJECTED = -30; // buyers account failed to let go of any money
		const AUTH_REJECTED = -20; // buyers account does not have money (enough)
		const INITIATED = 10; // a clean slate - no money has been talked about
		const AUTHORIZED = 20; // buyers account has been verified to have the money
		const CAPTURED = 30; // buyers account has been relieved of money
		const ARCHIVED = 40;

		private $status;
		private $invoice;
		private $invoiceId;
		private $gatewayID;
		private $gatewayTransactionID;
		private $amount;
		private $currency;
		private $errorCode;

		function amount(){
			return $this->amount;
		}
		function currency(){
			return $this->currency;
		}

		function status(){
			if(is_null($this->status)){
				return self::INITIATED;
			}
			return $this->status;
		}
		function invoice(Invoice $invoice = null){
			if(is_null($invoice)){
				return $this->invoice;
			}
			$this->invoice = $invoice;
		}
		function invoiceId($number = null){
			if(is_null($number)){
				return $this->invoiceId;
			}
			$this->invoiceId = $number;
		}
		function gatewayTransactionID($gatewayTransactionID = null){
			if(is_null($gatewayTransactionID)){
				return $this->gatewayTransactionID;
			}
			$this->gatewayTransactionID = $gatewayTransactionID;
		}
		function error($errorCode = null){
			if(is_null($errorCode)){
				return $this->errorCode;
			}
			$this->errorCode = $errorCode;
		}

		function __construct($gatewayID = null){
			$this->gatewayID = $gatewayID;
		}

		function writable(){
			return is_nan($this->errorCode) && is_null($this->gatewayTransactionID);
		}

		function gateway(){
			return Di::getDefault()->getPaymentGateway();
		}

		private function authorize(){
			$this->status = self::AUTHORIZED;
		}
		private function reject(){
			$this->status = self::REJECTED;
		}
		private function archive(){
			$this->status = self::ARCHIVED;
		}
		private function capture(){
			$this->status = self::CAPTURED;
		}
		private function refund(){
			$this->status = self::REFUNDED;
		}
		private function delete(){
			$this->status = self::DELETED;
		}


		static function initiate(Config $config, $amount, $currency = null){
			$gatewayID = $config->payment->gateway->id;
			$instance = new Transaction($gatewayID);
			$instance->amount = $amount;
			if(is_null($currency)){
				$currency = new Currency('DKK');
			}
			$instance->currency = $currency;
			$instance->status = self::INITIATED;
			$id = Di::getDefault()->getObjectwriter('transaction', $instance)->create();
			$instance->id($id);
			return $instance;
		}
		static function authorizeTransaction(Transaction $transaction){
			$transaction->authorize();
			$now = new Date();
			$data = [
					'id' => $transaction->id,
					'gateway_id' => $transaction->gatewayTransactionID,
					'authorized' => $now->format('Y-m-d H:i:s')
			];
			Di::getDefault()->getObjectwriter('transaction', $data)->update();
		}
		static function rejectTransaction(Transaction $transaction, $errorCode){
			$transaction->reject();
			$now = new Date();
			$data = [
					'id' => $transaction->id,
					'error_code' => $errorCode,
					'rejected' => $now->format('Y-m-d H:i:s')
			];
			Di::getDefault()->getObjectwriter('transaction', $data)->update();
		}

		static function captureTransaction(Transaction $transaction){
			$transaction->capture();
			$gateway = $transaction->gateway();

			$request = new Capture($transaction);
			$response = $gateway->capture($request);
			exit;
// 			$now = new Date();
// 			$data = [
// 					'id' => $transaction->id,
// 					'error_code' => $errorCode,
// 					'rejected' => $now->format('Y-m-d H:i:s')
// 			];
// 			Di::getDefault()->getObjectwriter('transaction', $data)->update();
		}
		static function refundTransaction(Transaction $transaction){
			$transaction->refund();
		}
		static function deleteTransaction(Transaction $transaction){
			$transaction->reject();
		}
		static function archiveTransaction(Transaction $transaction){
			$transaction->archive();
		}

		function serialize () {
			$object = (object) [
					'id' => $this->id,
// 					'invoiceId' => intval($this->invoiceId),
					'currency' => is_null($this->currency) ? null : $this->currency->code(),
					'status' => $this->status,
					'gatewayID' => $this->gatewayID,
			];
			$serialized = serialize($object);
			return $serialized;
// 			$object = $this->toObject();
// 			$object->currency = $this->currency->code();
// 			return serialize($object);
		}
		function unserialize ($serialized) {
			// handle the situations where an already unserialized object is passed by recursed unserialize calls
			if(is_null($serialized)){
				return;
			}
			elseif(is_object($serialized)){
				$this->id = $serialized->id;
// 				$this->invoiceId = intval($serialized->invoiceId);
				$this->status = $serialized->status;
				$this->gatewayID = $serialized->gatewayID;
				$this->currency = new Currency($serialized->currency);
			}
			else{
				// $serialized is a string-interpretation and should be unserialized
				if(!($serialized instanceof Transaction)){
					$serialized = unserialize($serialized); // $serialized is now a stdClass/simple object
				}
// 				var_dump($serialized);
				if($serialized instanceof Transaction){
					$this->id = $serialized->id;
// 					$this->invoiceId = intval($serialized->invoiceId);
					$this->status = $serialized->status;
					$this->gatewayID = $serialized->gatewayID;
					$this->currency = new Currency($serialized->currency);
				}
			}
		}

		static function inflate(DataReader $reader){
			$instance = new Transaction();
			$instance->id = $reader->id;
			$instance->gatewayID = $reader->gateway;
			$instance->gatewayTransactionID = $reader->gateway_id;
			$instance->errorCode = $reader->error_code;
			$instance->amount = $reader->amount;
			$instance->invoiceId = $reader->invoice_id;
			return $instance;
		}
	}
}