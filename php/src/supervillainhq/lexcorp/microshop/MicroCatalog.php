<?php
namespace supervillainhq\lexcorp\microshop{
	use Phalcon\Config;
	use Phalcon\DiInterface;
	use Phalcon\Di\InjectionAwareInterface;
	use supervillainhq\spectre\DependencyInjecting;
	use supervillainhq\lexcorp\microshop\browsing\Taggable;
	use supervillainhq\lexcorp\microshop\browsing\Navigator;
	use supervillainhq\lexcorp\microshop\browsing\TagsNavigator;

	/**
	 * A basic products catalog
	 *
	 * @author ak
	 *
	 */
	class MicroCatalog implements Catalog, Taggable, InjectionAwareInterface{
		use DependencyInjecting;

		private $navigationType;
		private $navigator;
		private $products;

		function __construct(DiInterface $dependencyInjector){
			$this->setDI($dependencyInjector);
			$this->resetProducts();
		}

		function navigationType(){
			return $this->navigationType;
		}

		function navigator(Navigator $navigator = null){
			if(is_null($navigator)){
				return $this->navigator;
			}
			$this->navigator = $navigator;
		}

		function resetProducts(array $products = []){
			$this->products = $products;
		}
		function addProduct(Product $product){
			array_push($this->products, $product);
		}
		function removeProduct(Product $product){
			$c = count($this->products);
			for($i = 0; $i < $c; $i++){
				if($this->products[$i] == $product){
					array_splice($this->products, $i, 0);
				}
			}
		}
		function hasProduct(Product $product){
			return in_array($this->products, $product);
		}
		function getProduct($index){
			return $this->products[$index];
		}
		function products(){
			return $this->products;
		}

		function tags(){
			return $this->navigator->tags(); // TODO: tags() not in interface
		}
		function getAnyTag(array $tags){
			$availableTags = $this->navigator->tags();
			return array_intersect($availableTags, $tags);
		}

		static function load(Config $config, DiInterface $dependencyInjector){
			$catalog = new MicroCatalog($dependencyInjector);
// 			foreach ($config->microshop->products as $productName => $classpath){
// 				$reflector = new \ReflectionClass($classpath);
// 				$product = $reflector->newInstance();
// 				if($product instanceof Product){
// 					$mapper = new ProductMapper(['name' => $productName]);
// 					$persistedInstance = $mapper->find();
// 					$product->id($persistedInstance->id());
// 					$product->name($productName);
// 					$catalog->addProduct($product);
// 				}
// 			}
			$catalog->navigationType = $config->microshop->catalogue->navigation;
			switch ($catalog->navigationType){
				case 'tags':
					$catalog->navigator(new TagsNavigator());
					break;
			}
			return $catalog;
		}
	}
}