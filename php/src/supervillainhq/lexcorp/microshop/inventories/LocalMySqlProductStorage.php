<?php
namespace supervillainhq\lexcorp\microshop\inventories{
	use Phalcon\DiInterface;
	use supervillainhq\lexcorp\microshop\ProductStorage;
	use supervillainhq\lexcorp\microshop\db\ShipmentMapper;
	use supervillainhq\spectre\db\SqlQuery;

	class LocalMySqlProductStorage implements ProductStorage{
		private $di;
		private $name;
		private $current;

		function __construct(DiInterface $di){
			$this->di = $di;
		}

		function name($name = null){
			if(!is_null($name)){
				$this->name = $name;
			}
			return $this->name;
		}

		function reload(){

		}

		function shipments(){
			$mapper = new ShipmentMapper();
			return $mapper->all();
		}

		function currentTotal(){
			$sql = "select sum(current_amount) as current_stock
					from shop_InventoryShipments
					where current_amount > 0;";
			$query = SqlQuery::create($sql);
			$query->query();
			return $query->fetchValue('current_stock', SqlQuery::TYPE_INT);
		}
	}
}