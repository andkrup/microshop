<?php
namespace supervillainhq\lexcorp\microshop{
	use Phalcon\Session\Bag;
	use Phalcon\DiInterface;
	use supervillainhq\lexcorp\microshop\payment\Currency;

	class MicroCart implements Cart{
		private $di;
		private $contents;

		function currency(){
			return new Currency('DKK');
		}

		function __construct(DiInterface $di){
			$this->contents = new Bag('microcart');
			$this->contents->setDI($di);
			if(!is_array($this->contents->get('items'))){
				$this->contents->set('items', []);
			}
		}

		function size(){
			$items = $this->contents->get('items');
			return count($items);
		}

		/**
		 * Add an item to the Cart instance. If the second optional argument is not used, the complete item is added, unless
		 * it already exists.
		 *
		 * @param Product $product
		 * @param number $amount (optional) The amount to add
		 */
		function add(Product $product, $amount = 1){
			$item = new MicroCartItem($product, intval($amount));
			$this->addItem($item);
		}
		/**
		 * Remove an item from the Cart instance. If the second aóptional argument is not used, the complete item is removed
		 *
		 * @param Product $product
		 * @param number $amount (optional) The amount to remove
		 */
		function remove(Product $product, $amount = 1){
		}
		/**
		 * Searches the Cart instance for the existance of an item with the product in question. If the second argument is used,
		 * the search checks if the amount is present.
		 *
		 * @param Product $product
		 * @param number $amount (optional)
		 */
		function hasProduct(Product $product, $amount = 0){
			foreach ($this->contents->items as $item){
				if($item->product->id() == $product->id()){
					return true;
				}
			}
			return false;
		}


		function items(){
			return $this->contents->get('items');
		}

		function addItem(CartItem $item){
			$updated = false;
			$items = $this->contents->get('items');
			// iterate existing items and see if one matches
			$c = count($items);
			for($i = 0; $i < $c; $i++){
				$existingItem = $items[$i];
				$serialized = MicroCartItem::hydrate($existingItem);
				// ...if it does, we only increment the amount
				if($serialized->equals($item)){
					$amount = $serialized->amount() + $item->amount();
					$serialized->amount($amount);
					$items[$i] = $serialized->serialize();
					$this->contents->set('items', $items);
					$updated = true;
				}
			}
			if($updated){
				return;
			}
			// if we couln't increment an exisiting items amount, we should add a new cart item
			array_push($items, $item->serialize());
			$this->contents->set('items', $items);
		}
		function removeItem(CartItem $item){}

		function itemAt($index){
			$items = $this->contents->get('items');
			$item = $items[$index];
			if(!empty($item)){
				return MicroCartItem::hydrate($item);
			}
			return null;
		}

		function updateAmountAt($index){}

		function total(){
			$total = 0;
			$items = $this->contents->get('items');
			$c = count($items);
			for($i = 0; $i < $c; $i++){
				$item = $this->itemAt($i);
				$total += $item->total();
			}
			return $total;
		}

		function vat(){
			return round($this->total() * $this->vatValue());
		}
		function vatValue(){
			return .25;
		}

		function totalIncVat(){
			return round($this->total() * (1 + $this->vatValue()));
		}

		function destroy(){
			$this->contents->destroy();
		}


		function checkout(){}
	}
}