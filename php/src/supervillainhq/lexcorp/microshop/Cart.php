<?php
namespace supervillainhq\lexcorp\microshop{
	interface Cart{
		function items();
		function size();
		function addItem(CartItem $item);
		function removeItem(CartItem $item);
		function hasProduct(Product $product, $amount = 0);
		function itemAt($index);
		function updateAmountAt($index);
		function total();
		function vat();
		function totalIncVat();
		function currency();
		function checkout();
	}
}