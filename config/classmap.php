<?php
/**
 * Auto-generated by nicknack classmap
 *
 */
return [
	'mappers' => [
		'asset' => 'supervillainhq\lexcorp\cms\db\AssetMapper',
		'product' => 'supervillainhq\tobissen\db\TobissenWoblerMapper',
		'category' => 'supervillainhq\lexcorp\microshop\browsing\db\ProductCategoryMapper',
		'tag' => 'supervillainhq\lexcorp\microshop\browsing\db\TagMapper',
		'invoice' => 'supervillainhq\lexcorp\microshop\db\InvoiceMapper',
		'transaction' => 'supervillainhq\lexcorp\microshop\db\TransactionMapper',
		'address' => 'supervillainhq\spectre\contacts\db\AddressMapper',
	],
	'writers' => [
		'asset' => 'supervillainhq\lexcorp\cms\db\AssetWriter',
		'product' => 'supervillainhq\tobissen\db\TobissenWoblerWriter',
		'category' => 'supervillainhq\lexcorp\microshop\browsing\db\ProductCategoryWriter',
		'tag' => 'supervillainhq\lexcorp\microshop\browsing\db\TagWriter',
		'invoice' => 'supervillainhq\lexcorp\microshop\db\InvoiceWriter',
		'transaction' => 'supervillainhq\lexcorp\microshop\db\TransactionWriter',
		'address' => 'supervillainhq\spectre\contacts\db\AddressWriter',
	],
];