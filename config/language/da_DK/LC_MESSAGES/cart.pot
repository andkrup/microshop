# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-11-20 18:53+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: /vagrant_root/app/views/shop/addresses.phtml:13
msgid "Input addresses"
msgstr ""

#: /vagrant_root/app/views/shop/addresses.phtml:16
msgid "Use alternate delivery address?"
msgstr ""

#: /vagrant_root/app/views/shop/addresses.phtml:45
msgid "Go to delivery options"
msgstr ""

#: /vagrant_root/app/views/shop/address.phtml:2
msgid "Full name"
msgstr ""

#: /vagrant_root/app/views/shop/address.phtml:5
msgid "ATT"
msgstr ""

#: /vagrant_root/app/views/shop/address.phtml:8
msgid "Address"
msgstr ""

#: /vagrant_root/app/views/shop/address.phtml:10
msgid "Address2"
msgstr ""

#: /vagrant_root/app/views/shop/address.phtml:12
msgid "Postal code"
msgstr ""

#: /vagrant_root/app/views/shop/address.phtml:14
msgid "City"
msgstr ""

#: /vagrant_root/app/views/shop/address.phtml:16
msgid "Country"
msgstr ""

#: /vagrant_root/app/views/shop/_cart.phtml:7
msgid "Cart contents:"
msgstr ""

#: /vagrant_root/app/views/shop/_cart.phtml:25
msgid "view cart"
msgstr ""

#: /vagrant_root/app/views/shop/_cart.phtml:26
msgid "checkout"
msgstr ""

#: /vagrant_root/app/views/shop/cart.phtml:11
msgid "Your Cart contents"
msgstr ""

#: /vagrant_root/app/views/shop/cart.phtml:15
msgid "Product"
msgstr ""

#: /vagrant_root/app/views/shop/cart.phtml:16
#: /vagrant_root/app/views/shop/_checkout.phtml:21
msgid "Amount"
msgstr ""

#: /vagrant_root/app/views/shop/cart.phtml:17
msgid "Item price"
msgstr ""

#: /vagrant_root/app/views/shop/cart.phtml:66
msgid "Check-out"
msgstr ""

#: /vagrant_root/app/views/shop/_checkout.phtml:16
msgid "Your Order"
msgstr ""

#: /vagrant_root/app/views/shop/_checkout.phtml:20
msgid "Items"
msgstr ""

#: /vagrant_root/app/views/shop/_checkout.phtml:22
#: /vagrant_root/app/views/shop/_checkout.phtml:93
msgid "Total"
msgstr ""

#: /vagrant_root/app/views/shop/_checkout.phtml:35
msgid "VAT Included"
msgstr ""

#: /vagrant_root/app/views/shop/_checkout.phtml:39
msgid "Products"
msgstr ""

#: /vagrant_root/app/views/shop/_checkout.phtml:47
msgid "Invoiced to"
msgstr ""

#: /vagrant_root/app/views/shop/_checkout.phtml:48
msgid "Delivered to"
msgstr ""

#: /vagrant_root/app/views/shop/_checkout.phtml:75
msgid "Delivery by"
msgstr ""

#: /vagrant_root/app/views/shop/contact_info.phtml:2
msgid "Email"
msgstr ""

#: /vagrant_root/app/views/shop/contact_info.phtml:4
msgid "Phone"
msgstr ""

#: /vagrant_root/app/views/shop/delivery.phtml:11
msgid "Delivery Service"
msgstr ""

#: /vagrant_root/app/views/shop/delivery.phtml:16
msgid "Select Delivery Service"
msgstr ""

#: /vagrant_root/app/views/shop/delivery.phtml:17
msgid "Cost"
msgstr ""

#: /vagrant_root/app/views/shop/delivery.phtml:33
msgid "Order Total (including your selected delivery option)"
msgstr ""

#: /vagrant_root/app/views/shop/delivery.phtml:43
msgid "Place Order"
msgstr ""

#: /vagrant_root/app/views/shop/index.phtml:3
msgid "Woblers"
msgstr ""

#: /vagrant_root/app/views/shop/payment.phtml:7
msgid "Summary &amp; Terms and conditions"
msgstr ""

#: /vagrant_root/app/views/shop/payment.phtml:11
msgid "Terms and conditions"
msgstr ""

#: /vagrant_root/app/views/shop/payment.phtml:11
msgid "read the terms and conditions"
msgstr ""

#: /vagrant_root/app/views/shop/payment.phtml:12
msgid "I have read the terms and agree"
msgstr ""

#: /vagrant_root/app/views/shop/payment.phtml:19
msgid "Start payment"
msgstr ""

#: /vagrant_root/app/views/shop/payment.phtml:19
msgid "(payment window at epay.dk)"
msgstr ""

#: /vagrant_root/app/views/shop/woblers.phtml:21
#: /vagrant_root/app/views/shop/woblers.phtml:22
#: /vagrant_root/app/views/shop/woblers.phtml:49
#: /vagrant_root/app/views/shop/woblers.phtml:50
msgid "Add to cart"
msgstr ""
