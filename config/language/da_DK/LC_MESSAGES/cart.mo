��    *      l  ;   �      �     �     �     �     �     �     �     �  	   �                         %     6     B  	   H     R     i     �     �  
   �     �  5   �     �     �     �     
               3  "   A     d     y          �     �     �  
   �     �     �  	   �  N       R     p     t     �  	   �     �     �     �     �     �     �     �     �  
   �     �     �       /   !     Q     l     {  	   �  &   �     �     �  
   �     �  	   �     �     	      	     1	     D	     I	     i	     x	  
   �	  	   �	     �	     �	     �	           *                                   &                                                     
         !          (                %                   $      	                '   #         )       "    (payment window at epay.dk) ATT Add to cart Address Address2 Amount Cart contents: Check-out City Cost Country Delivered to Delivery Service Delivery by Email Full name Go to delivery options I have read the terms and agree Input addresses Invoiced to Item price Items Order Total (including your selected delivery option) Phone Place Order Postal code Product Products Select Delivery Service Start payment Summary &amp; Terms and conditions Terms and conditions Total Use alternate delivery address? VAT Included Woblers Your Cart contents Your Order checkout read the terms and conditions view cart Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-11-20 19:53+0100
PO-Revision-Date: 2015-11-20 19:55+0100
Last-Translator: 
Language-Team: 
Language: da_DK
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.6
Plural-Forms: nplurals=2; plural=(n != 1);
 (betalingsvindue hos epay.dk) ATT Læg i indkøbskurven Adresse Adresse 2 Antal Indkøbskurv Gå til kassen By Pris Land Leveret til Leveringsmetode Leveret af Email For- og efternavne Vælg leveringsmetode Jeg har læst og godkender handelsbetingelserne Indtast adresseinformation Faktureret til Varens pris Genstande Ordretotal (iberegnet leveringsmetode) Telefon Afslut ordren Postnummer Produkt Produkter Vælg leveringsmetode Start betaling Faktura &amp; Handelsbetingelser Handelsbetingelser Ialt Brug en anden leveringsadresse? Inklusive moms Woblere Dine varer Din ordre gå til kassen læs handelsbetingelserne Kurvens indhold 