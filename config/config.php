<?php

return new \Phalcon\Config([
    'database' => json_decode(file_get_contents(__DIR__ . "/../env/database.json")),
    'application' => json_decode(file_get_contents(__DIR__ . "/../env/application.json")),
	'files' => json_decode(file_get_contents(__DIR__ . "/../env/files.json")),
	'localization' => json_decode(file_get_contents(__DIR__ . "/../env/localization.json")),
	'stylesheets' => json_decode(file_get_contents(__DIR__ . "/../env/stylesheets.json")),
	'scripts' => json_decode(file_get_contents(__DIR__ . "/../env/scripts.json")),
	'gettext' => json_decode(file_get_contents(__DIR__ . "/../env/gettext.json")),
	'ga' => json_decode(file_get_contents(__DIR__ . "/../env/ga.json")),
	'payment' => json_decode(file_get_contents(__DIR__ . "/../env/payment.json")),
	'microshop' => json_decode(file_get_contents(__DIR__ . "/../env/microshop.json")),
	'shopfront' => json_decode(file_get_contents(__DIR__ . "/../env/shopfront.json"))
]);
